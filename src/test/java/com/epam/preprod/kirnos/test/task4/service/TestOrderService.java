package com.epam.preprod.kirnos.test.task4.service;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.dao.OrderDao;
import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IOrderService;
import com.epam.preprod.kirnos.task4.service.impl.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestOrderService {

    private IOrderService orderService;
    private SmokingToolDao smokingToolDao;
    private OrderDao orderDao;
    private Date firstDate;
    private Date secondDate;


    @Before
    public void before() throws ParseException {
        smokingToolDao = Mockito.mock(SmokingToolDao.class);
        orderDao = Mockito.mock(OrderDao.class);
        orderService = new OrderService(orderDao, smokingToolDao);

        firstDate = new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse("10/10/10/10/2010");
        secondDate = new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse("11/10/10/10/2010");
    }

    @Test
    public void shouldBeCalledOrderDaoAddOrderWhenAddOrder() throws ShopException {
        orderService.addOrder(new Date(), null);

        verify(orderDao).addOrder(any(), any());
    }

    @Test
    public void shouldReturnOneOrderByPeriodIfExistOneOrder() throws ShopException {
        int expectedCount = 1;
        Map<Date, Map<String, Integer>> returnMap = new HashMap<>();
        returnMap.put(firstDate, new HashMap<>());
        when(orderDao.getByPeriod(any(), any())).thenReturn(returnMap);

        Map<Date, Map<SmokingTool, Integer>> map = orderService.getByPeriod(firstDate, secondDate);
        int actualCount = map.size();

        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void shouldReturnEmptyOrdersByPeriodWhenOrdersIsEmpty() throws ShopException {
        Map<Date, Map<SmokingTool, Integer>> map = orderService.getByPeriod(firstDate, secondDate);

        assertTrue(map.isEmpty());
    }


    @Test
    public void shouldReturnEmptyOrdersByDateWhenOrdersIsEmpty() throws ShopException {
        when(orderDao.getAllOrders()).thenReturn(new HashMap<>());

        Map<Date, Map<SmokingTool, Integer>> map = orderService.getByDate(firstDate);

        assertTrue(map.isEmpty());
    }

    @Test
    public void shouldReturnOneOrderByDateWhenOrdersContainOnlyOne() throws ShopException {
        int expectedCount = 1;
        Map<Date, Map<String, Integer>> orders = new HashMap<>();
        orders.put(new Date(), new HashMap<>());
        when(orderDao.getAllOrders()).thenReturn(orders);

        Map<Date, Map<SmokingTool, Integer>> map = orderService.getByDate(firstDate);
        int actualCount = map.size();

        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void shouldReturnCorrectOrderByDate() throws ShopException {
        int expectedCount = 1;
        Map<Date, Map<String, Integer>> orders = new HashMap<>();
        Map<String, Integer> firstMap = new HashMap<>();
        firstMap.put("A", 1);
        orders.put(firstDate, firstMap);
        Map<String, Integer> secondMap = new HashMap<>();
        secondMap.put("B", 1);
        orders.put(secondDate, secondMap);

        when(orderDao.getAllOrders()).thenReturn(orders);

        Map<Date, Map<SmokingTool, Integer>> map = orderService.getByDate(secondDate);
        int actualCount = map.size();

        assertEquals(expectedCount, actualCount);
        assertNotNull(map.get(secondDate));
    }


}
