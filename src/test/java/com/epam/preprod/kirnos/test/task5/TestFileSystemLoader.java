package com.epam.preprod.kirnos.test.task5;

import com.epam.preprod.kirnos.task5.util.FileSystemLoader;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestFileSystemLoader {

    private List<File> listFiles;

    @Before
    public void before(){
        File[] files = new File[3];
        for(int i = 0; i < files.length; i++){
            files[i] = Mockito.mock(File.class);
        }
        listFiles = Arrays.asList(files);
    }

    @Test
    public void shouldOutAllFiles() throws IOException {
        int expectedCount = listFiles.size() + 1;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);

        FileSystemLoader.outFilesInfo(listFiles, ps);
        BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(baos.toByteArray())));
        int actualCount = 0;
        while(br.readLine() != null){
            actualCount++;
        }

        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void shouldReturnOneElementInCollectionIfFileIsFile(){
        File file = listFiles.get(0);
        when(file.isFile()).thenReturn(true);

        List<File> actualList = new ArrayList<>();
        FileSystemLoader.getFilesByCatalog(file, actualList);

        assertEquals(1, actualList.size());
    }

    @Test
    public void shouldReturnAllFilesInCatalog(){
        File testFile = Mockito.mock(File.class);
        when(testFile.isFile()).thenReturn(false);
        when(testFile.isDirectory()).thenReturn(true);
        when(testFile.listFiles()).thenReturn((File[]) listFiles.toArray());
        for (File listFile : listFiles) {
            when(listFile.isFile()).thenReturn(true);
        }

        List<File> actualList = new ArrayList<>();
        FileSystemLoader.getFilesByCatalog(testFile, actualList);

        assertEquals(1 + listFiles.size(), actualList.size());
    }

}
