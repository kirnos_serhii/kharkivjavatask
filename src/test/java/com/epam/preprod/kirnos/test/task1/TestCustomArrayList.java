package com.epam.preprod.kirnos.test.task1;

import com.epam.preprod.kirnos.task1.util.CustomArrayList;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.function.Predicate;

import static org.junit.Assert.*;

public class TestCustomArrayList {

	private List<Integer> testList;

	@Before
	public void before() {
		testList = new CustomArrayList<>();
		testList.add(1);
		testList.add(2);
		testList.add(3);
	}

	@Test
	public void testAddElementMethodExpectedOneElementInList() {
		Integer expected = 999;

		testList.add(expected);

		assertTrue(testList.contains(expected));
	}

	@Test
	public void testAddNullElementExpectedContainsNullElementInList() {
		int index = 1;
		Integer expected = null;

		testList.add(index, expected);

		Integer actual = testList.get(index);
		assertNull(actual);
	}

	@Test
	public void testAddElementInBeginByIndexExpectedThisElementInBeginList() {
		int index = 0;
		Integer expected = 44;

		testList.add(index, expected);

		Integer actual = testList.get(index);
		assertSame(expected, actual);
	}

	@Test
	public void testAddElementInMiddleByIndexExpectedThisElementInMiddleList() {
		int index = 1;
		Integer expected = 44;

		testList.add(index, expected);

		Integer actual = testList.get(index);
		assertSame(expected, actual);
	}

	@Test
	public void testAddElementInEndByIndexExpectedThisElementInEndList() {
		int index = testList.size();
		Integer expected = 44;

		testList.add(index, expected);

		Integer actual = testList.get(index);
		assertSame(expected, actual);
	}

	@Test
	public void shouldIncreaseSizeBeforeAddElements() {
		int sizeBefore = testList.size();
		int countAdd = 3;

		for (int i = 0; i < countAdd; i++) {
			testList.add(0);
		}

		int sizeAfter = testList.size();
		assertEquals(sizeAfter - sizeBefore, countAdd);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldThrowExceptionWhenTryAddElementByIncorrectIndexLargeSize() {
		int index = testList.size();
		Integer expected = 44;

		testList.add(index + 1, expected);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldThrowExceptionWhenTryAddElementByIncorrectIndexLessZero() {
		int index = 0;
		Integer expected = 44;

		testList.add(index - 1, expected);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldThrowExceptionWhenTryGetElementByIncorrectIndexEqualSize() {
		testList.get(testList.size());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldThrowExceptionWhenTryGetElementByIncorrectIndexLessZero() {
		testList.get(-1);
	}

	@Test
	public void testGetLastElementExpectedGiveLastElementFromList() {
		int index = testList.size();
		Integer expected = 44;
		testList.add(index, expected);

		Integer actual = testList.get(index);

		assertSame(expected, actual);
	}

	@Test
	public void shouldRemoveFirstElementByIndex() {
		Integer firstBefore = testList.get(0);

		testList.remove(0);

		Integer firstAfter = testList.get(0);
		assertNotSame(firstBefore, firstAfter);
	}

	@Test
	public void shouldDecreaseSizeBeforeRemoveElements() {
		int sizeBefore = testList.size();

		testList.remove(0);

		int sizeAfter = testList.size();
		assertEquals(sizeAfter - sizeBefore, -1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldThrowExceptionWhenTryRemoveElementByIncorrectIndexEqualSize() {
		testList.remove(testList.size());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldThrowExceptionWhenTryRemoveElementByIncorrectIndexLessZero() {

		testList.remove(-1);
	}

	@Test
	public void testRemoveElementByObjectExpectedListDoNotContainsDeletedElement() {
		Integer element = 44;
		testList.add(element);

		testList.remove(element);

		assertFalse(testList.contains(element));
	}

	@Test
	public void testRemoveNullElementbyObjectExpectedListDoNotContainsFirstNullElement() {
		Integer element = null;
		testList.add(element);

		testList.remove(element);

		assertFalse(testList.contains(element));
	}

	@Test
	public void testIteratorHasNextOnAnEmptyCollection() {
		List<Object> emptyList = new CustomArrayList<>();
		Iterator<Object> iterator = emptyList.iterator();

		boolean actual = iterator.hasNext();

		assertFalse(actual);
	}

	@Test
	public void testIteratorHasNextOnAnNotEmptyCollection() {
		Iterator<Integer> iterator = testList.iterator();

		boolean actual = iterator.hasNext();

		assertTrue(actual);
	}

	@Test(expected = NoSuchElementException.class)
	public void shouldThrowExceptionWhenIteratorNextOnAnEmptyCollection() {
		List<Object> emptyList = new CustomArrayList<>();
		Iterator<Object> iterator = emptyList.iterator();

		iterator.next();
	}

	@Test(expected = IllegalStateException.class)
	public void shouldThrowExceptionWhenIteratorRemoveAgain() {
		Iterator<Integer> iterator = testList.iterator();

		iterator.next();
		iterator.remove();
		iterator.remove();
	}

	@Test
	public void testIteratorGetAllElement() {
		Integer[] first = new Integer[3];
		testList.toArray(first);
		Integer[] second = new Integer[3];
		int index = 0;

		for (Integer el : testList) {
			second[index++] = el;
		}

		assertTrue(Arrays.equals(first, second));
	}

	@Test
	public void shouldReturnAllElementInRightOrderViaIterator() {
		int index = 0;
		Iterator<Integer> iterator = testList.iterator();
		while (iterator.hasNext()) {
			assertSame(iterator.next(), testList.get(index++));
		}
	}

	@Test
	public void shouldReturnAllElementInRightOrderViaIteratorWithPredicate() {
		CustomArrayList<Integer> list = new CustomArrayList<>();
		int count = 100;
		for (int i = 0; i < count; i++) {
			list.add(i);
		}

		Predicate<Integer> predicate = (e) -> e > 2;
		Iterator<Integer> iterator = list.iterator(predicate);
		for (int i = 0; i < count; i++) {
			if (predicate.test(i)) {
				iterator.hasNext();
				assertSame(i, iterator.next());
			}
		}
	}

	@Test
	public void testRemoveAllExpectedListDoNotContainsElementsThatLocatedInCollection() {

		List<Integer> collection = new ArrayList<>();
		for (Integer e : testList) {
			collection.add(e);
		}

		testList.removeAll(collection);

		assertTrue(testList.isEmpty());
	}

	@Test
	public void testClearExpectedEmptyList() {
		testList.clear();

		assertTrue(testList.isEmpty());
	}

	@Test
	public void testSetExpectedEditElementInList() {
		Integer before = testList.get(0);

		testList.set(0, before + 1);

		Integer after = testList.get(0);
		assertNotEquals(before, after);
	}

	@Test
	public void testAddAllElementExpectedListContainsAllElementFromCollection() {
		List<Integer> addList = new ArrayList<>();
		addList.add(33);
		addList.add(44);
		addList.add(55);

		testList.addAll(addList);

		assertTrue(testList.containsAll(addList));
	}

}
