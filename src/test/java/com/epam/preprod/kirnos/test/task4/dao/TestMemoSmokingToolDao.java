package com.epam.preprod.kirnos.test.task4.dao;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.dao.impl.MemoSmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class TestMemoSmokingToolDao {

    private SmokingToolDao smokingToolDao;
    private List<SmokingTool> smokingTools;

    @Before
    public void before() {
        smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("1", "vape", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("2", "Electric vape", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));

        smokingToolDao = new MemoSmokingToolDao(smokingTools);
    }

    @Test
    public void shouldReturnAllSmokingTools() throws DBException {
        int expected = smokingTools.size();
        int actual = smokingToolDao.getAllSmokingTool().size();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnSmokingToolById() throws ShopException {
        String[] expectedId = new String[smokingTools.size()];
        for (int i = 0; i < expectedId.length; i++) {
            expectedId[i] = smokingTools.get(i).getId();
        }

        String[] actualId = new String[smokingTools.size()];
        for (int i = 0; i < expectedId.length; i++) {
            actualId[i] = smokingToolDao.getSmokingToolById(expectedId[i]).getId();
        }

        assertArrayEquals(expectedId, actualId);
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionWhenTryGetToolByIncorrectIndex() throws ShopException {
        smokingToolDao.getSmokingToolById("000");
    }

}
