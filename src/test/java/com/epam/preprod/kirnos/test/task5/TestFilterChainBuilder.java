package com.epam.preprod.kirnos.test.task5;

import com.epam.preprod.kirnos.task5.filter.Filter;
import com.epam.preprod.kirnos.task5.filter.impl.FilterChainBuilder;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestFilterChainBuilder {

    @Test
    public void shouldBuildCorrectChain() throws ParseException {
        FilterChainBuilder filterChainBuilder = new FilterChainBuilder();
        filterChainBuilder.addFilterByExtension("java");
        filterChainBuilder.addFilterByDate(new SimpleDateFormat("dd/MM/yyyy").parse("10/10/2010"),
                new SimpleDateFormat("dd/MM/yyyy").parse("13/10/2010"));
        filterChainBuilder.addFilterByName("name");
        filterChainBuilder.addFilterBySize(1, 3);

        Filter filterChain = filterChainBuilder.build();

        List<File> listFiles = new ArrayList<>();
        File file = Mockito.mock(File.class);
        when(file.isFile()).thenReturn(true);
        when(file.getName()).thenReturn("name.java");
        when(file.length()).thenReturn(2L);
        when(file.lastModified()).thenReturn(new SimpleDateFormat("dd/MM/yyyy").parse("11/10/2010").getTime());
        listFiles.add(file);

        filterChain.doFilter(listFiles);

        verify(file, times(2)).length();
        verify(file, times(2)).getName();
        verify(file).lastModified();
    }

}
