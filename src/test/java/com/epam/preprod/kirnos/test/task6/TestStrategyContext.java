package com.epam.preprod.kirnos.test.task6;

import com.epam.preprod.kirnos.task1.entity.Hookah;
import com.epam.preprod.kirnos.task1.entity.ManagementType;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.Pipe;
import com.epam.preprod.kirnos.task1.entity.SteamSystem;
import com.epam.preprod.kirnos.task1.entity.TypePipe;
import com.epam.preprod.kirnos.task1.entity.Vape;
import com.epam.preprod.kirnos.task1.entity.Wood;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.create.HookahStrategy;
import com.epam.preprod.kirnos.task6.create.PipeStrategy;
import com.epam.preprod.kirnos.task6.create.StrategyContext;
import com.epam.preprod.kirnos.task6.create.VapeStrategy;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task6.util.Message;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

public class TestStrategyContext {

    private StrategyContext strategyContext;
    private Input input;
    private String str = "ABC";
    private int i = 33;
    private double d = 0.33;
    private ManagementType managementType = ManagementType.AUTOMATIC;
    private Material material = Material.COMBINED;
    private SteamSystem steamSystem = SteamSystem.CARTOMIZER;
    private TypePipe typePipe = TypePipe.MODERN;
    private Wood wood = Wood.BEECH;

    @Before
    public void before() throws ShopException {
        input = Mockito.mock(Input.class);
        when(input.inputBoolean(any())).thenReturn(true);
        when(input.inputDouble(any(), anyDouble(), anyDouble())).thenReturn(d);
        when(input.inputInt(any(), anyInt(), anyInt())).thenReturn(i);
        when(input.inputString(any(), any())).thenReturn(str);
        when(input.inputEnum(Message.INPUT_MANAGEMENT_MSG, ManagementType.values())).thenReturn(managementType);
        when(input.inputEnum(Message.INPUT_MATERIAL_MINE_MSG, Material.values())).thenReturn(material);
        when(input.inputEnum(Message.INPUT_MATERIAL_MSG, Material.values())).thenReturn(material);
        when(input.inputEnum(Message.INPUT_CASE_MATERIAL_MSG, Material.values())).thenReturn(material);
        when(input.inputEnum(Message.INPUT_STREAM_TYPE_MSG, SteamSystem.values())).thenReturn(steamSystem);
        when(input.inputEnum(Message.INPUT_TYPE_PIPE_MSG, TypePipe.values())).thenReturn(typePipe);
        when(input.inputEnum(Message.INPUT_WOOD_TYPE_MSG, Wood.values())).thenReturn(wood);

        strategyContext = new StrategyContext();
    }

    @Test
    public void shouldReturnCorrectVape() throws ShopException {
        strategyContext.setSmokingToolStrategy(new VapeStrategy(input));
        Vape vape = (Vape) strategyContext.create();

        assertEquals(vape.getCapacity(), d, 0.01);
        assertEquals(vape.getSteamSystem(), steamSystem);
        assertEquals(vape.getBatteryCapacity(), d, 0.01);
        assertEquals(vape.getCaseMaterial(), material);
        assertEquals(vape.getId(), str);
        assertEquals(vape.getManagementType(), managementType);
        assertEquals(vape.getManufacturer(), str);
        assertEquals(vape.getName(), str);
        assertEquals(vape.getPrice(), d, 0.01);
    }

    @Test
    public void shouldReturnCorrectPipe() throws ShopException {
        strategyContext.setSmokingToolStrategy(new PipeStrategy(input));
        Pipe pipe = (Pipe) strategyContext.create();

        assertEquals(pipe.getDiameter(), d, 0.01);
        assertEquals(pipe.getWood(), wood);
        assertEquals(pipe.getMaterial(), material);
        assertEquals(pipe.getTypePipe(), typePipe);
        assertEquals(pipe.getId(), str);
        assertEquals(pipe.getManufacturer(), str);
        assertEquals(pipe.getName(), str);
        assertEquals(pipe.getPrice(), d, 0.01);
    }

    @Test
    public void shouldReturnCorrectHookah() throws ShopException {
        strategyContext.setSmokingToolStrategy(new HookahStrategy(input));
        Hookah hookah = (Hookah) strategyContext.create();

        assertEquals(hookah.getHeight(), i);
        assertEquals(hookah.getMineHookah(), material);
        assertEquals(hookah.getNumbTubes(), i);
        assertEquals(hookah.getMaterial(), material);
        assertEquals(hookah.getId(), str);
        assertEquals(hookah.getManufacturer(), str);
        assertEquals(hookah.getName(), str);
        assertEquals(hookah.getPrice(), d, 0.01);
    }

}
