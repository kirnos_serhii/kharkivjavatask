package com.epam.preprod.kirnos.test.task4.dao;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.dao.HistoryDao;
import com.epam.preprod.kirnos.task4.dao.impl.MemoHistoryDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class TestMemoHistoryDao {

    private HistoryDao historyDao;

    @Before
    public void before() {
        historyDao = new MemoHistoryDao();
    }

    @Test
    public void shouldReturnCurrentHistory() throws DBException {
        SmokingTool A = new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        SmokingTool B = new Vape("B", "B", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        SmokingTool C = new Vape("C", "C", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        SmokingTool D = new Vape("D", "D", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        SmokingTool E = new Vape("E", "E", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        SmokingTool G = new Vape("G", "G", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);

        historyDao.add(A.getId(), A);
        historyDao.add(A.getId(), A);
        historyDao.add(A.getId(), A);
        int expectedCount = 1;
        int actualCount = historyDao.getAll().size();

        assertEquals(expectedCount, actualCount);

        historyDao.add(B.getId(), B);
        historyDao.add(C.getId(), C);
        historyDao.add(D.getId(), D);
        expectedCount = 4;
        actualCount = historyDao.getAll().size();

        assertEquals(expectedCount, actualCount);

        historyDao.add(A.getId(), A);
        expectedCount = 4;
        actualCount = historyDao.getAll().size();

        assertEquals(expectedCount, actualCount);

        historyDao.add(E.getId(), E);
        historyDao.add(G.getId(), G);
        expectedCount = 5;
        actualCount = historyDao.getAll().size();

        assertEquals(expectedCount, actualCount);
    }

}
