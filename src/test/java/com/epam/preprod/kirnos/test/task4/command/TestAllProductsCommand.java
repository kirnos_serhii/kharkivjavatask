package com.epam.preprod.kirnos.test.task4.command;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.command.impl.AllProductsCommand;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task4.service.impl.SmokingToolService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestAllProductsCommand {

    private Command command;
    private ISmokingToolService smokingToolService;
    private Request request;
    private Response response;


    @Before
    public void before() {
        smokingToolService = Mockito.mock(SmokingToolService.class);
        command = new AllProductsCommand(smokingToolService);

        request = new Request();
        response = new Response();
    }

    @Test
    public void shouldBeCallGetAllSmokingToolService() throws IOException, ShopException {
        command.execute(request, response);
        verify(smokingToolService).getAllSmokingTool();
    }


    @Test
    public void shouldSetProductsParameter() throws IOException, ShopException {
        List<SmokingTool> testList = new ArrayList<>();
        testList.add(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        when(smokingToolService.getAllSmokingTool()).thenReturn(testList);
        command.execute(request, response);

        String products = "products";
        assertThat(testList, is((List<SmokingTool>) response.getAttribute(products)));
    }

    @Test
    public void shouldSetTitleParameter() throws IOException, ShopException {
        command.execute(request, response);

        String title = "title";
        assertTrue(response.getAttribute(title) instanceof String);
    }

}
