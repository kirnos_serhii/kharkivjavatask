package com.epam.preprod.kirnos.test.task4.command;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.command.impl.MainPageCommand;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

public class TestMainPageCommand {

    private Command command;
    private Request request;
    private Response response;

    @Before
    public void before(){
        command = new MainPageCommand();
        request = new Request();
        response = new Response();
    }

    @Test
    public void shouldSetTitleParameter() throws IOException, ShopException {
        command.execute(request, response);

        String title = "title";
        assertTrue(response.getAttribute(title) instanceof String);
    }

}
