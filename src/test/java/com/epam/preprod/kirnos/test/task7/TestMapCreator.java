package com.epam.preprod.kirnos.test.task7;

import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.Pipe;
import com.epam.preprod.kirnos.task1.entity.TypePipe;
import com.epam.preprod.kirnos.task1.entity.Wood;
import com.epam.preprod.kirnos.task7.proxy.create.impl.MapCreator;
import com.epam.preprod.kirnos.task7.proxy.entity.IPipe;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestMapCreator {

    @Test
    public void shouldCreateCorrectObject() throws Throwable {
        IPipe pipe = new Pipe("id", "name", 10.0, "manufacturer", true,
                Material.COMBINED, Wood.BEECH, 10.2, TypePipe.MODERN);

        IPipe mapPipe = (IPipe) (new MapCreator()).create(pipe);

        assertEquals(pipe.getDiameter(), mapPipe.getDiameter(), 0.01);
    }

    @Test
    public void shouldSetNewValue() throws Throwable {
        double newDiameter = 1.1;
        IPipe pipe = new Pipe("id", "name", 10.0, "manufacturer", true,
                Material.COMBINED, Wood.BEECH, 10.2, TypePipe.MODERN);

        IPipe mapPipe = (IPipe) (new MapCreator()).create(pipe);
        mapPipe.setDiameter(newDiameter);

        assertEquals(newDiameter, mapPipe.getDiameter(), 0.01);
    }

}
