package com.epam.preprod.kirnos.test.task9;

import com.epam.preprod.kirnos.task1.entity.ManagementType;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task1.entity.SteamSystem;
import com.epam.preprod.kirnos.task1.entity.Vape;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.impl.SmokingToolService;
import com.epam.preprod.kirnos.task9.Server;
import com.epam.preprod.kirnos.task9.commnd.Command;
import com.epam.preprod.kirnos.task9.commnd.impl.CountCommand;
import com.epam.preprod.kirnos.task9.thread.ThreadFactory;
import com.epam.preprod.kirnos.task9.thread.tcp.CommandThreadTcp;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestServer {

    private SmokingToolService smokingToolService;

    @Before
    public void before() throws ShopException {
        smokingToolService = Mockito.mock(SmokingToolService.class);

        List<SmokingTool> smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("B", "B", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));

        when(smokingToolService.getAllSmokingTool()).thenReturn(smokingTools);
        when(smokingToolService.getSmokingToolById(anyString())).thenReturn(smokingTools.get(0));
    }

    @Test
    public void shouldCallGetThreadMethod() throws IOException, InterruptedException {
        int countCalled = 3;
        ThreadFactory threadFactory = Mockito.mock(ThreadFactory.class);
        Thread commandThread = Mockito.mock(Thread.class);
        Map<String, Command> commands = new HashMap<>();
        commands.put("count", new CountCommand(smokingToolService));

        CommandThreadTcp commandThreadTcp = Mockito.mock(CommandThreadTcp.class);
        when(threadFactory.getThread(any())).thenReturn(commandThreadTcp);
        when(threadFactory.getPort()).thenReturn(3001);
        Server server = new Server(threadFactory);
        server.start();

        for(int i = 0; i < countCalled; i++) {
            Socket sock = new Socket("localhost", 3001);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(sock.getInputStream()));
            BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(sock.getOutputStream()));
            bw.write("get info=B");
            bw.newLine();
            bw.flush();
        }

        server.exit();

        verify(threadFactory, times(countCalled)).getThread(any(Socket.class));
    }


}
