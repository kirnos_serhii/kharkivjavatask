package com.epam.preprod.kirnos.test.task3;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task3.key.HashKey;
import com.epam.preprod.kirnos.task3.key.HashLenKey;
import com.epam.preprod.kirnos.task3.key.HashSumFourKey;
import org.junit.Test;

import java.util.*;

public class TestHashKey {

    interface HashKeyFactory<P extends HashKey> {
        P create(String key);
    }

    private List<SmokingTool> list = Arrays.asList(
        new Vape("1","vape", 12.30, "VapePro", ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false),
        new Vape("2","Electric vape", 12.30, "VapePro", ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false),
        new Vape("3","Vappe", 12.30, "VapePro", ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false)
    );

    @Test
    public void testHashMap(){
        System.out.println("--> testHashMap");
        Map<HashKey, SmokingTool> hashMap1 = new HashMap<>();
        fillerMap(hashMap1, HashSumFourKey::new);
        Map<HashKey, SmokingTool> hashMap2 = new HashMap<>();
        fillerMap(hashMap2, HashLenKey::new);

        out(hashMap1);
        out(hashMap2);
    }

    @Test
    public void testLinkedHashMap(){
        System.out.println("--> testLinkedHashMap");
        Map<HashKey, SmokingTool> linkedHashMap1 = new LinkedHashMap<>();
        fillerMap(linkedHashMap1, HashSumFourKey::new);

        Map<HashKey, SmokingTool> linkedHashMap2 = new LinkedHashMap<>();
        fillerMap(linkedHashMap2, HashLenKey::new);

        out(linkedHashMap1);
        out(linkedHashMap2);
    }

    private void fillerMap(Map<HashKey, SmokingTool> inputMap, HashKeyFactory hkf){
        for(SmokingTool st: list){
            inputMap.put(hkf.create(st.getName()), st);
        }
    }

    private void out(Map<HashKey, SmokingTool> map) {
        map.forEach((k, v)-> System.out.println("key = " + k));
    }

}
