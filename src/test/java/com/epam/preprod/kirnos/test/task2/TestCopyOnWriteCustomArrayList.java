package com.epam.preprod.kirnos.test.task2;

import com.epam.preprod.kirnos.task2.util.CopyOnWriteCustomArrayList;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import java.util.function.Predicate;

import static org.junit.Assert.*;

public class TestCopyOnWriteCustomArrayList {

    private List<Integer> testList;

    @Before
    public void before() {
        testList = new CopyOnWriteCustomArrayList<>();
        testList.add(1);
        testList.add(2);
        testList.add(3);
    }

    @Test
    public void testAddElementMethodExpectedOneElementInList() {
        Integer expected = 999;

        testList.add(expected);

        assertTrue(testList.contains(expected));
    }

    @Test
    public void testAddNullElementExpectedContainsNullElementInList() {
        int index = 1;
        Integer expected = null;

        testList.add(index, expected);

        Integer actual = testList.get(index);
        assertNull(actual);
    }

    @Test
    public void testAddElementInBeginByIndexExpectedThisElementInBeginList() {
        int index = 0;
        Integer expected = 44;

        testList.add(index, expected);

        Integer actual = testList.get(index);
        assertSame(expected, actual);
    }

    @Test
    public void testAddElementInMiddleByIndexExpectedThisElementInMiddleList() {
        int index = 1;
        Integer expected = 44;

        testList.add(index, expected);

        Integer actual = testList.get(index);
        assertSame(expected, actual);
    }

    @Test
    public void testAddElementInEndByIndexExpectedThisElementInEndList() {
        int index = testList.size();
        Integer expected = 44;

        testList.add(index, expected);

        Integer actual = testList.get(index);
        assertSame(expected, actual);
    }

    @Test
    public void shouldIncreaseSizeBeforeAddElements() {
        int sizeBefore = testList.size();
        int countAdd = 3;

        for (int i = 0; i < countAdd; i++) {
            testList.add(0);
        }

        int sizeAfter = testList.size();
        assertEquals(sizeAfter - sizeBefore, countAdd);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrowExceptionWhenTryAddElementByIncorrectIndexLargeSize() {
        int index = testList.size();
        Integer expected = 44;

        testList.add(index + 1, expected);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrowExceptionWhenTryAddElementByIncorrectIndexLessZero() {
        int index = 0;
        Integer expected = 44;

        testList.add(index - 1, expected);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrowExceptionWhenTryGetElementByIncorrectIndexEqualSize() {
        testList.get(testList.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrowExceptionWhenTryGetElementByIncorrectIndexLessZero() {
        testList.get(-1);
    }

    @Test
    public void testGetLastElementExpectedGiveLastElementFromList() {
        int index = testList.size();
        Integer expected = 44;
        testList.add(index, expected);

        Integer actual = testList.get(index);

        assertSame(expected, actual);
    }

    @Test
    public void shouldRemoveFirstElementByIndex() {
        Integer firstBefore = testList.get(0);

        testList.remove(0);

        Integer firstAfter = testList.get(0);
        assertNotSame(firstBefore, firstAfter);
    }

    @Test
    public void shouldDecreaseSizeBeforeRemoveElements() {
        int sizeBefore = testList.size();

        testList.remove(0);

        int sizeAfter = testList.size();
        assertEquals(sizeAfter - sizeBefore, -1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrowExceptionWhenTryRemoveElementByIncorrectIndexEqualSize() {
        testList.remove(testList.size());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrowExceptionWhenTryRemoveElementByIncorrectIndexLessZero() {

        testList.remove(-1);
    }

    @Test
    public void testRemoveElementByObjectExpectedListDoNotContainsDeletedElement() {
        Integer element = 44;
        testList.add(element);

        testList.remove(element);

        assertFalse(testList.contains(element));
    }

    @Test
    public void testRemoveNullElementByObjectExpectedListDoNotContainsFirstNullElement() {
        Integer element = null;
        testList.add(element);

        testList.remove(element);

        assertFalse(testList.contains(element));
    }

    @Test
    public void testIteratorHasNextOnAnEmptyCollection() {
        Iterator<Object> iterator = (new CopyOnWriteCustomArrayList<>()).iterator();

        boolean actual = iterator.hasNext();

        assertFalse(actual);
    }

    @Test
    public void testIteratorHasNextOnAnNotEmptyCollection() {
        Iterator<Integer> iterator = testList.iterator();

        boolean actual = iterator.hasNext();

        assertTrue(actual);
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldThrowExceptionWhenIteratorNextOnAnEmptyCollection() {
        Iterator<Object> iterator = new CopyOnWriteCustomArrayList<>().iterator();

        iterator.next();
    }

    @Test
    public void testIteratorGetAllElement() {
        Integer[] first = new Integer[3];
        testList.toArray(first);
        Integer[] second = new Integer[3];
        int index = 0;

        for (Integer el : testList) {
            second[index++] = el;
        }

        assertArrayEquals(first, second);
    }

    @Test
    public void shouldReturnAllElementInRightOrderViaIterator() {
        int index = 0;
        for (Integer integer : testList) {
            assertSame(integer, testList.get(index++));
        }
    }

    @Test
    public void shouldReturnAllElementInRightOrderViaIteratorWithPredicate() {
        CopyOnWriteCustomArrayList<Integer> list = new CopyOnWriteCustomArrayList<>();
        int count = 100;
        for (int i = 0; i < count; i++) {
            list.add(i);
        }

        Predicate<Integer> predicate = (e) -> e > 2;
        Iterator<Integer> iterator = list.iterator(predicate);
        for (int i = 0; i < count; i++) {
            if (predicate.test(i)) {
                iterator.hasNext();
                assertSame(i, iterator.next());
            }
        }
    }

    @Test
    public void testRemoveAllExpectedListDoNotContainsElementsThatLocatedInCollection() {
        List<Integer> collection = new ArrayList<>();
        collection.addAll(testList);

        testList.removeAll(collection);

        assertTrue(testList.isEmpty());
    }

    @Test
    public void testClearExpectedEmptyList() {
        testList.clear();

        assertTrue(testList.isEmpty());
    }

    @Test
    public void testSetExpectedEditElementInList() {
        Integer before = testList.get(0);

        testList.set(0, before + 1);

        Integer after = testList.get(0);
        assertNotEquals(before, after);
    }

    @Test
    public void testAddAllElementExpectedListContainsAllElementFromCollection() {
        List<Integer> addList = new ArrayList<>();
        addList.add(33);
        addList.add(44);
        addList.add(55);

        testList.addAll(addList);

        assertTrue(testList.containsAll(addList));
    }

    @Test
    public void shouldNotContainsAddedElementsWithAdd() {
        Integer firstElement = 100;
        Integer secondElement = 200;
        Iterator<Integer> iterator = testList.iterator();
        Object[] array = testList.toArray();

        testList.add(firstElement);
        testList.add(0, secondElement);

        while (iterator.hasNext()) {
            Object element = iterator.next();
            assertNotEquals(element, firstElement);
            assertNotEquals(element, secondElement);
        }

        for (Object element : array) {
            assertNotEquals(element, firstElement);
            assertNotEquals(element, secondElement);
        }
    }

    @Test
    public void shouldNotContainsAddedElementsWithAddAll() {
        List<Integer> firstElements = new ArrayList<>();
        for (int i = 100; i < 123; i++) {
            firstElements.add(i);
        }
        List<Integer> secondElements = new ArrayList<>();
        for (int i = 123; i < 130; i++) {
            secondElements.add(i);
        }
        Iterator<Integer> iterator = testList.iterator();
        Object[] array = testList.toArray();

        testList.addAll(firstElements);
        testList.addAll(0, secondElements);

        while (iterator.hasNext()) {
            Object itElement = iterator.next();
            for (Object element : firstElements) {
                assertNotEquals(element, itElement);
            }
            for (Object element : secondElements) {
                assertNotEquals(element, itElement);
            }
        }

        for (Object arrElement : array) {
            for (Object element : firstElements) {
                assertNotEquals(element, arrElement);
            }
            for (Object element : secondElements) {
                assertNotEquals(element, arrElement);
            }
        }
    }

    @Test
    public void shouldIteratorAndArrayNotBeEmptyAfterClean() {
        Iterator<Integer> iterator = testList.iterator();
        Object[] array = testList.toArray();

        testList.clear();

        assertTrue(iterator.hasNext());
        assertTrue(array.length > 0);
    }

    @Test
    public void shouldIteratorAndArrayContainsElementThatWereRemovedByObject() {
        Integer removeElement = 333;
        testList.add(removeElement);
        Iterator<Integer> iterator = testList.iterator();
        Object[] array = testList.toArray();

        testList.remove(removeElement);

        boolean b1 = false;
        while (iterator.hasNext()) {
            Object itElement = iterator.next();
            if(removeElement.equals(itElement)){
                b1 = true;
            }
        }
        boolean b2 = false;
        for (Object arrElement : array) {
            if(arrElement.equals(removeElement)){
                b2 = true;
                break;
            }
        }
        assertTrue(b1);
        assertTrue(b2);
    }

    @Test
    public void shouldIteratorAndArrayContainsElementThatWereRemovedByIndex() {
        Iterator<Integer> iterator = testList.iterator();
        Object[] array = testList.toArray();

        Integer removeElement = testList.get(0);
        testList.remove(0);

        boolean b1 = false;
        while (iterator.hasNext()) {
            Object itElement = iterator.next();
            if(itElement.equals(removeElement)){
                b1 = true;
            }
        }
        boolean b2 = false;
        for (Object arrElement : array) {
            if (arrElement.equals(removeElement)) {
                b2 = true;
                break;
            }
        }
        assertTrue(b1);
        assertTrue(b2);
    }

    @Test
    public void shouldIteratorAndArrayContainsElementsThatWereRemoved() {
        Iterator<Integer> iterator = testList.iterator();
        Object[] array = testList.toArray();
        List<Integer> removeElements = new ArrayList<>();
        for (Object element : array) {
            removeElements.add((Integer) element);
        }
        int count = testList.size() - removeElements.size();

        testList.removeAll(removeElements);

        while (iterator.hasNext()) {
            Object itElement = iterator.next();
            for (Object element : removeElements)
                M:{
                    if (itElement.equals(element)) {
                        count++;
                        break M;
                    }
                }
            count--;
        }
        assertEquals(0, count);

        for (Object element : removeElements) {
            boolean p = false;
            for (Object arrElement : array) {
                if (arrElement.equals(element)) {
                    p = true;
                    break;
                }
            }
            assertTrue(p);
        }
    }

    @Test
    public void shouldBeSameElementAfterSetByIndex(){
        Integer setElement = testList.get(0);
        Iterator<Integer> iterator = testList.iterator();
        Object[] array = testList.toArray();

        testList.set(0, setElement + 1);

        assertEquals(iterator.next(),setElement);
        assertEquals(array[0],setElement);
    }

}
