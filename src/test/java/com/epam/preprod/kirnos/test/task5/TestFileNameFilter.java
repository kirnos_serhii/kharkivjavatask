package com.epam.preprod.kirnos.test.task5;

import com.epam.preprod.kirnos.task5.filter.Filter;
import com.epam.preprod.kirnos.task5.filter.impl.FilterChainBuilder;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestFileNameFilter {

    @Test
    public void shouldReturnListWithCurrentCountElement() {
        int expected = 1;
        FilterChainBuilder filterChainBuilder = new FilterChainBuilder();
        filterChainBuilder.addFilterByName("name");
        Filter filterChain = filterChainBuilder.build();
        List<File> listFiles = new ArrayList<>();
        File file1 = Mockito.mock(File.class);
        when(file1.isFile()).thenReturn(true);
        when(file1.getName()).thenReturn("name.ja");
        listFiles.add(file1);
        File file2 = Mockito.mock(File.class);
        when(file2.isFile()).thenReturn(true);
        when(file2.getName()).thenReturn("noname.ja");
        listFiles.add(file2);

        listFiles = filterChain.doFilter(listFiles);

        assertEquals(expected, listFiles.size());
    }

}
