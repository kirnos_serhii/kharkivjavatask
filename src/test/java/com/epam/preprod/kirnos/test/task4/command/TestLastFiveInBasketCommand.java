package com.epam.preprod.kirnos.test.task4.command;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.command.impl.LastFiveInBasketCommand;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IHistoryService;
import com.epam.preprod.kirnos.task4.service.impl.HistoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestLastFiveInBasketCommand {

    private Command command;
    private IHistoryService historyService;
    private Request request;
    private Response response;

    @Before
    public void before() {
        historyService = Mockito.mock(HistoryService.class);
        command = new LastFiveInBasketCommand(historyService);

        request = new Request();
        response = new Response();
    }

    @Test
    public void shouldBeCalled() throws IOException, ShopException {
        command.execute(request, response);
        verify(historyService).getLastFive();
    }

    @Test
    public void shouldBeReturnCorrectHistoryList() throws IOException, ShopException {
        List<SmokingTool> testList = new ArrayList<>();
        testList.add(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        when(historyService.getLastFive()).thenReturn(testList);
        command.execute(request, response);

        String products = "products";
        assertThat(testList, is((List<SmokingTool>) response.getAttribute(products)));
    }

    @Test
    public void shouldSetTitleParameter() throws IOException, ShopException {
        command.execute(request, response);

        String title = "title";
        assertTrue(response.getAttribute(title) instanceof String);
    }

}
