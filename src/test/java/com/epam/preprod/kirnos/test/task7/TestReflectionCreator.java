package com.epam.preprod.kirnos.test.task7;

import com.epam.preprod.kirnos.task1.entity.Hookah;
import com.epam.preprod.kirnos.task1.entity.ManagementType;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.Pipe;
import com.epam.preprod.kirnos.task1.entity.SteamSystem;
import com.epam.preprod.kirnos.task1.entity.TypePipe;
import com.epam.preprod.kirnos.task1.entity.Vape;
import com.epam.preprod.kirnos.task1.entity.Wood;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task7.create.ReflectionCreator;
import com.epam.preprod.kirnos.task7.create.TypesToolContainer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

public class TestReflectionCreator {

    private String inputString = "ABC";
    private int inputInteger = 33;
    private double inputDouble = 0.33;
    private ManagementType inputManagementType = ManagementType.AUTOMATIC;
    private Material inputMaterial = Material.COMBINED;
    private SteamSystem inputSteamSystem = SteamSystem.CARTOMIZER;
    private TypePipe inputTypePipe = TypePipe.MODERN;
    private Wood inputWood = Wood.BEECH;
    private String inputHookahStr = "hookah";
    private String inputVapeStr = "vape";
    private String inputPipeStr = "pipe";
    ReflectionCreator reflectionCreator;

    @Before
    public void before() throws ShopException {
        Input input = Mockito.mock(Input.class);
        when(input.inputBoolean(any())).thenReturn(true);
        when(input.inputDouble(any(), anyDouble(), anyDouble())).thenReturn(inputDouble);
        when(input.inputInt(any(), anyInt(), anyInt())).thenReturn(inputInteger);
        when(input.inputString(any(), any())).thenReturn(inputString);
        when(input.inputEnum(
                anyString(),
                eq(ManagementType.values()))
        ).thenReturn(inputManagementType);
        when(input.inputEnum(
                anyString(),
                eq(Material.values()))
        ).thenReturn(inputMaterial);
        when(input.inputEnum(
                anyString(),
                eq(SteamSystem.values()))
        ).thenReturn(inputSteamSystem);
        when(input.inputEnum(
                anyString(),
                eq(TypePipe.values()))
        ).thenReturn(inputTypePipe);
        when(input.inputEnum(
                anyString(),
                eq(Wood.values()))
        ).thenReturn(inputWood);
        TypesToolContainer.addCreator("hookah", Hookah.class);
        TypesToolContainer.addCreator("vape", Vape.class);
        TypesToolContainer.addCreator("pipe", Pipe.class);
        reflectionCreator = new ReflectionCreator(input);
    }

    @Test
    public void shouldReturnCorrectVape() throws ShopException, InstantiationException, IllegalAccessException {
        Vape vape = (Vape) reflectionCreator.create("vape");

        assertEquals(vape.getCapacity(), inputDouble, 0.01);
        assertEquals(vape.getSteamSystem(), inputSteamSystem);
        assertEquals(vape.getBatteryCapacity(), inputDouble, 0.01);
        assertEquals(vape.getCaseMaterial(), inputMaterial);
        assertEquals(vape.getId(), inputString);
        assertEquals(vape.getManagementType(), inputManagementType);
        assertEquals(vape.getManufacturer(), inputString);
        assertEquals(vape.getName(), inputString);
        assertEquals(vape.getPrice(), inputDouble, 0.01);
    }

    @Test
    public void shouldReturnCorrectPipe() throws ShopException, InstantiationException, IllegalAccessException {
        Pipe pipe = (Pipe) reflectionCreator.create("pipe");

        assertEquals(pipe.getDiameter(), inputDouble, 0.01);
        assertEquals(pipe.getWood(), inputWood);
        assertEquals(pipe.getMaterial(), inputMaterial);
        assertEquals(pipe.getTypePipe(), inputTypePipe);
        assertEquals(pipe.getId(), inputString);
        assertEquals(pipe.getManufacturer(), inputString);
        assertEquals(pipe.getName(), inputString);
        assertEquals(pipe.getPrice(), inputDouble, 0.01);
    }

    @Test
    public void shouldReturnCorrectHookah() throws ShopException, InstantiationException, IllegalAccessException {
        Hookah hookah = (Hookah) reflectionCreator.create("hookah");

        assertEquals(hookah.getHeight(), inputInteger);
        assertEquals(hookah.getMineHookah(), inputMaterial);
        assertEquals(hookah.getNumbTubes(), inputInteger);
        assertEquals(hookah.getMaterial(), inputMaterial);
        assertEquals(hookah.getId(), inputString);
        assertEquals(hookah.getManufacturer(), inputString);
        assertEquals(hookah.getName(), inputString);
        assertEquals(hookah.getPrice(), inputDouble, 0.01);
    }


}
