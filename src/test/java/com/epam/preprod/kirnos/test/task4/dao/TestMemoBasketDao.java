package com.epam.preprod.kirnos.test.task4.dao;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.dao.BasketDao;
import com.epam.preprod.kirnos.task4.dao.impl.MemoBasketDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestMemoBasketDao {

    private BasketDao basketDao;
    private List<SmokingTool> smokingTools;

    @Before
    public void before() {
        smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("1", "vape", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("2", "Electric vape", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));

        basketDao = new MemoBasketDao();
    }

    @Test
    public void shouldReturnAllItemsFromBasket() throws DBException {
        int expected = smokingTools.size();
        for (SmokingTool smokingTool : smokingTools) {
            basketDao.addSmokingTool(smokingTool);
        }

        int actual = basketDao.getAll().size();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldBeEmptyWhenClearBasket() throws DBException {
        for (SmokingTool smokingTool : smokingTools) {
            basketDao.addSmokingTool(smokingTool);
        }
        basketDao.clear();

        Map<String, Integer> actual = basketDao.getAll();

        assertTrue(actual.isEmpty());
    }
}
