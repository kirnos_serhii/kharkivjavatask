package com.epam.preprod.kirnos.test.task6;

import com.epam.preprod.kirnos.task6.input.impl.TypeInput;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class TestTypeInput {

    @Test
    public void shouldReturnCorrectInput() {
        String[] keys = TypeInput.getKeys();

        for (int i = 0; i < keys.length; i++) {
            assertNotNull(TypeInput.getInput(keys[i]));
        }
    }

    @Test
    public void shouldReturnNull(){
        assertNull(TypeInput.getInput("000"));
    }

}
