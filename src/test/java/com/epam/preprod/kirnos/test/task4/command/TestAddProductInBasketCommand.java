package com.epam.preprod.kirnos.test.task4.command;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.command.impl.AddProductInBasketCommand;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task4.service.impl.BasketService;
import com.epam.preprod.kirnos.task4.service.impl.SmokingToolService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestAddProductInBasketCommand {

    private Command command;
    private ISmokingToolService smokingToolService;
    private IBasketService basketService;

    private Request request;
    private Response response;
    private String readerStr = "reader";
    private BufferedReader reader;

    @Before
    public void before() {
        smokingToolService = Mockito.mock(SmokingToolService.class);
        basketService = Mockito.mock(BasketService.class);
        reader = Mockito.mock(BufferedReader.class);
        command = new AddProductInBasketCommand(smokingToolService, basketService);
        request = new Request();
        response = new Response();
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionInputIncorrectIdNoNumber() throws IOException, ShopException {
        when(reader.readLine()).thenReturn("hj").thenReturn("1");
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionInputIncorrectIdLarge() throws IOException, ShopException {
        when(reader.readLine()).thenReturn("22").thenReturn("1");
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionInputIncorrectCountNoNumber() throws IOException, ShopException {
        SmokingTool st = new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        when(smokingToolService.getSmokingToolById("1")).thenReturn(st);
        when(reader.readLine()).thenReturn("1").thenReturn("aan");
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionInputIncorrectCountLess() throws IOException, ShopException {
        SmokingTool st = new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        when(smokingToolService.getSmokingToolById("1")).thenReturn(st);
        when(reader.readLine()).thenReturn("1").thenReturn("-1");
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test
    public void shouldCallAddSmokingToolService() throws IOException, ShopException {
        SmokingTool st = new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        when(smokingToolService.getSmokingToolById("1")).thenReturn(st);
        when(reader.readLine()).thenReturn("1").thenReturn("1");
        request.setAttribute(readerStr, reader);

        command.execute(request, response);

        verify(basketService).addSmokingTool(st);
    }

    @Test
    public void shouldSetCorrectParametersResponse() throws IOException, ShopException {
        SmokingTool st = new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false);
        when(smokingToolService.getSmokingToolById("1")).thenReturn(st);
        when(reader.readLine()).thenReturn("1").thenReturn("1");
        request.setAttribute(readerStr, reader);
        Map<SmokingTool, Integer> expected = new HashMap<>();

        expected.put(st, 1);
        command.execute(request, response);

        assertTrue(response.getAttribute("title") instanceof String);
        assertEquals(response.getAttribute("products"), expected);
    }

}
