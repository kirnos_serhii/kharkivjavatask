package com.epam.preprod.kirnos.test.task2;

import com.epam.preprod.kirnos.task2.exception.UnmodifiableDoubleCustomListClassException;
import com.epam.preprod.kirnos.task2.util.DoubleCustomList;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class TestDoubleCustomList {

    private List<Integer> privatePart = new ArrayList<>();
    private List<Integer> publicPart = new ArrayList<>();
    private List<Integer> testList;

    @Before
    public void before() {
        for (int i = 0; i < 3; i++) {
            privatePart.add(i);
        }
        for (int i = 3; i < 10; i++) {
            privatePart.add(i);
        }
        testList = new DoubleCustomList<>(privatePart, publicPart);
    }

    @Test
    public void shouldReturnSameElementsWithIterator() {
        int i = 0;
        for (Integer actual : testList) {
            Integer expected = i < privatePart.size() ? privatePart.get(i) : publicPart.get(i);
            i++;
            assertEquals(expected, actual);
        }
    }

    @Test
    public void shouldReturnSameElementsWithToArray() {
        Object[] actual = testList.toArray();
        Object[] expected = new Integer[privatePart.size() + publicPart.size()];
        System.arraycopy(privatePart.toArray(), 0, expected, 0, privatePart.size());
        System.arraycopy(publicPart.toArray(), 0, expected, privatePart.size(), publicPart.size());

        assertArrayEquals(expected, actual);
    }

    @Test
    public void shouldReturnSameElementsWithToArrayWithParameterClass() {
        Integer[] actual = new Integer[testList.size()];
        actual = testList.toArray(actual);
        Object[] expected = new Integer[privatePart.size() + publicPart.size()];
        Integer[] part1 = new Integer[privatePart.size()];
        part1 = privatePart.toArray(part1);
        Integer[] part2 = new Integer[publicPart.size()];
        part2 = publicPart.toArray(part2);

        System.arraycopy(part1, 0, expected, 0, part1.length);
        System.arraycopy(part2, 0, expected, part1.length, part2.length);

        assertArrayEquals(expected, actual);
    }

    @Test
    public void shouldReturnCorrectSize() {
        int expectedSize = privatePart.size() + publicPart.size();
        assertEquals(expectedSize, testList.size());
    }

    @Test
    public void shouldBeIsEmptyWhenListCreateWithTwoEmptyList() {
        assertTrue(new DoubleCustomList<Integer>(new ArrayList<>(), new ArrayList<>()).isEmpty());
    }

    @Test
    public void shouldNotIsEmptyWhenListCreateWithTwoNotEmptyList() {
        assertFalse(new DoubleCustomList<>(privatePart, publicPart).isEmpty());
    }

    @Test
    public void shouldContainAllElementsThatWereAdded() {
        List<Integer> addPart = new ArrayList<>();
        for (int i = 99; i < 120; i++) {
            addPart.add(i);
            testList.add(i);
        }

        assertTrue(testList.containsAll(privatePart));
        assertTrue(testList.containsAll(publicPart));
        assertTrue(testList.containsAll(addPart));
    }

    @Test
    public void shouldDoNotContainsRemovedElements() {
        for (Integer object : publicPart) {
            testList.remove(object);
            assertFalse(testList.contains(object));
        }
    }

    @Test(expected = UnmodifiableDoubleCustomListClassException.class)
    public void shouldThrowExceptionWhenTrieRemoveUnmodifiedPart() {
        testList.remove(privatePart.get(0));
    }

    @Test(expected = UnmodifiableDoubleCustomListClassException.class)
    public void shouldThrowExceptionWhenTrieClearListWithNotEmptyUnmodifiedPart() {
        testList.clear();
    }

    @Test
    public void shouldBeSameElementsWhenAddAndGetElementBySameIndex() {
        Integer element = 1000;
        int index = privatePart.size();
        testList.add(index, element);

        assertEquals(element, testList.get(index));
    }

    @Test
    public void shouldDoNotContainsRemovedElement() {
        Integer element = 1000;
        int index = privatePart.size();
        testList.add(index, element);
        testList.remove(index);

        assertFalse(testList.contains(element));
    }

    @Test
    public void shouldReturnCorrectIndex() {
        Integer element = 1000;
        int index = privatePart.size();
        testList.add(index, element);

        assertEquals(testList.indexOf(element), index);
    }

    @Test
    public void shouldContainsAllElementCollectionAfterAddThisCollection() {
        List<Integer> addPart = new ArrayList<>();
        for (int i = 99; i < 120; i++) {
            addPart.add(i);
            testList.add(i);
        }
        testList.addAll(addPart);

        assertTrue(testList.containsAll(addPart));
    }

    @Test
    public void shouldDoNotContainsAllElementRemovedWithRemoveAll() {
        testList.removeAll(publicPart);

        for (Integer element: testList){
            assertTrue(testList.contains(element));
        }
    }




    @Test
    public void testRemoveNullElementByObjectExpectedListDoNotContainsFirstNullElement() {
        Integer element = null;
        testList.add(element);

        testList.remove(element);

        assertFalse(testList.contains(element));
    }

    @Test
    public void testIteratorHasNextOnAnEmptyCollection() {
        List<Object> emptyList = new DoubleCustomList<>(new ArrayList<>(), new ArrayList<>());
        Iterator<Object> iterator = emptyList.iterator();

        boolean actual = iterator.hasNext();

        assertFalse(actual);
    }

    @Test
    public void testIteratorHasNextOnAnNotEmptyCollection() {
        Iterator<Integer> iterator = testList.iterator();

        boolean actual = iterator.hasNext();

        assertTrue(actual);
    }

    @Test(expected = NoSuchElementException.class)
    public void shouldThrowExceptionWhenIteratorNextOnAnEmptyCollection() {
        List<Object> emptyList = new DoubleCustomList<>(new ArrayList<>(), new ArrayList<>());
        Iterator<Object> iterator = emptyList.iterator();

        iterator.next();
    }

    @Test
    public void shouldReturnAllElementInRightOrderViaIterator() {
        int index = 0;
        for (Integer integer : testList) {
            assertSame(integer, testList.get(index++));
        }
    }


}
