package com.epam.preprod.kirnos.test.task3;

import com.epam.preprod.kirnos.task3.exception.NoUniqueElementException;
import com.epam.preprod.kirnos.task3.util.SingleArrayList;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TestSingleArrayList {

    private List<Integer> sourceList;
    private ArrayList<Integer> testList;

    @Before
    public void before() {
        sourceList = Arrays.asList(1, 2, 3);
        testList = new SingleArrayList<Integer>(sourceList);
    }

    @Test(expected = NoUniqueElementException.class)
    public void shouldThrowExceptionCreateCollectionUsingNoUniqueElements() {
        ArrayList<Integer> collection = new SingleArrayList<Integer>(Arrays.asList(1, 2, 1));
    }

    @Test
    public void testCreateList() {
        List list = Arrays.asList(1, 2, 3);

        ArrayList<Integer> collection = new SingleArrayList<Integer>(list);

        assertEquals(list.size(), collection.size());
    }

    @Test(expected = NoUniqueElementException.class)
    public void shouldThrowExceptionAddNoUniqueElement() {
        testList.add(1);
    }

    @Test
    public void shouldAddUniqueElementIntoCollection() {
        int beforeSize = testList.size();

        testList.add(4);

        assertEquals(beforeSize, testList.size() - 1);
    }

    @Test(expected = NoUniqueElementException.class)
    public void shouldThrowExceptionSetNoUniqueElement() {
        testList.set(0, sourceList.get(0));
    }

    @Test
    public void shouldSetUniqueElementIntoCollection() {
        Integer beforeElement = testList.get(0);

        testList.set(0, 33);

        assertNotEquals(beforeElement, testList.get(0));
    }

    @Test(expected = NoUniqueElementException.class)
    public void shouldThrowExceptionAddByIndexNoUniqueElement() {
        testList.add(1, sourceList.get(0));
    }

    @Test
    public void shouldAddByIndexUniqueElementIntoCollection() {
        int beforeSize = testList.size();

        testList.add(0, 4);

        assertEquals(beforeSize, testList.size() - 1);
    }

    @Test(expected = NoUniqueElementException.class)
    public void shouldThrowExceptionAddAllRepetitiveElements() {
        testList.addAll(Arrays.asList(6, 4, 6));
    }

    @Test(expected = NoUniqueElementException.class)
    public void shouldThrowExceptionAddAllNoUniqueElement() {
        testList.addAll(Arrays.asList(3, 4, 6));
    }

    @Test
    public void shouldAddAllUniqueElement() {
        int beforeSize = testList.size();

        testList.addAll(Arrays.asList(4, 6));

        assertTrue(beforeSize < testList.size());
    }

    @Test(expected = NoUniqueElementException.class)
    public void shouldThrowExceptionAddAllByIndexRepetitiveElements() {
        testList.addAll(1, Arrays.asList(4, 4, 6));
    }

    @Test(expected = NoUniqueElementException.class)
    public void shouldThrowExceptionAddAllByIndexNoUniqueElement() {
        testList.addAll(1, Arrays.asList(3, 4, 6));
    }

    @Test
    public void shouldAddAllByIndexUniqueElement() {
        int beforeSize = testList.size();
        List addedList = Arrays.asList(4, 5, 6);

        testList.addAll(1, addedList);

        assertEquals(beforeSize, testList.size() - addedList.size());
    }
}
