package com.epam.preprod.kirnos.test.task4.service;

import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task4.service.impl.SmokingToolService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


import static org.mockito.Mockito.verify;

public class TestSmokingToolService {

    private ISmokingToolService smokingToolService;
    private  SmokingToolDao smokingToolDao;

    @Before
    public void before(){
        smokingToolDao = Mockito.mock(SmokingToolDao.class);
        smokingToolService = new SmokingToolService(smokingToolDao);
    }

    @Test
    public void shouldCallDaoGetAll() throws ShopException {
        smokingToolService.getAllSmokingTool();

        verify(smokingToolDao).getAllSmokingTool();
    }

    @Test
    public void shouldCallDaoGetById() throws ShopException {
        String id = "testID";
        smokingToolService.getSmokingToolById(id);

        verify(smokingToolDao).getSmokingToolById(id);
    }

}
