package com.epam.preprod.kirnos.test.task4.command;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.command.impl.BuyAllCommand;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task4.service.impl.BasketService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestByAllCommand {

    private Command command;
    private IBasketService basketService;

    private Request request;
    private Response response;
    private String readerStr = "reader";
    private BufferedReader reader;

    @Before
    public void before(){
        basketService = Mockito.mock(BasketService.class);
        reader = Mockito.mock(BufferedReader.class);
        command = new BuyAllCommand(basketService);

        request = new Request();
        response = new Response();
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionIfBasketIsEmpty() throws IOException, ShopException {
        command.execute(request, response);
    }

    @Test
    public void shouldBeCallServiceMethods() throws IOException, ShopException, ParseException {
        Map<SmokingTool, Integer> testMap = new HashMap<>();
        testMap.put(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false), 1);
        when(basketService.getAll()).thenReturn(testMap);
        String dateStr = "10/10/10/10/2010";
        when(reader.readLine()).thenReturn(dateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);

        verify(basketService).getAll();
        verify(basketService).buyAll( new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse(dateStr));
    }

    @Test(expected = ShopException.class)
    public void shouldBThrowExceptionIfDateIncorrect() throws IOException, ShopException {
        Map<SmokingTool, Integer> testMap = new HashMap<>();
        testMap.put(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false), 1);
        when(basketService.getAll()).thenReturn(testMap);
        String dateStr = "80/10/10/10/2010";
        when(reader.readLine()).thenReturn(dateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test(expected = ShopException.class)
    public void shouldBThrowExceptionIfDateLargeNow() throws IOException, ShopException {
        Map<SmokingTool, Integer> testMap = new HashMap<>();
        testMap.put(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false), 1);
        when(basketService.getAll()).thenReturn(testMap);
        String dateStr = "10/10/10/10/" + (Calendar.getInstance().get(Calendar.YEAR) + 1);
        when(reader.readLine()).thenReturn(dateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test
    public void shouldSetTitleParameter() throws IOException, ShopException {
        Map<SmokingTool, Integer> testMap = new HashMap<>();
        testMap.put(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false), 1);
        when(basketService.getAll()).thenReturn(testMap);
        String dateStr = "10/10/10/10/2010";
        when(reader.readLine()).thenReturn(dateStr);
        request.setAttribute(readerStr, reader);
        command.execute(request, response);

        String titleStr = "title";
        assertTrue(response.getAttribute(titleStr) instanceof String);
    }

}
