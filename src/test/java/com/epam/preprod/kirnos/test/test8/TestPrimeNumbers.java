package com.epam.preprod.kirnos.test.test8;

import com.epam.preprod.kirnos.task8.util.IntDiapason;
import com.epam.preprod.kirnos.task8.PrimeNumbersInDiapason;
import com.epam.preprod.kirnos.task8.util.PrimeNumber;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class TestPrimeNumbers {

    @Test
    public void shouldReturnCorrectDiapasons(){
        int count = 4;
        int begin = 1;
        int end = 13;
        PrimeNumbersInDiapason primeNumbers = new PrimeNumbersInDiapason();
        IntDiapason[] diapasons = PrimeNumber.distribution(begin, end, count);
        int expected = end - begin;

        int actual = 0;
        for (IntDiapason diapason : diapasons) {
            actual += diapason.getEnd() - diapason.getBegin();
        }
        assertEquals(actual, expected);
    }

    @Test
    public void shouldReturnCorrectCountDiapasons(){
        int count = 3;
        PrimeNumbersInDiapason primeNumbers = new PrimeNumbersInDiapason();
        IntDiapason[] diapasons = PrimeNumber.distribution(0, 7, count);

        assertTrue(count == diapasons.length);
    }


}
