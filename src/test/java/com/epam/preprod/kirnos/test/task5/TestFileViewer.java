package com.epam.preprod.kirnos.test.task5;

import com.epam.preprod.kirnos.task5.util.FileViewer;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestFileViewer {

    private FileViewer fileViewer;
    private FileViewer emptyFileViewer;

    @Before
    public void before() {
        String fileName = "src/test/resource/test.txt";
        String emptyFileName = "src/test/resource/empty.txt";
        fileViewer = new FileViewer(fileName);
        emptyFileViewer = new FileViewer(emptyFileName);
    }

    @Test
    public void suchReturnAllLinerFromFile() {
        int expected = 3;
        int actual = 0;
        for (String ignored : fileViewer) {
            actual++;
        }

        assertEquals(expected, actual);
    }

    @Test(expected = NoSuchElementException.class)
    public void suchThrowExceptionWhenNoMoreElements() {
        Iterator<String> iterator = fileViewer.iterator();
        while (iterator.hasNext()) {
            iterator.next();
        }
        iterator.next();
    }

    @Test
    public void testIteratorHasNextOnAnEmptyCollection() {
        Iterator<String> iterator = emptyFileViewer.iterator();

        boolean actual = iterator.hasNext();

        assertFalse(actual);
    }

    @Test
    public void testIteratorHasNextOnAnNotEmptyCollection() {
        Iterator<String> iterator = fileViewer.iterator();

        boolean actual = iterator.hasNext();

        assertTrue(actual);
    }
}
