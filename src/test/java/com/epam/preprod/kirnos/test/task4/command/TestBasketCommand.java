package com.epam.preprod.kirnos.test.task4.command;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.command.impl.BasketCommand;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task4.service.impl.BasketService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBasketCommand {

    private Command command;
    private IBasketService basketService;
    private Request request;
    private Response response;

    @Before
    public void before(){
        basketService = Mockito.mock(BasketService.class);
        command = new BasketCommand(basketService);

        request = new Request();
        response = new Response();
    }

    @Test
    public void shouldBeCallGetAllService() throws IOException, ShopException {
        command.execute(request, response);
        verify(basketService).getAll();
    }


    @Test
    public void shouldSetProductsParameter() throws IOException, ShopException {
        Map<SmokingTool, Integer> testMap = new HashMap<>();
        testMap.put(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false), 1);

        when(basketService.getAll()).thenReturn(testMap);
        command.execute(request, response);

        String products = "products";
        assertEquals(testMap, response.getAttribute(products));
    }

    @Test
    public void shouldSetTitleParameter() throws IOException, ShopException {
        command.execute(request, response);

        String title = "title";
        assertTrue(response.getAttribute(title) instanceof String);
    }


}
