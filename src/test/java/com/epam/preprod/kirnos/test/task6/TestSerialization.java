package com.epam.preprod.kirnos.test.task6;

import com.epam.preprod.kirnos.task1.entity.ManagementType;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task1.entity.SteamSystem;
import com.epam.preprod.kirnos.task1.entity.Vape;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RunWith(value = Parameterized.class)
public class TestSerialization {

    private int count;
    private List<SmokingTool> smokingTools;

    @Before
    public void before() {
        smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("1", "vape", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("2", "Electric vape", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));
    }

    @Parameterized.Parameters
    public static Collection dataParameters() {
        return Arrays.asList(1, 5, 10, 15, 20, 25, 30);
    }

    /**
     *
     * @param count
     */
    public TestSerialization(int count) {
        this.count = count;
    }

    @Test
    public void repeatedSerialization() throws IOException {
        File file = new File("tmp.data");
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            for (int i = 0; i < count; i++) {
                for (SmokingTool st : smokingTools) {
                    out.writeObject(st);
                }
            }
            out.flush();
        }
        System.out.println("Size(" + count + ") = " + file.length());
        file.delete();
    }

    @Test
    public void repeatedZipSerialization() throws IOException {
        File file = new File("compressed.zip");
        FileOutputStream fos = new FileOutputStream(file);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        ZipEntry zipEntry = new ZipEntry("objectStream.data");
        zipOut.putNextEntry(zipEntry);
        ObjectOutputStream out = new ObjectOutputStream(zipOut);
        for (int i = 0; i < count; i++) {
            for (SmokingTool st : smokingTools) {
                out.writeObject(st);
            }
        }
        out.flush();
        out.close();
        zipOut.close();
        fos.close();
        System.out.println("SizeZ(" + count + ") = " + file.length());
        file.delete();
    }

}
