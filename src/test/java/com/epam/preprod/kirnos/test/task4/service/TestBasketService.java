package com.epam.preprod.kirnos.test.task4.service;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.dao.BasketDao;
import com.epam.preprod.kirnos.task4.dao.HistoryDao;
import com.epam.preprod.kirnos.task4.dao.OrderDao;
import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task4.service.impl.BasketService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestBasketService {

    private IBasketService basketService;

    private BasketDao basketDao;
    private SmokingToolDao smokingToolDao;
    private HistoryDao historyDao;
    private OrderDao orderDao;
    private List<SmokingTool> smokingTools;

    @Before
    public void before() throws DBException {
        basketDao = Mockito.mock(BasketDao.class);
        historyDao = Mockito.mock(HistoryDao.class);
        smokingToolDao = Mockito.mock(SmokingToolDao.class);
        orderDao = Mockito.mock(OrderDao.class);
        basketService = new BasketService(basketDao, smokingToolDao, historyDao, orderDao);

        smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("B", "B", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));

        Map<String, Integer> insertMap = new HashMap<>();
        insertMap.put(smokingTools.get(0).getId(), 1);
        insertMap.put(smokingTools.get(1).getId(), 1);
        when(basketDao.getAll()).thenReturn(insertMap);
        when(smokingToolDao.getSmokingToolById(smokingTools.get(0).getId())).thenReturn(smokingTools.get(0));
        when(smokingToolDao.getSmokingToolById(smokingTools.get(1).getId())).thenReturn(smokingTools.get(1));
    }


    @Test
    public void shouldReturnAllElementsFromBasket() throws ShopException {
        int expectedCount = 2;

        Map<SmokingTool, Integer> basketItems = basketService.getAll();
        int actualCount = basketItems.size();

        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void shouldBeCalledDaoMethodsWhenAdd() throws ShopException {
        basketService.addSmokingTool(smokingTools.get(0));

        verify(basketDao).addSmokingTool(any());
        verify(historyDao).add(anyString(), any());
    }

    @Test
    public void shouldBeCalledBasketClearWhenByAll() throws ShopException {
        basketService.addSmokingTool(smokingTools.get(0));
        basketService.buyAll(new Date());

        verify(smokingToolDao).getSmokingToolById(smokingTools.get(0).getId());
        verify(orderDao).addOrder(any(), any());
        verify(basketDao).clear();
    }

    @Test
    public void shouldReturnCorrectSumWhenByAll() throws ShopException {
        double expectedSum = 0;
        for (SmokingTool tool : smokingTools) {
            expectedSum += tool.getPrice();
        }
        double actualSum = basketService.buyAll(new Date());

        assertEquals(expectedSum, actualSum, 0.01);
    }

}
