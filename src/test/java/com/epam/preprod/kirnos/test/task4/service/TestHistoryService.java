package com.epam.preprod.kirnos.test.task4.service;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.dao.HistoryDao;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IHistoryService;
import com.epam.preprod.kirnos.task4.service.impl.HistoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestHistoryService {

    private IHistoryService historyService;
    private HistoryDao historyDao;
    private List<SmokingTool> smokingTools;

    @Before
    public void before() {
        historyDao = Mockito.mock(HistoryDao.class);
        historyService = new HistoryService(historyDao);

        smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("B", "B", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));
    }

    @Test
    public void shouldBeCalledHistoryDaoGetAllWhenGetHistory() throws ShopException {
        historyService.getLastFive();

        verify(historyDao).getAll();
    }

    @Test
    public void shouldReturnCorrectListWhenGetHistory() throws ShopException {
        int expectedCount = 1;
        Map<String, SmokingTool> returnMap = new HashMap<>();
        returnMap.put("A", smokingTools.get(0));
        when(historyDao.getAll()).thenReturn(returnMap);

        List<SmokingTool> smokingTools = historyService.getLastFive();
        int actualCount = smokingTools.size();

        assertEquals(expectedCount, actualCount);
    }

}
