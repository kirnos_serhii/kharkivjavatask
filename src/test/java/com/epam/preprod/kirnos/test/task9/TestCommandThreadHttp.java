package com.epam.preprod.kirnos.test.task9;

import com.epam.preprod.kirnos.task1.entity.ManagementType;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task1.entity.SteamSystem;
import com.epam.preprod.kirnos.task1.entity.Vape;
import com.epam.preprod.kirnos.task4.dao.impl.MemoSmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.impl.SmokingToolService;
import com.epam.preprod.kirnos.task9.Server;
import com.epam.preprod.kirnos.task9.commnd.Command;
import com.epam.preprod.kirnos.task9.commnd.impl.CountCommand;
import com.epam.preprod.kirnos.task9.commnd.impl.ProductCommand;
import com.epam.preprod.kirnos.task9.thread.ThreadFactory;
import com.epam.preprod.kirnos.task9.thread.http.ThreadFactoryHttp;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class TestCommandThreadHttp {

    private Server server;
    private Command commandCount;
    private Command commandItem;
    private BufferedReader br;
    private BufferedWriter bw;
    private List<SmokingTool> smokingTools;

    @Before
    public void before() throws IOException {
        smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("B", "B", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));
        SmokingToolService smokingToolService = new SmokingToolService(new MemoSmokingToolDao(smokingTools));
        Map<String, Command> commands = new HashMap<>();
        commandCount = spy(new CountCommand(smokingToolService));
        commandItem = spy(new ProductCommand(smokingToolService));
        commands.put("item", commandItem);
        commands.put("count", commandCount);

        ThreadFactory threadFactory = new ThreadFactoryHttp(commands, 1000);
        server = new Server(threadFactory);
        server.start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Socket sock = new Socket("localhost", 1000);
        br = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
        bw = new BufferedWriter(
                new OutputStreamWriter(sock.getOutputStream()));
    }

    @After
    public void after(){
        server.exit();
        try {
            server.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldCallCountCommand() throws IOException, ShopException {
        bw.write("GET /shop/count HTTP/1.0");
        bw.newLine();
        bw.flush();

        br.readLine();

        verify(commandCount).execute(any(), any());
    }

    @Test
    public void shouldReturnCorrectResult() throws IOException {
        String expected = "size: " + smokingTools.size();
        bw.write("GET /shop/count HTTP/1.0");
        bw.newLine();
        bw.flush();

        StringBuilder actual = new StringBuilder();
        actual.append(br.readLine());
        while(br.ready()){
            actual.append("\n");
            actual.append(br.readLine());
        }

        assertTrue(actual.indexOf(expected) > 0);
    }

    @Test
    public void shouldReturn400ErrorIfThisCommandNotExist() throws IOException {
        String expected = "400";
        bw.write("GET /shop/list HTTP/1.0");
        bw.newLine();
        bw.flush();

        StringBuilder actual = new StringBuilder();
        actual.append(br.readLine());

        assertTrue(actual.indexOf(expected) > 0);
    }

    @Test
    public void shouldReturn500ErrorIfItemWithThisIdNotExist() throws IOException {
        String expected = "500";
        bw.write("GET /shop/item?get_info=123 HTTP/1.0");
        bw.newLine();
        bw.flush();

        StringBuilder actual = new StringBuilder();
        actual.append(br.readLine());

        System.out.println(actual);

        assertTrue(actual.indexOf(expected) > 0);
    }

}
