package com.epam.preprod.kirnos.test.task4.dao;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.dao.OrderDao;
import com.epam.preprod.kirnos.task4.dao.impl.MemoOrderDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class TestMemoOrderDao {

    private OrderDao orderDao;
    private List<SmokingTool> smokingTools;

    @Before
    public void before() {
        smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("1", "vape", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("2", "Electric vape", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));

        orderDao = new MemoOrderDao();
    }

    @Test
    public void shouldReturnAllItemsFromOrders() throws InterruptedException, DBException {
        int expected = smokingTools.size();
        for (SmokingTool smokingTool : smokingTools) {
            Map<String, Integer> insertMap = new HashMap<>();
            insertMap.put(smokingTool.getId(), 1);
            orderDao.addOrder(new Date(), insertMap);
            Thread.sleep(100);
        }

        int actual = orderDao.getAllOrders().size();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnOrdersByPeriod() throws ParseException, DBException {
        Map<String, Integer> insertMap = new HashMap<>();
        insertMap.put(smokingTools.get(0).getId(), 1);
        orderDao.addOrder(new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse("10/10/10/10/2010"), insertMap);
        insertMap = new HashMap<>();
        insertMap.put(smokingTools.get(1).getId(), 1);
        orderDao.addOrder(new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse("11/10/10/10/2010"), insertMap);
        Date firstDate = new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse("10/09/10/10/2010");
        Date secondDate = new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse("10/11/10/10/2010");
        int expected = 1;

        int actual = orderDao.getByPeriod(firstDate, secondDate).size();

        assertEquals(expected, actual);
    }

}
