package com.epam.preprod.kirnos.test.task4.command;

import com.epam.preprod.kirnos.task1.entity.*;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.command.impl.OrdersByPeriodCommand;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IOrderService;
import com.epam.preprod.kirnos.task4.service.impl.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestOrdersByPeriodCommand {

    private Command command;
    private IOrderService orderService;

    private Request request;
    private Response response;
    private String titleStr = "title";
    private String readerStr = "reader";
    private String productsStr = "products";
    private BufferedReader reader;

    @Before
    public void before() {
        orderService = Mockito.mock(OrderService.class);
        reader = Mockito.mock(BufferedReader.class);
        command = new OrdersByPeriodCommand(orderService);
        request = new Request();
        response = new Response();
    }

    @Test
    public void shouldBeCallServiceMethods() throws IOException, ShopException {
        String firstDateStr = "10/10/2010";
        String secondDateStr = "10/11/2011";
        when(reader.readLine()).thenReturn(firstDateStr).thenReturn(secondDateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);

        verify(orderService).getByPeriod(any(), any());
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionIfFirstDateIsIncorrect() throws IOException, ShopException {
        String firstDateStr = "40/10/2010";
        String secondDateStr = "10/11/2011";
        when(reader.readLine()).thenReturn(firstDateStr).thenReturn(secondDateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionIfFirstDateIsIncorrectLargeNow() throws IOException, ShopException {
        String firstDateStr = "10/10/" + (Calendar.getInstance().get(Calendar.YEAR) + 1);
        String secondDateStr = "10/10/2010";
        when(reader.readLine()).thenReturn(firstDateStr).thenReturn(secondDateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionIfFirstDateLessFirst() throws IOException, ShopException {
        String firstDateStr = "10/12/2010";
        String secondDateStr = "10/10/2010";
        when(reader.readLine()).thenReturn(firstDateStr).thenReturn(secondDateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionIfSecondDateIsIncorrect() throws IOException, ShopException {
        String firstDateStr = "10/10/2010";
        String secondDateStr = "40/11/2011";
        when(reader.readLine()).thenReturn(firstDateStr).thenReturn(secondDateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test(expected = ShopException.class)
    public void shouldThrowExceptionIfSecondDateIsIncorrectLargeNow() throws IOException, ShopException {
        String firstDateStr = "10/10/2010";
        String secondDateStr = "10/10/" + (Calendar.getInstance().get(Calendar.YEAR) + 1);
        when(reader.readLine()).thenReturn(firstDateStr).thenReturn(secondDateStr);
        request.setAttribute(readerStr, reader);

        command.execute(request, response);
    }

    @Test
    public void shouldSetCorrectParameters() throws IOException, ShopException, ParseException {
        String firstDateStr = "10/10/2010";
        String secondDateStr = "27/08/2019";
        when(reader.readLine()).thenReturn(firstDateStr).thenReturn(secondDateStr);
        request.setAttribute(readerStr, reader);
        String dateStr = "27/08/2019";
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
        Map<SmokingTool, Integer> tmpMap = new HashMap<>();
        tmpMap.put(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false), 1);
        Map<Date, Map<SmokingTool, Integer>> products = new HashMap<>();
        products.put(date, tmpMap);
        products.put(date, tmpMap);
        when(orderService.getByPeriod(any(), any())).thenReturn(products);

        command.execute(request, response);

        assertTrue(response.getAttribute(titleStr) instanceof String);
        assertEquals(products, response.getAttribute(productsStr));
    }

}
