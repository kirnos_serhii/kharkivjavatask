package com.epam.preprod.kirnos.test.task9;

import com.epam.preprod.kirnos.task1.entity.ManagementType;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task1.entity.SteamSystem;
import com.epam.preprod.kirnos.task1.entity.Vape;
import com.epam.preprod.kirnos.task4.dao.impl.MemoSmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.impl.SmokingToolService;
import com.epam.preprod.kirnos.task9.Server;
import com.epam.preprod.kirnos.task9.commnd.Command;
import com.epam.preprod.kirnos.task9.commnd.impl.CountCommand;
import com.epam.preprod.kirnos.task9.commnd.impl.ProductCommand;
import com.epam.preprod.kirnos.task9.thread.tcp.ThreadFactoryTcp;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class TestCommandThreadTcp {

    private static CountCommand countCommand;
    private static ProductCommand productCommand;
    private static List<SmokingTool> smokingTools;
    private static ThreadFactoryTcp threadFactoryTcp;

    @BeforeClass
    public static void before() {
        System.out.println("BEFORE");
        smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("B", "B", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));
        SmokingToolService smokingToolService = new SmokingToolService(new MemoSmokingToolDao(smokingTools));
        Map<String, Command> commands = new HashMap<>();
        countCommand = spy(new CountCommand(smokingToolService));
        productCommand = spy(new ProductCommand(smokingToolService));
        commands.put("count", countCommand);
        commands.put("item", productCommand);

        threadFactoryTcp = new ThreadFactoryTcp(commands, 3000);
        Server server = new Server(threadFactoryTcp);
        server.start();
    }

    @Test
    public void shouldCallCountCommand() throws ShopException, IOException {
        Socket sock = new Socket("localhost", 3000);
        BufferedReader br = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(sock.getOutputStream()));
        bw.write("get count");
        bw.newLine();
        bw.flush();

        br.readLine();

        verify(countCommand).execute(any(), any());
    }


    @Test
    public void shouldReturnCorrectMessageWhenIncorrectCommand() throws IOException {
        Socket sock = new Socket("localhost", 3000);
        BufferedReader br = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(sock.getOutputStream()));
        String expected = "Error";
        bw.write("get item=");
        bw.newLine();
        bw.flush();

        String actual = br.readLine();

        assertEquals(expected, actual.substring(0, 5));
    }

    @Test
    public void shouldReturnCorrectMessageWhenUnknownCommand() throws IOException {
        Socket sock = new Socket("localhost", 3000);
        BufferedReader br = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(sock.getOutputStream()));
        String expected = "Unknown command";
        bw.write("get it");
        bw.newLine();
        bw.flush();

        String actual = br.readLine();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnCorrectInfoMessageIfElementNotExist() throws IOException {
        Socket sock = new Socket("localhost", 3000);
        BufferedReader br = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(sock.getOutputStream()));
        String expected = "Error.Tool with this id do not exist.";
        bw.write("get item=111");
        bw.newLine();
        bw.flush();

        String actual = br.readLine();

        assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnCorrectCount() throws IOException {
        Socket sock = new Socket("localhost", 3000);
        BufferedReader br = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(sock.getOutputStream()));
        bw.write("get count");
        bw.newLine();
        bw.flush();

        String actual = br.readLine();

        assertEquals(String.valueOf(smokingTools.size()), actual);
    }

    @Test
    public void shouldReturnCurrentProductInfo() throws IOException {
        Socket sock = new Socket("localhost", 3000);
        BufferedReader br = new BufferedReader(
                new InputStreamReader(sock.getInputStream()));
        BufferedWriter bw = new BufferedWriter(
                new OutputStreamWriter(sock.getOutputStream()));
        SmokingTool smokingTool = smokingTools.get(0);
        String expected = smokingTool.getName() + "|" + String.format("%.2f", smokingTool.getPrice());
        bw.write("get item=" + smokingTool.getId());
        bw.newLine();
        bw.flush();

        String actual = br.readLine();

        assertEquals(expected, actual);
    }

}
