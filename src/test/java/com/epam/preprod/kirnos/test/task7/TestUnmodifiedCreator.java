package com.epam.preprod.kirnos.test.task7;

import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.Pipe;
import com.epam.preprod.kirnos.task1.entity.TypePipe;
import com.epam.preprod.kirnos.task1.entity.Wood;
import com.epam.preprod.kirnos.task7.proxy.create.impl.UnmodifiedCreator;
import com.epam.preprod.kirnos.task7.proxy.entity.IPipe;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestUnmodifiedCreator {

    @Test(expected = UnsupportedOperationException.class)
    public void shouldThrowExceptionTryEditObject() {
        IPipe pipe = new Pipe("id", "name", 10.0, "manufacturer", true,
                Material.COMBINED, Wood.BEECH, 10.2, TypePipe.MODERN);
        IPipe unmodifiedPipe = (IPipe) (new UnmodifiedCreator()).create(pipe);
        unmodifiedPipe.setName("newName");
    }

    @Test
    public void shouldCreateCorrectObject() {
        IPipe pipe = new Pipe("id", "name", 10.0, "manufacturer", true,
                Material.COMBINED, Wood.BEECH, 10.2, TypePipe.MODERN);
        IPipe unmodifiedPipe = (IPipe) (new UnmodifiedCreator()).create(pipe);

        assertEquals(pipe.getDiameter(), unmodifiedPipe.getDiameter(), 0.01);
    }

}
