package com.epam.preprod.kirnos.task3.util;

import com.epam.preprod.kirnos.task3.exception.NoUniqueElementException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class SingleArrayList<E> extends ArrayList<E> {


    public SingleArrayList(Collection<? extends E> inputCollection) {
        super();
        if (new HashSet<E>(inputCollection).size() != inputCollection.size()) {
            throw new NoUniqueElementException("Input collection contains no unique elements.");
        } else {
            addAll(inputCollection);
        }
    }

    public E set(int index, E element) {
        if (super.contains(element)) {
            throw new NoUniqueElementException("Collection has such element.");
        } else {
            return super.set(index, element);
        }
    }

    public boolean add(E element) {
        if (super.contains(element)) {
            throw new NoUniqueElementException("Collection has such element.");
        } else {
            return super.add(element);
        }
    }

    public void add(int index, E element) {
        if (super.contains(element)) {
            throw new NoUniqueElementException("Collection has such element.");
        } else {
            super.add(index, element);
        }
    }

    public boolean addAll(Collection<? extends E> inputCollection) {
        if(new HashSet<E>(inputCollection).size() < inputCollection.size()){
            throw new NoUniqueElementException("Input collection contains repetitive elements.");
        }
        this.forEach((e) -> {
            if (inputCollection.contains(e)) throw new NoUniqueElementException("Collection has such element.");
        });
        return super.addAll(inputCollection);
    }

    public boolean addAll(int index, Collection<? extends E> inputCollection) {
        if(new HashSet<E>(inputCollection).size() < inputCollection.size()){
            throw new NoUniqueElementException("Input collection contains repetitive elements.");
        }
        this.forEach((e) -> {
            if (inputCollection.contains(e)) throw new NoUniqueElementException("Collection has such element.");
        });
        return super.addAll(index, inputCollection);
    }

}
