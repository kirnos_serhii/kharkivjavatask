package com.epam.preprod.kirnos.task3.key;

public class HashSumFourKey extends HashKey {

    public HashSumFourKey(String key){
        super(key);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        for(int i = 0; i < 4; i++){
            if(i < key.length()){
                hash += key.charAt(i);
            }
        }
        return hash;
    }
}
