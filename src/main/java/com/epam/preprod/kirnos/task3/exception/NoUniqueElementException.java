package com.epam.preprod.kirnos.task3.exception;

public class NoUniqueElementException extends RuntimeException {
    public NoUniqueElementException() {
        super();
    }

    public NoUniqueElementException(String message) {
        super(message);
    }
}
