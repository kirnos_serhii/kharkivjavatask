package com.epam.preprod.kirnos.task3.key;

public class HashLenKey extends HashKey{

    public HashLenKey(String key){
        super(key);
    }

    @Override
    public int hashCode() {
        return key.length();
    }
}
