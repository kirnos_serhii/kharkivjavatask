package com.epam.preprod.kirnos.task3.key;

import java.util.Objects;

public abstract class HashKey {

    String key;

    public HashKey(String key){
        this.key = key;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HashSumFourKey that = (HashSumFourKey) o;
        return Objects.equals(key, that.key);
    }

    @Override
    public String toString(){
        return key;
    }

}
