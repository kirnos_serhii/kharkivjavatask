package com.epam.preprod.kirnos.task5.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class FileViewer implements Iterable<String> {

    private String filePath;

    public FileViewer(String filePath) {
        if (filePath == null) {
            throw new NullPointerException();
        }
        this.filePath = filePath;
    }

    @Override
    public Iterator<String> iterator() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
            return new FileIterator(bufferedReader);
        } catch (FileNotFoundException e) {
            throw new UncheckedIOException("File not found exception.", e);
        }

    }

    private static class FileIterator implements Iterator<String> {

        private BufferedReader bufferedReader;
        String nextLine;
        boolean isClose = false;

        FileIterator(BufferedReader bufferedReader) {
            this.bufferedReader = bufferedReader;
            try {
                nextLine = bufferedReader.readLine();
            } catch (IOException e) {
                // it impossible
                e.printStackTrace();
            }
        }

        @Override
        public boolean hasNext() {
            return nextLine != null;
        }

        @Override
        public String next() {
            if (nextLine != null) {
                String returnLine = nextLine;
                try {
                    nextLine = bufferedReader.readLine();
                } catch (IOException e) {
                    // it impossible
                    e.printStackTrace();
                }
                return returnLine;
            } else {
                if (!isClose) {
                    try {
                        bufferedReader.close();
                        isClose = true;
                    } catch (IOException e) {
                        // it impossible
                        e.printStackTrace();
                    }
                }
                throw new NoSuchElementException();
            }
        }

    }

}
