package com.epam.preprod.kirnos.task5.filter.impl;

import com.epam.preprod.kirnos.task5.filter.Filter;

import java.io.File;

class FileSizeFilter extends BaseFilter {

    private int minSize;
    private int maxSize;

    FileSizeFilter(Filter filter, int minSize, int maxSize) {
        super(filter);
        this.minSize = minSize;
        this.maxSize = maxSize;
    }

    @Override
    boolean predicate(File file) {
        return file.length() >= minSize && file.length() <= maxSize;
    }

}
