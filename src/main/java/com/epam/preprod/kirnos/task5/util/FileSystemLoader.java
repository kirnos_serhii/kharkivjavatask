package com.epam.preprod.kirnos.task5.util;

import java.io.File;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class FileSystemLoader {

    public static void getFilesByCatalog(File file, List<File> files) {
        if (file.isFile()) {
            files.add(file);
        } else if (file.isDirectory()) {
            File[] fileArr = file.listFiles();
            if(fileArr != null) {
                for(File f : fileArr) {
                    getFilesByCatalog(f, files);
                }
            }
            files.add(file);
        }
    }

    public static  void outFilesInfo(List<File> files, PrintStream os){
        os.println(String.format("| %-9s | %-20s | %-100s", "size", "date", "name"));
        for (File file : files) {
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm dd MM yyyy");
            String strDate = formatter.format(new Date(file.lastModified()));
            os.println(String.format("| %-9s | %-20s | %-200s", file.length(), strDate , file.getName()));
        }
    }

}
