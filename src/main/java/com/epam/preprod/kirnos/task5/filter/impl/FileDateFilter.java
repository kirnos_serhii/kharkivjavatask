package com.epam.preprod.kirnos.task5.filter.impl;

import com.epam.preprod.kirnos.task5.filter.Filter;

import java.io.File;
import java.util.Date;

class FileDateFilter extends BaseFilter {

    private Date firstDate;
    private Date secondDate;

    FileDateFilter(Filter nextFilter, Date firstDate, Date secondDate) {
        super(nextFilter);
        this.firstDate = firstDate;
        this.secondDate = secondDate;
    }

    @Override
    boolean predicate(File file) {
        return file.lastModified() >= firstDate.getTime() || file.lastModified() <= secondDate.getTime();
    }

}
