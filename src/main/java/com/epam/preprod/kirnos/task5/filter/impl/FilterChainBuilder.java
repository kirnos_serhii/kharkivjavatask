package com.epam.preprod.kirnos.task5.filter.impl;

import com.epam.preprod.kirnos.task5.filter.Filter;
import com.epam.preprod.kirnos.task5.filter.IFilterChainBuilder;

import java.util.Date;

public class FilterChainBuilder implements IFilterChainBuilder {

    private Filter filterChain = null;

    @Override
    public void addFilterByName(String name) {
        filterChain = new FileNameFilter(filterChain, name);
    }

    @Override
    public void addFilterByExtension(String name) {
        filterChain = new FileExtensionFilter(filterChain, name);
    }

    @Override
    public void addFilterBySize(int min, int max) {
        filterChain = new FileSizeFilter(filterChain, min, max);
    }

    @Override
    public void addFilterByDate(Date firstDate, Date secondDate) {
       filterChain = new FileDateFilter(filterChain, firstDate, secondDate);
    }

    @Override
    public Filter build() {
        return filterChain;
    }

}
