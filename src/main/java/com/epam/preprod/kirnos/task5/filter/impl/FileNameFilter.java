package com.epam.preprod.kirnos.task5.filter.impl;

import com.epam.preprod.kirnos.task5.filter.Filter;

import java.io.File;

public class FileNameFilter extends BaseFilter {

    private String name;

    FileNameFilter(Filter nextFilter, String name) {
        super(nextFilter);
        this.name = name;
    }

    @Override
    boolean predicate(File file) {
        String fileName = file.getName();
        int lastIndex = fileName.lastIndexOf(".");
        if (lastIndex <= 0) {
            if (name.equals(fileName)) {
                return true;
            }
        } else {
            if (name.equals(fileName.substring(0,lastIndex))) {
                return true;
            }
        }
        return false;
    }
}
