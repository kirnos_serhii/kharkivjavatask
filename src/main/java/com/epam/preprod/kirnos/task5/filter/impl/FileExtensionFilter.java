package com.epam.preprod.kirnos.task5.filter.impl;

import com.epam.preprod.kirnos.task5.filter.Filter;

import java.io.File;

class FileExtensionFilter extends BaseFilter {

    private String fileExtension;

    FileExtensionFilter(Filter nextFilter, String fileExtension){
        super(nextFilter);
        this.fileExtension = "." + fileExtension;
    }

    @Override
    boolean predicate(File file) {
        String fileName = file.getName();
        int lastIndex = fileName.lastIndexOf(".");
        return lastIndex > 0 && fileExtension.equals(fileName.substring(lastIndex));
    }

}
