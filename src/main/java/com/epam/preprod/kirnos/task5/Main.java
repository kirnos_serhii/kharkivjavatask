package com.epam.preprod.kirnos.task5;

import com.epam.preprod.kirnos.task4.util.Validator;
import com.epam.preprod.kirnos.task5.util.FileSystemLoader;
import com.epam.preprod.kirnos.task5.filter.Filter;
import com.epam.preprod.kirnos.task5.filter.impl.FilterChainBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws IOException {
        FilterChainBuilder filterChainBuilder = new FilterChainBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputLine;
        File file;
        Map<String, Command> commands = new LinkedHashMap<>();
        commands.put("> искать по имени файла ? (0\\1)", Main::searchByName);
        commands.put("> искать по расширению файла ? (0\\1)", Main::searchByExtension);
        commands.put("> искать по диапазону размеров ? (0\\1)", Main::searchBySize);
        commands.put("> искать по диапазону дат ? (0\\1)", Main::searchByDate);

        System.out.println("Введите каталог:");
        inputLine = reader.readLine();
        file = new File(inputLine);
        while (!file.exists() || !file.isDirectory()) {
            System.out.println("Неправельный ввод. Введите еще раз:");
            inputLine = reader.readLine();
            file = new File(inputLine);
        }
        try {
            for (Map.Entry<String, Command> command : commands.entrySet()) {
                System.out.println(command.getKey());
                inputLine = reader.readLine();
                while (!Validator.validate(inputLine, Validator.ZERO_ONE)) {
                    System.out.println("Введите еще раз:");
                    inputLine = reader.readLine();
                }
                if (inputLine.equals("1")) {
                    command.getValue().execute(reader, filterChainBuilder);
                }
            }
            // Get filter chain
            Filter filter = filterChainBuilder.build();

            // Extract all files from directory
            List<File> files = new LinkedList<>();
            FileSystemLoader.getFilesByCatalog(file, files);
            //Execute

            long startTime = System.currentTimeMillis();
            files = filter.doFilter(files);
            long endTime = System.currentTimeMillis();
            // Out result
            FileSystemLoader.outFilesInfo(files, System.out);

            System.out.println("Milliseconds: " + (endTime - startTime));
        } catch (ParseException ex) {
            System.out.println("Error parse.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error input.");
            ex.printStackTrace();
        }
    }

    interface Command {
        void execute(BufferedReader reader, FilterChainBuilder filterChainBuilder) throws IOException, ParseException;
    }

    private static void searchByName(BufferedReader reader, FilterChainBuilder filterChainBuilder) throws IOException {
        System.out.println("> Введите имя файла:");
        String line = reader.readLine();
        if (Validator.validate(line, Validator.NAME_FILE)) {
            filterChainBuilder.addFilterByName(line);
        } else {
            searchByName(reader, filterChainBuilder);
        }
    }

    private static void searchByExtension(BufferedReader reader, FilterChainBuilder filterChainBuilder) throws IOException {
        System.out.println("> Введите расширение файла:");
        String line = reader.readLine();
        if (Validator.validate(line, Validator.EXPANSION)) {
            filterChainBuilder.addFilterByExtension(line);
        } else {
            searchByExtension(reader, filterChainBuilder);
        }
    }

    private static void searchBySize(BufferedReader reader, FilterChainBuilder filterChainBuilder) throws IOException {
        System.out.println("> Введите минимальный размер файла (bytes):");
        String min = reader.readLine();
        if (Validator.validate(min, Validator.NUMBER_REGEX_WITHOUT_ZERO)) {
            System.out.println("> Введите максимальный размер файла (bytes):");
            String max = reader.readLine();
            if (Validator.validate(max, Validator.NUMBER_REGEX_WITHOUT_ZERO)) {
                filterChainBuilder.addFilterBySize(Integer.parseInt(min), Integer.parseInt(max));
                return;
            }
        }
        searchBySize(reader, filterChainBuilder);
    }

    private static void searchByDate(BufferedReader reader, FilterChainBuilder filterChainBuilder) throws IOException, ParseException {
        System.out.println("> Введите первую дату (dd/MM/yyyy):");
        String firstDateStr = reader.readLine();
        if (Validator.validate(firstDateStr, Validator.DATE_REGEX)) {
            System.out.println("> Введите вторую дату (dd/MM/yyyy):");
            String secondDateStr = reader.readLine();
            if (Validator.validate(secondDateStr, Validator.DATE_REGEX)) {
                Date firstDate = new SimpleDateFormat("dd/MM/yyyy").parse(firstDateStr);
                Date secondDate = new SimpleDateFormat("dd/MM/yyyy").parse(secondDateStr);
                if (firstDate.before(secondDate)) {
                    filterChainBuilder.addFilterByDate(firstDate, secondDate);
                    return;
                }
            }
        }
        System.out.println("Неправельный ввод.");
        searchByDate(reader, filterChainBuilder);
    }

}
