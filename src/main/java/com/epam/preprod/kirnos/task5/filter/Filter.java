package com.epam.preprod.kirnos.task5.filter;

import java.io.File;
import java.util.List;

public interface Filter {

    List<File> nextFilter(List<File> files);

    List<File> doFilter(List<File> files);

}
