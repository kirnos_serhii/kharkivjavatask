package com.epam.preprod.kirnos.task5.filter.impl;

import com.epam.preprod.kirnos.task5.filter.Filter;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseFilter implements Filter {

    private Filter nextFilter;

    BaseFilter(Filter nextFilter) {
        this.nextFilter = nextFilter;
    }

    public List<File> nextFilter(List<File> files) {
        if (nextFilter != null && !files.isEmpty()) {
            return nextFilter.doFilter(files);
        }
        return files;
    }

    public List<File> doFilter(List<File> files) {
        return nextFilter(files.parallelStream().filter(this::predicate).collect(Collectors.toList()));
    }

    abstract boolean predicate(File file);

}
