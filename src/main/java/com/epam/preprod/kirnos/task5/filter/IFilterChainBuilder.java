package com.epam.preprod.kirnos.task5.filter;

import java.io.IOException;
import java.util.Date;

public interface IFilterChainBuilder {

    Filter build() throws IOException;

    void addFilterByName(String name);

    void addFilterByExtension(String name);

    void addFilterBySize(int min, int max);

    void addFilterByDate(Date firstDate, Date secondDate);
}
