package com.epam.preprod.kirnos.task2.exception;

public class UnmodifiableDoubleCustomListClassException extends RuntimeException {

    public UnmodifiableDoubleCustomListClassException() {
        super();
    }

    public UnmodifiableDoubleCustomListClassException(String message) {
        super(message);
    }
}

