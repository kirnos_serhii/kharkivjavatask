package com.epam.preprod.kirnos.task2.util;

import java.util.*;
import java.util.function.Predicate;

public class CopyOnWriteCustomArrayList<E> implements List<E> {

    private Object[] dataArray;

    public CopyOnWriteCustomArrayList() {
        dataArray = new Object[0];
    }

    private Object[] recapacity(int countChange) {
        Object[] oldArray = dataArray;
        int newCapacity = dataArray.length + countChange;
        dataArray = Arrays.copyOf(dataArray, newCapacity);
        return oldArray;
    }

    @SuppressWarnings("unchecked")
    private E getData(int index)
    {
        return (E) dataArray[index];
    }

    @Override
    public boolean add(E e) {
        recapacity(1);
        dataArray[dataArray.length - 1] = e;
        return true;
    }

    @Override
    public void add(int index, E element) {
        indexCheckAdd(index);
        recapacity(1);
        for (int i = dataArray.length - 1; i > index; i--) {
            dataArray[i] = dataArray[i - 1];
        }
        dataArray[index] = element;
    }

    private void indexCheck(int index) {
        if (index < 0 || index >= dataArray.length) {
            throw new IndexOutOfBoundsException("Do not correct index. Such index does not exist.");
        }
    }

    private void indexCheckAdd(int index) {
        if (index < 0 || index > dataArray.length) {
            throw new IndexOutOfBoundsException("Do not correct index. Such index does not exist.");
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> inputCollection) {
        int oldSize = dataArray.length;
        recapacity(inputCollection.size());
        for (E object : inputCollection) {
            dataArray[oldSize++] = object;
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> inputCollection) {
        indexCheckAdd(index);
        Object[] oldArray = recapacity(inputCollection.size());
        int i = index;
        for (E object : inputCollection) {
            dataArray[i++] = object;
        }
        for (int j = index + inputCollection.size(); j < dataArray.length; j++) {
            dataArray[j] = oldArray[j - inputCollection.size()];
        }
        return true;
    }

    @Override
    public void clear() {
        recapacity(-1 * dataArray.length);
    }

    @Override
    public boolean contains(Object object) {
        return indexOf(object) != -1;
    }

    @Override
    public boolean containsAll(Collection<?> inputCollection) {
        for (Object object : inputCollection) {
            if (indexOf(object) == -1) {
                return false;
            }
        }
        return true;
    }

    @Override
    public E get(int index) {
        indexCheck(index);
        return getData(index);
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < dataArray.length; i++) {
                if (dataArray[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < dataArray.length; i++) {
                if (dataArray[i].equals(o)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return dataArray.length == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyFilterIterator(toArray());
    }

    public Iterator<E> iterator(Predicate<E> p) {
        return new MyFilterIterator(p, toArray());
    }

    @Override
    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = dataArray.length - 1; i >= 0; i--) {
                if (dataArray[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = dataArray.length - 1; i >= 0; i--) {
                if (dataArray[i].equals(o)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("Do not support listIterator by this CopyOnWriteCustomArrayList collection.");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("Do not support listIterator(int index) by CopyOnWriteCustomArrayList collection.");
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            for (int i = 0; i < dataArray.length; i++) {
                if (dataArray[i] == null) {
                    remove(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < dataArray.length; i++) {
                if (dataArray[i].equals(o)) {
                    remove(i);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public E remove(int index) {
        indexCheck(index);
        E oldValue = getData(index);
        Object[] oldArray = recapacity(-1);
        for (int i = index; i < oldArray.length - 1; i++) {
            dataArray[i] = oldArray[i + 1];
        }
        return oldValue;
    }

    @Override
    public boolean removeAll(Collection<?> inputCollection) {
        boolean ret = false;
        boolean[] flags = new boolean[dataArray.length];
        int countRemove = 0;
        M:
        for (int i = 0; i < dataArray.length; i++) {
            for (Object object : inputCollection) {
                if (object.equals(dataArray[i])) {
                    flags[i] = true;
                    ret = true;
                    countRemove++;
                    continue M;
                }
            }
        }
        Object[] oldArray = dataArray;
        dataArray = new Object[oldArray.length - countRemove];

        int offset = 0;
        for (int i = 0; i < dataArray.length; i++){
            dataArray[i] = oldArray[flags[i]?offset++:offset];
        }
        return ret;
    }

    @Override
    public boolean retainAll(Collection<?> inoutCollection) {
        boolean result = false;
        List<Object> tmp = new ArrayList();
        for (Object object : inoutCollection) {
            if (!this.contains(object)) {
                result = true;
                tmp.add(object);
            }
        }
        return result;
    }

    @Override
    public E set(int index, E element) {
        indexCheck(index);
        Object[] oldArray = recapacity(0);
        E oldValue = getData(index);
        dataArray[index] = element;
        return oldValue;
    }

    @Override
    public int size() {
        return dataArray.length;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Do not support subList by CopyOnWriteCustomArrayList collection.");
    }

    @Override
    public Object[] toArray() {
        return dataArray;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T[] toArray(T[] inputArray) {
        if (inputArray.length < dataArray.length) {
            return (T[]) Arrays.copyOf(dataArray, dataArray.length, inputArray.getClass());
        }
        System.arraycopy(dataArray, 0, inputArray, 0, dataArray.length);
        if (inputArray.length > dataArray.length) {
            inputArray[dataArray.length] = null;
        }
        return inputArray;
    }

    private class MyFilterIterator implements Iterator<E> {

        private int cursor;
        private int lastRet = -1;
        private Predicate<E> predicate;
        Object[] data;

        MyFilterIterator(Object[] data) {
            this.data = data;
        }

        MyFilterIterator(Predicate<E> p, Object[] data) {
            this.data = data;
            predicate = p;
            setCursor(0);
        }

        @Override
        public boolean hasNext() {
            return cursor < data.length;
        }

        @SuppressWarnings("unchecked")
        @Override
        public E next() {
            int i = cursor;
            if (i >= data.length) {
                throw new NoSuchElementException("Iteration has no more elements.");
            }
            setCursor(cursor + 1);
            return (E) data[lastRet = i];
        }

        @Override
        public void remove() {
            if (lastRet < 0) {
                throw new IllegalStateException("Method has not	yet been called, or the remove() method has "
                        + "already been called after the last call to the next() method");
            }
            CopyOnWriteCustomArrayList.this.remove(lastRet);
            setCursor(lastRet);
            lastRet = -1;
        }

        private void setCursor(int start) {
            if (predicate == null) {
                cursor = start;
            } else {
                for (int i = start; i < data.length; i++) {
                    if (predicate.test(CopyOnWriteCustomArrayList.this.get(i))) {
                        cursor = i;
                        return;
                    }
                }
                cursor = data.length;
            }
        }

    }
}
