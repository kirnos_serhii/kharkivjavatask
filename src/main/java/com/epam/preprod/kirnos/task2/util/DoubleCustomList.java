package com.epam.preprod.kirnos.task2.util;

import com.epam.preprod.kirnos.task2.exception.UnmodifiableDoubleCustomListClassException;

import java.util.*;

public class DoubleCustomList<T> implements List<T> {

    private List<T> unmodifiedList;
    private List<T> modifiedList;

    public DoubleCustomList(List<T> unmodifiedList, List<T> modifiedList) {
        if (unmodifiedList != null && modifiedList != null) {
            this.unmodifiedList = unmodifiedList;
            this.modifiedList = modifiedList;
        } else {
            throw new NullPointerException("Can not contain null list.");
        }
    }

    private void indexCheck(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException("Do not correct index. Such index does not exist.");
        }
    }

    private void indexCheckAdd(int index) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException("Do not correct index. Such index does not exist.");
        }
    }

    private int getRealIndex(int index) {
        if (index < unmodifiedList.size()) {
            throw new UnmodifiableDoubleCustomListClassException("Do no can edit unmodified part of list.");
        }
        return index - unmodifiedList.size();
    }

    @Override
    public int size() {
        return unmodifiedList.size() + modifiedList.size();
    }

    @Override
    public boolean isEmpty() {
        return unmodifiedList.isEmpty() && modifiedList.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return unmodifiedList.contains(o) || modifiedList.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return new DoubleCustomListIterator();
    }

    @Override
    public Object[] toArray() {
        Object[] resultArray = new Object[size()];
        System.arraycopy(unmodifiedList.toArray(), 0, resultArray, 0, unmodifiedList.size());
        System.arraycopy(modifiedList.toArray(), 0, resultArray, unmodifiedList.size(), modifiedList.size());
        return resultArray;
    }

    @Override
    public <E> E[] toArray(E[] inputArray) {
        if (inputArray.length < size()) {
            return (E[]) Arrays.copyOf(toArray(), size(), inputArray.getClass());
        }

        System.arraycopy(toArray(), 0, inputArray, 0, size());
        if (inputArray.length > size()) {
            inputArray[size()] = null;
        }
        return inputArray;
    }

    @Override
    public boolean add(T element) {
        return modifiedList.add(element);
    }

    @Override
    public boolean remove(Object o) {
        int elementIndex = indexOf(o);

        if (elementIndex != -1) {
            int realIndex = getRealIndex(elementIndex);
            modifiedList.remove(realIndex);
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> inputCollection) {
        for (Object obj: inputCollection) {
            boolean f = false;
            for (int i = 0; i < size(); i++) {
                if (i < unmodifiedList.size()) {
                    if(obj.equals(unmodifiedList.get(i))){
                        f = true;
                        break;
                    }
                } else {
                    if( obj.equals(modifiedList.get(i - unmodifiedList.size()))){
                        f = true;
                        break;
                    }
                }
            }
            if (!f){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> inputCollection) {
        for (T object : inputCollection) {
            add(object);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> inputCollection) {
        indexCheckAdd(index);
        getRealIndex(index);

        int offset = 0;
        for (T object : inputCollection) {
            add(index + offset++, object);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> inputCollection) {
        boolean ret = false;
        for (Object object : inputCollection) {
            if (remove(object)) {
                ret = true;
            }
        }
        return ret;
    }

    @Override
    public void clear() {
        if (!unmodifiedList.isEmpty()) {
            throw new UnmodifiableDoubleCustomListClassException("Do no can edit unmodified part of list.");
        } else {
            modifiedList.clear();
        }
    }

    @Override
    public T get(int index) {
        indexCheck(index);
        if (index < unmodifiedList.size()){
            return unmodifiedList.get(index);
        } else {
            return modifiedList.get(index - unmodifiedList.size());
        }
    }

    @Override
    public T set(int index, T element) {
        indexCheck(index);
        int realIndex = getRealIndex(index);
        return modifiedList.set(realIndex, element);

    }

    @Override
    public void add(int index, T element) {
        indexCheckAdd(index);
        int realIndex = getRealIndex(index);
        modifiedList.add(realIndex, element);
    }

    @Override
    public T remove(int index) {
        indexCheck(index);
        int realIndex = getRealIndex(index);
        return modifiedList.remove(realIndex);
    }

    @Override
    public int indexOf(Object o) {
        int index = unmodifiedList.indexOf(o);
        if (index != -1) {
            return index;
        }
        return modifiedList.indexOf(o) + unmodifiedList.size();
    }

    @Override
    public int lastIndexOf(Object o) {
        int lastIndex = modifiedList.lastIndexOf(o);
        if (lastIndex != -1) {
            return lastIndex + unmodifiedList.size();
        }
        return unmodifiedList.lastIndexOf(o);
    }

    @Override
    public ListIterator<T> listIterator() {
        throw new UnsupportedOperationException("Do not support listIterator by this DoubleCustomList collection.");
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        throw new UnsupportedOperationException("Do not support listIterator(int index) by DoubleCustomList collection.");
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException("Do not support subList(int fromIndex, int toIndex) by DoubleCustomList collection.");
    }

    @Override
    public boolean retainAll(Collection<?> inputCollection) {
        boolean result = false;
        for (Object object : inputCollection) {
            if (!this.contains(object)) {
                result = true;
                while (this.remove(object));
            }
        }
        return result;
    }

    private class DoubleCustomListIterator implements Iterator<T> {

        private int cursor;

        @Override
        public boolean hasNext() {
            return cursor < size();
        }

        @Override
        public T next() {
            if (cursor >= size()) {
                throw new NoSuchElementException("Iteration has no more elements.");
            }
            int i = cursor;
            cursor++;
            if (i < unmodifiedList.size()) {
                return unmodifiedList.get(i);
            } else {
                return modifiedList.get(i - unmodifiedList.size());
            }
        }

    }

}


