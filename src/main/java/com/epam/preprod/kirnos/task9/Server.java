package com.epam.preprod.kirnos.task9;

import com.epam.preprod.kirnos.task9.thread.ThreadFactory;
import com.epam.preprod.kirnos.task9.thread.http.CommandThreadHttp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server extends Thread {

    private static final Logger log = LoggerFactory.getLogger(CommandThreadHttp.class);

    private ThreadFactory threadFactory;
    private ServerSocket serverSock;

    public Server(ThreadFactory threadFactory) {
        this.threadFactory = threadFactory;
    }

    @Override
    public void run() {
        try {
            serverSock = new ServerSocket(threadFactory.getPort());
            while (true) {
                Socket sock = serverSock.accept();
                sock.setTcpNoDelay(true);
                Thread thread = threadFactory.getThread(sock);
                thread.start();
            }
        } catch (SocketException ignored){
            log.debug(ignored.getLocalizedMessage(), ignored);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exit(){
        log.debug("Close server");
        try {
            serverSock.close();
        } catch (IOException e) {
           log.error(e.getMessage(), e);
        }
    }

}
