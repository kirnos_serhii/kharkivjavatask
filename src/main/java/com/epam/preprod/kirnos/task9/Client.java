package com.epam.preprod.kirnos.task9;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Client extends Thread {

    public static void main(String[] args) {
        Client client = new Client();
        client.start();
    }

    public void run() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
             Socket sock = new Socket("localhost", 3000);
             BufferedReader br = new BufferedReader(
                     new InputStreamReader(sock.getInputStream()));
             BufferedWriter bw = new BufferedWriter(
                     new OutputStreamWriter(sock.getOutputStream()))) {
            String inputStr;
            String result;
            System.out.println("Input command:");
            inputStr = reader.readLine();
            bw.write(inputStr);
            bw.newLine();
            bw.flush();
            result = br.readLine();
            System.out.println("Result:" + result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
