package com.epam.preprod.kirnos.task9.thread;

import com.epam.preprod.kirnos.task9.commnd.Command;

import java.net.Socket;
import java.util.Map;

public abstract class ThreadFactory {

    protected Map<String, Command> commands;
    private int port;

    public ThreadFactory(Map<String, Command> commands, int port){
        this.commands = commands;
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public abstract Thread getThread(Socket socket);

}
