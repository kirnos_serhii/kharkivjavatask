package com.epam.preprod.kirnos.task9.commnd;

import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;

public interface Command {

    void execute(Request request,
                        Response response) throws ShopException;

}
