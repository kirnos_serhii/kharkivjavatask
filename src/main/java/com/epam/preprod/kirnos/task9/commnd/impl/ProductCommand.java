package com.epam.preprod.kirnos.task9.commnd.impl;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task9.commnd.Command;

public class ProductCommand implements Command {

    private ISmokingToolService smokingToolService;

    public ProductCommand(ISmokingToolService smokingToolService){
        this.smokingToolService = smokingToolService;
    }

    @Override
    public void execute(Request request, Response response) throws ShopException {
        String id = (String) request.getAttribute("get_info");
        if(id != null){
            SmokingTool smokingTool = smokingToolService.getSmokingToolById(id);
            response.setAttribute("name", smokingTool.getName());
            response.setAttribute("price", String.format("%.2f", smokingTool.getPrice()));
        } else{
            throw new ShopException("Incorrect Id.");
        }

    }

}
