package com.epam.preprod.kirnos.task9.thread.tcp;

import com.epam.preprod.kirnos.task9.commnd.Command;
import com.epam.preprod.kirnos.task9.thread.ThreadFactory;

import java.net.Socket;
import java.util.Map;

public class ThreadFactoryTcp extends ThreadFactory {

    public ThreadFactoryTcp(Map<String, Command> commands, int port) {
        super(commands, port);
    }

    @Override
    public Thread getThread(Socket socket) {
        return new CommandThreadTcp(commands, socket);
    }

}
