package com.epam.preprod.kirnos.task9.debugging;

import com.epam.preprod.kirnos.task1.entity.ManagementType;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task1.entity.SteamSystem;
import com.epam.preprod.kirnos.task1.entity.Vape;
import com.epam.preprod.kirnos.task4.dao.impl.MemoSmokingToolDao;
import com.epam.preprod.kirnos.task4.service.impl.SmokingToolService;
import com.epam.preprod.kirnos.task9.Server;
import com.epam.preprod.kirnos.task9.commnd.Command;
import com.epam.preprod.kirnos.task9.commnd.impl.CountCommand;
import com.epam.preprod.kirnos.task9.commnd.impl.ProductCommand;
import com.epam.preprod.kirnos.task9.thread.http.ThreadFactoryHttp;
import com.epam.preprod.kirnos.task9.thread.tcp.ThreadFactoryTcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StarterServer {

    private static ThreadFactoryTcp threadFactoryTcp;
    private static ThreadFactoryHttp threadFactoryHttp;

    public static void main(String[] args) {
        initContext();

        Server serverTcp = new Server(threadFactoryTcp);
        Server serverHttp = new Server(threadFactoryHttp);

        serverTcp.start();
        serverHttp.start();
    }

    private static void initContext() {
        List<SmokingTool> smokingTools = new ArrayList<>();
        smokingTools.add(new Vape("A", "A", 12.30, "VapePro", ManagementType.AUTOMATIC,
                10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER, false));
        smokingTools.add(new Vape("B", "B", 12.30, "VapePro",
                ManagementType.AUTOMATIC, 10, Material.COMBINED, 1.2, SteamSystem.CARTOMIZER,
                false));
        SmokingToolService smokingToolService = new SmokingToolService(new MemoSmokingToolDao(smokingTools));
        Map<String, Command> commands = new HashMap<>();
        commands.put("count", new CountCommand(smokingToolService));
        commands.put("item", new ProductCommand(smokingToolService));

        threadFactoryTcp = new ThreadFactoryTcp(commands, 3000);
        threadFactoryHttp = new ThreadFactoryHttp(commands, 1000);
    }

}
