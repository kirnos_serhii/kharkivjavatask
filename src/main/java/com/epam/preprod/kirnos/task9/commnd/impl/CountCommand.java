package com.epam.preprod.kirnos.task9.commnd.impl;

import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task9.commnd.Command;

public class CountCommand implements Command {

    private ISmokingToolService smokingToolService;

    public CountCommand(ISmokingToolService smokingToolService){
        this.smokingToolService = smokingToolService;
    }

    @Override
    public void execute(Request request, Response response) throws ShopException {
        response.setAttribute("size", String.format("%d",smokingToolService.getAllSmokingTool().size()));
    }

}
