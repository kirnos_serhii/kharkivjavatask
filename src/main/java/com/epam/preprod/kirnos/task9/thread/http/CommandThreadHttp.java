package com.epam.preprod.kirnos.task9.thread.http;

import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task9.commnd.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandThreadHttp extends Thread {

    private static final Logger log = LoggerFactory.getLogger(CommandThreadHttp.class);

    private Socket socket;
    private Map<String, Command> commands;

    public CommandThreadHttp(Map<String, Command> commands, Socket socket) {
        this.socket = socket;
        this.commands = commands;
    }

    @Override
    public void run() {
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(
                            socket.getOutputStream()));
            String st = br.readLine();

            System.out.println(st);
            if (st == null) {
                return;
            }
            String commandKey;
            String params;
            Pattern pattern = Pattern.compile(
                    "^GET /shop/(\\w+)(\\?(((\\w+)=([^&\\s]+))(&((\\w+)=([^&\\s]+)))*))?" +
                            " ((HTTP/1.0)|(HTTP/1.1))(\\n(.*))*$");
            Matcher matcher = pattern.matcher(st);
            if (matcher.find()) {
                commandKey = matcher.group(1);
                params = matcher.group(3);
            } else {
                bw.write(formatResult("Incorrect command", 400, ""));
                bw.newLine();
                bw.flush();
                return;
            }
            String resultString = executeCommand(commandKey, params);
            bw.write(resultString);
            bw.newLine();
            bw.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    private String executeCommand(String commandKey, String params) {
        Command command = commands.get(commandKey);
        if (command != null) {
            Request request = new Request();
            Response response = new Response();
            if (params != null) {
                for (String commandAndParam : params.split("$")) {
                    String parameter = commandAndParam.substring(0, commandAndParam.indexOf("="));
                    String value = commandAndParam.substring(commandAndParam.indexOf("=") + 1);
                    request.setAttribute(parameter, value);
                }
            }
            try {
                command.execute(request, response);
                Map<String, Object> responseParams = response.getParams();
                return formatResult("OK", 200, formatJson(responseParams));
            } catch (ShopException e) {
                log.debug("Shop exception", e);
                return formatResult("Error!", 500, "");
            }
        } else {
            return formatResult("Unknown command", 400, "");
        }
    }

    private String formatResult(String msg, int code, String body) {
        StringBuilder resultString = new StringBuilder();
        resultString.append("HTTP/1.1 ");
        resultString.append(code);
        resultString.append(" ");
        resultString.append(msg);
        resultString.append("\n");
        resultString.append("Content-type: application/json");
        if (body != null && !body.equals("")) {
            resultString.append("\n\n");
            resultString.append(body);
        }
        return resultString.toString();
    }

    private String formatJson(Map<String, Object> responseParams) {
        StringBuilder resultString = new StringBuilder();
        resultString.append("{");
        for (Map.Entry<String, Object> responseParam : responseParams.entrySet()) {
            resultString.append(responseParam.getKey());
            resultString.append(": ");
            resultString.append((String) responseParam.getValue());
            resultString.append(", ");
        }
        resultString.delete(resultString.length() - 2, resultString.length());
        resultString.append("}");
        return resultString.toString();
    }

}
