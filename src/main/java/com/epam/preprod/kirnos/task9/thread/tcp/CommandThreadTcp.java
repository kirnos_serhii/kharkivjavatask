package com.epam.preprod.kirnos.task9.thread.tcp;

import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task9.commnd.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandThreadTcp extends Thread {

    private static final Logger log = LoggerFactory.getLogger(CommandThreadTcp.class);

    private Socket socket;
    private Map<String, Command> commands;

    public CommandThreadTcp(Map<String, Command> commands, Socket socket) {
        this.socket = socket;
        this.commands = commands;
    }

    @Override
    public void run() {
        try {
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));
            BufferedWriter bw = new BufferedWriter(
                    new OutputStreamWriter(
                            socket.getOutputStream()));
            String st = br.readLine();

            Pattern pattern = Pattern.compile("get (\\w+)(=(.+))?");
            Matcher matcher = pattern.matcher(st);
            String commandKey;
            String param;
            if (matcher.find()) {
                commandKey = matcher.group(1);
                param = matcher.group(3);
            } else {
                bw.write("Incorrect command");
                bw.newLine();
                bw.flush();
                return;
            }
            String resultString = executeCommand(commandKey, param);
            bw.write(resultString);
            bw.newLine();
            bw.flush();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    private String executeCommand(String commandKey, String param) {
        Command command = commands.get(commandKey);
        if (command != null) {
            Request request = new Request();
            Response response = new Response();
            request.setAttribute("get_info", param);
            try {
                command.execute(request, response);
                Map<String, Object> params = response.getParams();
                StringBuilder resultString = new StringBuilder();
                for (Map.Entry<String, Object> responseParam : params.entrySet()) {
                    resultString.append((String) responseParam.getValue());
                    resultString.append("|");
                }
                return resultString.substring(0, resultString.length() - 1);
            } catch (ShopException e) {
                return "Error." + e.getMessage();
            }
        }else {
            return "Unknown command";
        }
    }

}

