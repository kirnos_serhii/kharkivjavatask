package com.epam.preprod.kirnos.task9.thread.http;

import com.epam.preprod.kirnos.task9.commnd.Command;
import com.epam.preprod.kirnos.task9.thread.ThreadFactory;

import java.net.Socket;
import java.util.Map;

public class ThreadFactoryHttp extends ThreadFactory {

    public ThreadFactoryHttp(Map<String, Command> commands, int port) {
        super(commands, port);
    }

    @Override
    public Thread getThread(Socket socket) {
        return new CommandThreadHttp(commands, socket);
    }
}
