package com.epam.preprod.kirnos.task8.util;

public class IntDiapasonImpl implements IntDiapason {
    private int beginValue;
    private int endValue;

    IntDiapasonImpl(int beginValue, int endValue) {
        this.beginValue = beginValue;
        this.endValue = endValue;
    }

    public int getBegin() {
        return beginValue;
    }

    public int getEnd() {
        return endValue;
    }
}