package com.epam.preprod.kirnos.task8.util;

public class PrimeNumber {

    public static boolean isPrime(int number) {
        if (number < 2) {
            return false;
        }
        if (number == 2){
            return  true;
        }
        for (int i = 3; i < number / 2 + 1; i++, i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static IntDiapason[] distribution(int begin, int end, int count) {
        IntDiapason[] diapasons = new IntDiapason[count];
        int mod = (end - begin) % count;
        int div = (end - begin) / count;
        int prev = begin;
        for (int i = 0; i < count; i++) {
            diapasons[i] = new IntDiapasonImpl(prev, prev += div + (mod > 0 ? mod - (--mod) : 0));
        }
        return diapasons;
    }

}
