package com.epam.preprod.kirnos.task8;


import com.epam.preprod.kirnos.task8.util.IntDiapason;
import com.epam.preprod.kirnos.task8.util.PrimeNumber;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class CallablePrimeNumbersSearcher implements Callable<List<Integer>> {

    private IntDiapason diapason;
    private List<Integer> primeNumbers;

    CallablePrimeNumbersSearcher(IntDiapason diapason) {
        this.diapason = diapason;
        primeNumbers = new ArrayList<>();
    }

    @Override
    public List<Integer> call() {
        for (int i = diapason.getBegin(); i < diapason.getEnd(); i++) {
            if (PrimeNumber.isPrime(i)) {
                primeNumbers.add(i);
            }
        }
        return primeNumbers;
    }
}