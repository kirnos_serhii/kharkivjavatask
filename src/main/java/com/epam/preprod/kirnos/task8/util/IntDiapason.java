package com.epam.preprod.kirnos.task8.util;

public interface IntDiapason {

    int getBegin();

    int getEnd();

}
