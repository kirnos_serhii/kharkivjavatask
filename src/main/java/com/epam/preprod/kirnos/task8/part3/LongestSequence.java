package com.epam.preprod.kirnos.task8.part3;

import com.epam.preprod.kirnos.task4.util.Validator;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class LongestSequence {

    private final static int COUNT_OF_THREADS = 50;
    private static BlockingQueue<String> messages;
    private static List<Task> tasks = new ArrayList<>();

    public static void main(String[] args) throws IOException, InterruptedException {
        List<SearchSequence> searchSequences = new ArrayList<>();
        for (int i = 0; i < COUNT_OF_THREADS; i++) {
            SearchSequence searchSequence = new SearchSequence(tasks);
            searchSequences.add(searchSequence);
            searchSequence.start();
        }

        String readLine = "+";
        while (readLine.equals("+")) {
            readLine = inputString("Input filename:", Validator.FILE);

            LongestFileSequence longestFileSequence = addTasks(readLine);
            waitAll();

            outResult(longestFileSequence);
            readLine = inputString("Continue? +/-:", Validator.PLUS_OR_MINUS);
        }

        for (SearchSequence searchSequence : searchSequences) {
            searchSequence.interrupt();
        }
    }

    private static void outResult(LongestFileSequence longestFileSequence) {
        System.out.println("Result: len = " + longestFileSequence.getLengthSequence()
                + " : (" + longestFileSequence.getFirstIndex()
                + " - " + longestFileSequence.getSecondIndex() + ");");
        System.out.println("Complete!");
    }

    private static void waitAll() throws InterruptedException {
        for (int i = 0; i < messages.size(); i++) {
            String msg = messages.take();
            System.out.println(msg);
        }
    }

    private static LongestFileSequence addTasks(String readLine) throws IOException {
        File file = new File(readLine);
        byte[] fileContent = Files.readAllBytes(file.toPath());
        LongestFileSequence longestFileSequence = new LongestFileSequence(fileContent);
        List<Task> tasksTmp = dividerTasks(longestFileSequence);
        int countTask = tasksTmp.size();
        messages = new ArrayBlockingQueue<>(countTask, true);
        synchronized (tasks) {
            tasks.addAll(tasksTmp);
            tasks.notifyAll();
        }
        return longestFileSequence;
    }

    private static String inputString(String msg, String validator) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String readLine;
        System.out.println(msg);
        readLine = reader.readLine();
        while (!Validator.validate(readLine, validator)) {
            System.out.println("Incorrect input filename. Reaped please.");
            readLine = reader.readLine();
        }
        return readLine;
    }

    /**
     * Utility method for creates Tasks with some count of elements
     *
     * @param fileSequence object for search that used in tasks
     * @return task list
     */
    private static List<Task> dividerTasks(LongestFileSequence fileSequence) {
        List<Task> tasks = new ArrayList<>();

        int up = fileSequence.getArray().length - 2;
        int down = 0;
        while (up > down) {
            Task task = new Task(fileSequence);
            task.addIndex(up--);
            task.addIndex(down++);
            tasks.add(task);
        }
        if (up == down) {
            Task task = new Task(fileSequence);
            task.addIndex(up);
            tasks.add(task);
        }
        return tasks;
    }

    private static class SearchSequence extends Thread {

        final List<Task> tasks;

        SearchSequence(List<Task> tasks) {
            this.tasks = tasks;
        }

        @Override
        public void run() {
            while (true) {
                Task task;
                synchronized (tasks) {
                    if (tasks.isEmpty()) {
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            return;
                        }
                    }
                    task = tasks.remove(tasks.size() - 1);
                }
                for (Integer index : task.getIndexes()) {
                    for (int i = index + 1; i < task.getFileSequence().getArray().length; i++) {
                        task.getFileSequence().add(index, i, check(task.getFileSequence().getArray(), index, i));
                    }
                }
                task.setComplete(true);
                try {
                    messages.put("Thread " + this.getName() + " complete task");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        private int check(byte[] array, int firstIndex, int secondIndex) {
            int len = 0;
            int diapason = Math.min(secondIndex - firstIndex, array.length - secondIndex);
            for (int i = 0; i < diapason; i++) {
                if (array[firstIndex++] == array[secondIndex++]) {
                    len++;
                } else {
                    return len;
                }
            }
            return len;
        }
    }

}
