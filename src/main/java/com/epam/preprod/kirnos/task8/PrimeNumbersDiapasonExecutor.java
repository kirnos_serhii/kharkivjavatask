package com.epam.preprod.kirnos.task8;

import com.epam.preprod.kirnos.task8.util.IntDiapason;
import com.epam.preprod.kirnos.task8.util.PrimeNumber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class PrimeNumbersDiapasonExecutor {

    private static ExecutorService executorService = Executors.newFixedThreadPool(5);

    public static void main(String[] arg) {
        PrimeNumbersDiapasonExecutor primeNumbersExecutor = new PrimeNumbersDiapasonExecutor();
        List<Integer> primeNumbersList;
        try {
            primeNumbersList = primeNumbersExecutor.search(0, 1000000, 5, TypeThread.CALLABLE);
            System.out.println(primeNumbersList.size());
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Error. Do not can find prime numbers.");
            e.printStackTrace();
        }
    }

    public List<Integer> search(int beginValue, int endValue, int count, TypeThread typeThread)
            throws InterruptedException, ExecutionException {
        if (beginValue < 0) {
            throw new PrimeNumbersIllegalArgumentException(beginValue);
        }
        int subLen = endValue - beginValue;
        if (subLen < 0) {
            throw new PrimeNumbersIllegalArgumentException(subLen);
        }
        if (count < 1) {
            throw new PrimeNumbersIllegalArgumentException(count);
        }

        if (subLen < count) {
            count = subLen;
        }
        IntDiapason[] diapasons = PrimeNumber.distribution(beginValue, endValue, count);
        List<Integer> synchronizedPrimeNumbersList;

        if (typeThread == TypeThread.RUNNABLE) {
            synchronizedPrimeNumbersList = runnableGetPrimeNumbersList(diapasons);
        } else {
            synchronizedPrimeNumbersList = callableGetPrimeNumbersList(diapasons);
        }
        return synchronizedPrimeNumbersList;
    }

    private List<Integer> runnableGetPrimeNumbersList(IntDiapason[] diapasons)
            throws InterruptedException {
        List<Integer> synchronizedPrimeNumbersList = Collections.synchronizedList(new ArrayList<>());
        for (IntDiapason diapason : diapasons) {
            executorService.submit(new RunnablePrimeNumbersSearcher(synchronizedPrimeNumbersList, diapason));
        }
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.MINUTES);
        return synchronizedPrimeNumbersList;
    }

    private List<Integer> callableGetPrimeNumbersList(IntDiapason[] diapasons) throws InterruptedException, ExecutionException {
        List<Integer> synchronizedPrimeNumbersList = new ArrayList<>();
        List<Callable<List<Integer>>> tasks = new ArrayList<>();
        for (IntDiapason diapason : diapasons) {
            tasks.add(new CallablePrimeNumbersSearcher(diapason));
        }

        List<Future<List<Integer>>> futures = executorService.invokeAll(tasks);
        executorService.shutdown();

        for (Future<List<Integer>> future : futures) {
            List<Integer> list = future.get();
            synchronizedPrimeNumbersList.addAll(list);
        }
        return synchronizedPrimeNumbersList;
    }

}
