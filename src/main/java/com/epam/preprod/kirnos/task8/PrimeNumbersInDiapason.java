package com.epam.preprod.kirnos.task8;

import com.epam.preprod.kirnos.task8.util.IntDiapason;
import com.epam.preprod.kirnos.task8.util.PrimeNumber;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class PrimeNumbersInDiapason {

    public static void main(String[] arg) {
        PrimeNumbersInDiapason primeNumbers = new PrimeNumbersInDiapason();
        List<Integer> primeNumbersList = primeNumbers.search(0, 1000000, 50, TypeThread.RUNNABLE);
        System.out.println(primeNumbersList.size());
    }

    public List<Integer> search(int beginValue, int endValue, int count, TypeThread typeThread) {
        if (beginValue < 0) {
            throw new PrimeNumbersIllegalArgumentException(beginValue);
        }
        int subLen = endValue - beginValue;
        if (subLen < 0) {
            throw new PrimeNumbersIllegalArgumentException(subLen);
        }
        if (count < 1) {
            throw new PrimeNumbersIllegalArgumentException(count);
        }
        if (subLen < count) {
            count = subLen;
        }

        IntDiapason[] diapasons = PrimeNumber.distribution(beginValue, endValue, count);
        List<Integer> synchronizedPrimeNumbersList;

        if (typeThread == TypeThread.RUNNABLE) {
            synchronizedPrimeNumbersList = runnableGetPrimeNumbersList(diapasons);
        } else {
            synchronizedPrimeNumbersList = callableGetPrimeNumbersList(diapasons);
        }

        return synchronizedPrimeNumbersList;
    }

    private List<Integer>  runnableGetPrimeNumbersList(IntDiapason[] diapasons){
        List<Integer> synchronizedPrimeNumbersList = Collections.synchronizedList(new ArrayList<>());
        Thread[] threads = new Thread[diapasons.length];
        for (int i = 0; i < diapasons.length; i++) {
            threads[i] = new Thread(new RunnablePrimeNumbersSearcher(synchronizedPrimeNumbersList, diapasons[i]));
            threads[i].start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return synchronizedPrimeNumbersList;
    }

    private List<Integer>  callableGetPrimeNumbersList(IntDiapason[] diapasons){
        List<Integer> synchronizedPrimeNumbersList = Collections.synchronizedList(new ArrayList<>());
        List<FutureTask<List<Integer>>> tasks = new ArrayList<>();
        for (int i = 0; i < diapasons.length; i++) {
            Callable<List<Integer>> callablePrimeNumbersSearcher = new CallablePrimeNumbersSearcher(diapasons[i]);
            tasks.add(new FutureTask<>(callablePrimeNumbersSearcher));
            new Thread(tasks.get(i)).start();
        }

        for (FutureTask<List<Integer>> task : tasks) {
            try {
                List<Integer> list = task.get();
                synchronizedPrimeNumbersList.addAll(list);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        return synchronizedPrimeNumbersList;
    }

}
