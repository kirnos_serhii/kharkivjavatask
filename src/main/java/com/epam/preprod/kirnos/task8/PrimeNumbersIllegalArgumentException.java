package com.epam.preprod.kirnos.task8;

public class PrimeNumbersIllegalArgumentException extends IllegalArgumentException {

    public PrimeNumbersIllegalArgumentException() {
        super();
    }

    public PrimeNumbersIllegalArgumentException(String s) {
        super(s);
    }

    public PrimeNumbersIllegalArgumentException(int index) {
        super("String index out of range: " + index);
    }

}
