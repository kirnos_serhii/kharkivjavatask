package com.epam.preprod.kirnos.task8.part3;

public class LongestFileSequence {

    private byte[] array;
    private int firstIndex;
    private int secondIndex;
    private int lengthSequence;

    public LongestFileSequence(byte[] array) {
        this.array = array;
    }

    public boolean add(int firstIndex, int secondIndex, int lengthSequence){
        if(lengthSequence > this.lengthSequence){
            synchronized (this){
                this.firstIndex = firstIndex;
                this.secondIndex = secondIndex;
                this.lengthSequence = lengthSequence;
            }
            return true;
        }
        return  false;
    }

    public byte[] getArray(){
        return array;
    }

    public int getFirstIndex() {
        return firstIndex;
    }

    public int getSecondIndex() {
        return secondIndex;
    }

    public int getLengthSequence() {
        return lengthSequence;
    }

}
