package com.epam.preprod.kirnos.task8;

import com.epam.preprod.kirnos.task8.util.IntDiapason;
import com.epam.preprod.kirnos.task8.util.PrimeNumber;

import java.util.List;

public class RunnablePrimeNumbersSearcher implements Runnable {

    private IntDiapason diapason;
    private List<Integer> primeNumbers;

    RunnablePrimeNumbersSearcher(List<Integer> primeNumbers, IntDiapason diapason) {
        this.diapason = diapason;
        this.primeNumbers = primeNumbers;
    }

    @Override
    public void run() {
        for (int i = diapason.getBegin(); i < diapason.getEnd(); i++) {
            if (PrimeNumber.isPrime(i)) {
                primeNumbers.add(i);
            }
        }
    }

}
