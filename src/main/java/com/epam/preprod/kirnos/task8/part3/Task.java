package com.epam.preprod.kirnos.task8.part3;

import java.util.ArrayList;
import java.util.List;

public class Task {

    private LongestFileSequence fileSequence;

    public List<Integer> getIndexes() {
        return indexes;
    }

    private List<Integer> indexes;
    private volatile boolean complete;

    public Task(LongestFileSequence fileSequence) {
        this.fileSequence = fileSequence;
        indexes = new ArrayList<>();
    }

    public void addIndex(int index){
        indexes.add(index);
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public LongestFileSequence getFileSequence() {
        return fileSequence;
    }

}
