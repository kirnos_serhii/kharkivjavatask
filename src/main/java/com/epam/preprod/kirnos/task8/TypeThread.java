package com.epam.preprod.kirnos.task8;

public enum TypeThread {
    RUNNABLE, CALLABLE
}
