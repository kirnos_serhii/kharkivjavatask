package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task4.util.Messages;
import com.epam.preprod.kirnos.task4.util.Validator;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;


public class BuyAllCommand implements Command {

    private IBasketService basketService;

    public BuyAllCommand(IBasketService basketService) {
        this.basketService = basketService;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException, IOException {
        Map<SmokingTool, Integer> basket = basketService.getAll();
        if(basket.isEmpty()){
            throw new ShopException("Basket is empty.");
        }

        BufferedReader reader = (BufferedReader) request.getAttribute("reader");
        System.out.println("Input date in format \"HH/mm/dd/MM/yyyy\":");
        String dateStr = reader.readLine();
        if (!Validator.validate(dateStr, Validator.TIME_AND_DATE_REGEX)) {
            throw new ShopException("Incorrect date input.");
        }
        Date date;
        try {
            date = new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse(dateStr);
            Date currentDate = new Date();
            if (currentDate.before(date)) {
                throw new ShopException("Date must be less current.");
            }
        } catch (ParseException ex) {
            throw new ShopException("Incorrect date format.");
        }

        Double sum = basketService.buyAll(date);
        response.setAttribute(Messages.TITLE, String.format("Order price: %.02f", sum));
        return CommandContainer.INFO_PAGE;

    }
}
