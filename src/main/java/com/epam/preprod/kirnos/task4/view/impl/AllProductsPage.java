package com.epam.preprod.kirnos.task4.view.impl;

import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.view.Page;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.List;

public class AllProductsPage implements Page {

    @Override
    public void execute(Response response) throws ShopException {
        List<SmokingTool> products = (List<SmokingTool>) response.getAttribute("products");
        System.out.println(response.getAttribute("title"));
        int i = 1;
        for (SmokingTool tool: products) {
            System.out.println("| " + i++  + " | " + tool);
        }
    }
}