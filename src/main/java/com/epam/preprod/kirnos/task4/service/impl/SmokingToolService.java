package com.epam.preprod.kirnos.task4.service.impl;

import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.List;

public class SmokingToolService implements ISmokingToolService {

    private SmokingToolDao smokingToolDao;

    public SmokingToolService(SmokingToolDao smokingToolDao) {
        this.smokingToolDao = smokingToolDao;
    }

    public List<SmokingTool> getAllSmokingTool() throws ShopException {
        return smokingToolDao.getAllSmokingTool();
    }

    public SmokingTool getSmokingToolById(String id) throws ShopException {
        return smokingToolDao.getSmokingToolById(id);
    }

    @Override
    public void addSmokingTool(SmokingTool smokingTool) throws ShopException {
        smokingToolDao.addSmokingTool(smokingTool);
    }
}
