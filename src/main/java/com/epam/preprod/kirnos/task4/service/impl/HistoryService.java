package com.epam.preprod.kirnos.task4.service.impl;

import com.epam.preprod.kirnos.task4.dao.HistoryDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task4.service.IHistoryService;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HistoryService implements IHistoryService {

    private HistoryDao historyDao;

    public HistoryService(HistoryDao historyDao){
        this.historyDao = historyDao;
    }

    @Override
    public List<SmokingTool> getLastFive() throws DBException {
        List<SmokingTool> smokingTools = new ArrayList<>();
        for(Map.Entry<String, SmokingTool> entry :historyDao.getAll().entrySet()){
            smokingTools.add(entry.getValue());
        }
        return smokingTools;
    }

}
