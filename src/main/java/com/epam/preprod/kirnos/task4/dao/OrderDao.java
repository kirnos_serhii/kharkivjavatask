package com.epam.preprod.kirnos.task4.dao;

import com.epam.preprod.kirnos.task4.exceptions.DBException;

import java.util.Date;
import java.util.Map;

public interface OrderDao {

    Map<Date, Map<String, Integer>> getAllOrders() throws DBException;

    void addOrder(Date date, Map<String, Integer> products) throws DBException;

    Map<Date, Map<String, Integer>> getByPeriod(Date firstDate, Date secondDate) throws DBException;
}
