package com.epam.preprod.kirnos.task4.view.impl;

import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.view.Page;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.Date;
import java.util.Map;

public class OrdersPage implements Page {

    @Override
    public void execute(Response response) throws ShopException {
        Map<Date, Map<SmokingTool, Integer>> products = (Map<Date, Map<SmokingTool, Integer>>) response.getAttribute("products");
        String title = (String) response.getAttribute("title");
        System.out.println(title);
        if(products.isEmpty()){
            System.out.println("No orders for a given period!");
        }
        for (Map.Entry<Date, Map<SmokingTool, Integer>> entry : products.entrySet()) {
            System.out.println("Date: " + entry.getKey());
            for (Map.Entry<SmokingTool, Integer> pair : entry.getValue().entrySet()) {
                System.out.println("->tool = " + pair.getKey());
                System.out.println("->count = " + pair.getValue());
            }
        }
    }
}
