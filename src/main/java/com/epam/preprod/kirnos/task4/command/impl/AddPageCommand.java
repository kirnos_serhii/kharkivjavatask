package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.util.Messages;
import com.epam.preprod.kirnos.task6.create.CreateContainer;

import java.util.Set;

public class AddPageCommand implements Command {

    @Override
    public CommandContainer execute(Request request, Response response) {
        Set<String> products = CreateContainer.getKeys();

        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "Products types");
        return CommandContainer.ALL_PRODUCTS_TYPE_PAGE;
    }
}
