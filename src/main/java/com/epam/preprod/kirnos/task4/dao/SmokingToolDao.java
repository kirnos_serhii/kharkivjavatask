package com.epam.preprod.kirnos.task4.dao;

import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.List;

public interface SmokingToolDao {

    public List<SmokingTool> getAllSmokingTool() throws DBException;

    public SmokingTool getSmokingToolById(String id) throws DBException;

    public void addSmokingTool(SmokingTool smokingTool) throws DBException;
}
