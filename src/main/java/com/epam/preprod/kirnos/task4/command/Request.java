package com.epam.preprod.kirnos.task4.command;

import java.util.Map;
import java.util.TreeMap;

public class Request {

    private Map<String, Object> commands = new TreeMap<>();

    public void setAttribute(String name, Object object) {
        commands.put(name, object);
    }

    public Object getAttribute(String name){
        return commands.get(name);
    }
}
