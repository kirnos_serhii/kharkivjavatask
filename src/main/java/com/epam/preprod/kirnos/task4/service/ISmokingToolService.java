package com.epam.preprod.kirnos.task4.service;

import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.List;

public interface ISmokingToolService {

    List<SmokingTool> getAllSmokingTool() throws ShopException;

    SmokingTool getSmokingToolById(String id) throws ShopException;

    void addSmokingTool(SmokingTool smokingTool) throws ShopException;
}
