package com.epam.preprod.kirnos.task4.controller;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.context.Context;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.util.Serializer;
import com.epam.preprod.kirnos.task4.view.Page;
import com.epam.preprod.kirnos.task4.view.TableView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ConsoleShop {

    private List<SmokingTool> smokingTools = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        ConsoleShop consoleShop = new ConsoleShop();
        consoleShop.start();
    }

    private void start() throws IOException {
        String fileName = "collection.data";
        try {
            Serializer.initParam(fileName, smokingTools);
        } catch (ShopException e) {
            System.out.println("Could not read collection from file. " + e.getMessage());
        }
        Context.init(smokingTools);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            CommandContainer commandContainer = CommandContainer.MAIN_PAGE;
            Page page = commandContainer.getPage();
            TableView tableView = new TableView();String inputData = "";
            Request request = new Request();
            request.setAttribute("reader", reader);
            Response response = new Response();
            response.setAttribute("title", "Main Page");
            while (!inputData.equals("done")) {
                if(!outPage(page, response)){
                    break;
                }
                tableView.show(commandContainer.getCommands());
                response = new Response();
                inputData = reader.readLine();
                Command command = commandContainer.get(inputData);
                try {
                    commandContainer = command.execute(request, response);
                    page = commandContainer.getPage();
                } catch (ShopException ex) {
                    String errorMessage = ex.getMessage();
                    response.setAttribute("errorMessage", errorMessage);
                    page = CommandContainer.ERROR_PAGE.getPage();
                }
            }
        } finally {
            try {
                Serializer.saveObjects(fileName, smokingTools);
                Context.doneServers();
            } catch (ShopException e) {
                System.out.println("Could not write collection in file. " + e.getMessage());
            }
        }
    }

    private boolean outPage(Page page, Response response){
        try {
            page.execute(response);
        } catch (ShopException ex) {
            System.out.println("Could not show result.");
            return false;
        }
        return true;
    }

}
