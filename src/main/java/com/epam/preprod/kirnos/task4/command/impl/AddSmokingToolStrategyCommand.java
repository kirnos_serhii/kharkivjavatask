package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task4.util.Messages;
import com.epam.preprod.kirnos.task6.create.CreateContainer;
import com.epam.preprod.kirnos.task6.create.SmokingToolStrategy;
import com.epam.preprod.kirnos.task6.create.StrategyContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class AddSmokingToolStrategyCommand implements Command {

    private ISmokingToolService smokingToolService;
    private StrategyContext strategyContext;

    public AddSmokingToolStrategyCommand(ISmokingToolService smokingToolService, StrategyContext strategyContext) {
        this.smokingToolService = smokingToolService;
        this.strategyContext = strategyContext;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException, IOException {
        BufferedReader reader = (BufferedReader) request.getAttribute("reader");
        System.out.println("Input type tool:");
        String type = reader.readLine();
        SmokingToolStrategy smokingToolStrategy = CreateContainer.get(type);
        while(smokingToolStrategy == null){
            System.out.println("Incorrect input. Reaped please.");
            smokingToolStrategy = CreateContainer.get(reader.readLine());
        }
        strategyContext.setSmokingToolStrategy(smokingToolStrategy);
        SmokingTool smokingTool = strategyContext.create();
        smokingToolService.addSmokingTool(smokingTool);

        List<SmokingTool> products = smokingToolService.getAllSmokingTool();

        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "All products");
        return CommandContainer.ALL_PRODUCTS_PAGE;
    }
}
