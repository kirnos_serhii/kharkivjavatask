package com.epam.preprod.kirnos.task4.service;

import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.Date;
import java.util.Map;

public interface IOrderService {

    Map<Date, Map<SmokingTool, Integer>> getByPeriod(Date firstDate, Date secondDate) throws ShopException;

    void addOrder(Date date, Map<String, Integer> order) throws ShopException;

    Map<Date, Map<SmokingTool, Integer>> getByDate(Date date) throws ShopException;
}
