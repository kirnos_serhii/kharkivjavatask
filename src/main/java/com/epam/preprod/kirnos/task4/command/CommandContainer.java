package com.epam.preprod.kirnos.task4.command;


import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.util.Validator;
import com.epam.preprod.kirnos.task4.view.Page;
import com.epam.preprod.kirnos.task4.view.impl.*;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public enum CommandContainer {

    MAIN_PAGE {
        {
            page = new TitlePage();
        }
    },
    ERROR_PAGE {
        {
            page = new ErrorPage();
        }
    },
    BASKET_PAGE {
        {
            page = new BasketPage();
        }
    },
    ALL_PRODUCTS_PAGE {
        {
            page = new AllProductsPage();
        }
    },
    INFO_PAGE {
        {
            page = new TitlePage();
        }
    }, ALL_PRODUCTS_WITHOUT_COMMANDS_PAGE {
        {
            page = new AllProductsPage();
        }
    }, ORDER_PAGE {
        {
            page = new OrdersPage();
        }
    }, ALL_PRODUCTS_TYPE_PAGE {
        {
            page = new AllProductsTypePage();
        }
    };

    protected Map<String, Command> commands = new LinkedHashMap<>();
    protected Page page;

    {
        this.commands.put("No Command", (request, response) -> {
            throw new ShopException("No such or incorrect command.");
        });
    }


    public Command get(String commandName) {
        try {
            if (!Validator.validate(commandName, Validator.NUMBER_REGEX)) {
                throw new NumberFormatException();
            }
            int numberCommand = Integer.parseInt(commandName);
            numberCommand++;
            int i = 0;

            for (Map.Entry<String, Command> entry : commands.entrySet()) {
                if (i == numberCommand) {
                    return entry.getValue();
                }
                i++;
            }
            return commands.get("No Command");
        } catch (NumberFormatException ex) {
            return commands.get("No Command");
        }
    }

    public void add(String nameCommand, Command command) {
        this.commands.put(nameCommand, command);
    }

    public Page getPage() {
        return page;
    }

    public List<String> getCommands() {
        List<String> commandsNameList = new ArrayList<>();
        for (String commandName : commands.keySet()) {
            if (!commandName.equals("No Command")) {
                commandsNameList.add(commandName);
            }
        }
        return commandsNameList;
    }

}
