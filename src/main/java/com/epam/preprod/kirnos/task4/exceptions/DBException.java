package com.epam.preprod.kirnos.task4.exceptions;

public class DBException extends ShopException {

    public DBException() {
        super();
    }

    public DBException(String message, Throwable cause) {
        super(message, cause);
    }

    public DBException(String message) {
        super(message);
    }
}
