package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IOrderService;
import com.epam.preprod.kirnos.task4.util.Messages;
import com.epam.preprod.kirnos.task4.util.Validator;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class OrdersByPeriodCommand implements Command {

    private IOrderService orderService;
    private static int MILLISECOND_IN_HOUR = 3600000;

    public OrdersByPeriodCommand(IOrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException, IOException {
        BufferedReader reader = (BufferedReader) request.getAttribute("reader");
        System.out.println("Input first date dd/MM/yyyy:");
        String firstDateStr = reader.readLine();
        if (!Validator.validate(firstDateStr, Validator.DATE_REGEX)) {
            throw new ShopException("Incorrect date input.");
        }
        Date firstDate;
        Date currentDate = new Date();
        try {
            firstDate = new SimpleDateFormat("dd/MM/yyyy").parse(firstDateStr);
            if (currentDate.before(firstDate)) {
                throw new ShopException("Date must be less current.");
            }
        } catch (ParseException ex) {
            throw new ShopException("Incorrect date format.");
        }

        System.out.println("Input second date dd/MM/yyyy:");
        String secondDateStr = reader.readLine();
        if (!Validator.validate(secondDateStr, Validator.DATE_REGEX)) {
            throw new ShopException("Incorrect date input.");
        }
        Date secondDate;
        try {
            secondDate = new SimpleDateFormat("dd/MM/yyyy").parse(secondDateStr);
            if (currentDate.before(secondDate)) {
                throw new ShopException("Date must be less current.");
            }
        } catch (ParseException ex) {
            throw new ShopException("Incorrect date format.");
        }


        if (!firstDate.before(secondDate)) {
            throw new ShopException("First date must be before second date.");
        }
        Map<Date, Map<SmokingTool, Integer>> products = orderService.getByPeriod(firstDate, new Date(secondDate.getTime()+MILLISECOND_IN_HOUR*24));
        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "Products by period.");
        return CommandContainer.ORDER_PAGE;
    }
}
