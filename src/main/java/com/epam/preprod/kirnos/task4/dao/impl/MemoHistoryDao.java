package com.epam.preprod.kirnos.task4.dao.impl;

import com.epam.preprod.kirnos.task4.dao.HistoryDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task4.util.HistoryMap;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.Map;

public class MemoHistoryDao implements HistoryDao {

    private Map<String, SmokingTool> history;

    public MemoHistoryDao() {
        history = new HistoryMap<>(5);
    }

    public MemoHistoryDao(int capacity) throws DBException {
        if(capacity < 0){
            throw new DBException("Incorrect capacity value.");
        }
        history = new HistoryMap<>(capacity);
    }

    @Override
    public void add(String id, SmokingTool smokingTool) throws DBException {
        if(id == null || smokingTool == null){
            throw new DBException("ID or smokingTool is null.");
        }
        history.put(id, smokingTool);
    }

    @Override
    public Map<String, SmokingTool> getAll() {
        return history;
    }
}
