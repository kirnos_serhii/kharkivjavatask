package com.epam.preprod.kirnos.task4.view.impl;

import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.view.Page;

import java.util.Set;

public class AllProductsTypePage implements Page {

    @Override
    public void execute(Response response) throws ShopException {
        Set<String> products = (Set<String>) response.getAttribute("products");
        System.out.println(response.getAttribute("title"));
        for (String str: products) {
            System.out.println(str);
        }
    }
}
