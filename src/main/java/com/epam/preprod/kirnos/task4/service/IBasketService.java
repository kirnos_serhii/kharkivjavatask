package com.epam.preprod.kirnos.task4.service;

import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.Date;
import java.util.Map;

public interface IBasketService {

    Map<SmokingTool, Integer> getAll() throws ShopException;

    void addSmokingTool(SmokingTool st) throws ShopException;

    Double buyAll(Date date) throws ShopException;

}
