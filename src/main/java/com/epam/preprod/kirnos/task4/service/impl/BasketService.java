package com.epam.preprod.kirnos.task4.service.impl;

import com.epam.preprod.kirnos.task4.dao.BasketDao;
import com.epam.preprod.kirnos.task4.dao.HistoryDao;
import com.epam.preprod.kirnos.task4.dao.OrderDao;
import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.*;

public class BasketService implements IBasketService {

    private BasketDao basketDao;
    private SmokingToolDao smokingToolDao;
    private HistoryDao historyDao;
    private OrderDao orderDao;

    public BasketService(BasketDao basketDao, SmokingToolDao smokingToolDao, HistoryDao historyDao, OrderDao orderDao) {
        this.basketDao = basketDao;
        this.smokingToolDao = smokingToolDao;
        this.historyDao = historyDao;
        this.orderDao = orderDao;
    }

    public Map<SmokingTool, Integer> getAll() throws ShopException {
        Map<SmokingTool, Integer> resultMap = new HashMap<>();
        Map<String, Integer> basketData = basketDao.getAll();
        for (Map.Entry<String, Integer> entry : basketData.entrySet()) {
            resultMap.put(smokingToolDao.getSmokingToolById(entry.getKey()), entry.getValue());
        }
        return resultMap;
    }

    public void addSmokingTool(SmokingTool st) throws ShopException {
        basketDao.addSmokingTool(st);
        historyDao.add(st.getId(), st);
    }

    public Double buyAll(Date date) throws ShopException {
        double sum = 0.0;
        Map<String, Integer> order = new HashMap<>();
        for (Map.Entry<String, Integer> entry : basketDao.getAll().entrySet()) {
            sum = sum + (smokingToolDao.getSmokingToolById(entry.getKey()).getPrice() * entry.getValue());
            order.put(entry.getKey(), entry.getValue());
        }
        orderDao.addOrder(date, order);
        basketDao.clear();
        return sum;
    }

}


