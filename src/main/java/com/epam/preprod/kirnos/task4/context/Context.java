package com.epam.preprod.kirnos.task4.context;

import com.epam.preprod.kirnos.task1.entity.Hookah;
import com.epam.preprod.kirnos.task1.entity.Pipe;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task1.entity.Vape;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.impl.AddPageCommand;
import com.epam.preprod.kirnos.task4.command.impl.AddProductInBasketCommand;
import com.epam.preprod.kirnos.task4.command.impl.AddSmokingToolReflectionCommand;
import com.epam.preprod.kirnos.task4.command.impl.AddSmokingToolStrategyCommand;
import com.epam.preprod.kirnos.task4.command.impl.AllProductsCommand;
import com.epam.preprod.kirnos.task4.command.impl.BasketCommand;
import com.epam.preprod.kirnos.task4.command.impl.BuyAllCommand;
import com.epam.preprod.kirnos.task4.command.impl.LastFiveInBasketCommand;
import com.epam.preprod.kirnos.task4.command.impl.MainPageCommand;
import com.epam.preprod.kirnos.task4.command.impl.OrdersByDateCommand;
import com.epam.preprod.kirnos.task4.command.impl.OrdersByPeriodCommand;
import com.epam.preprod.kirnos.task4.dao.BasketDao;
import com.epam.preprod.kirnos.task4.dao.HistoryDao;
import com.epam.preprod.kirnos.task4.dao.OrderDao;
import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.dao.impl.MemoBasketDao;
import com.epam.preprod.kirnos.task4.dao.impl.MemoHistoryDao;
import com.epam.preprod.kirnos.task4.dao.impl.MemoOrderDao;
import com.epam.preprod.kirnos.task4.dao.impl.MemoSmokingToolDao;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task4.service.IHistoryService;
import com.epam.preprod.kirnos.task4.service.IOrderService;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task4.service.impl.BasketService;
import com.epam.preprod.kirnos.task4.service.impl.HistoryService;
import com.epam.preprod.kirnos.task4.service.impl.OrderService;
import com.epam.preprod.kirnos.task4.service.impl.SmokingToolService;
import com.epam.preprod.kirnos.task6.create.CreateContainer;
import com.epam.preprod.kirnos.task6.create.HookahStrategy;
import com.epam.preprod.kirnos.task6.create.PipeStrategy;
import com.epam.preprod.kirnos.task6.create.StrategyContext;
import com.epam.preprod.kirnos.task6.create.VapeStrategy;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task6.input.impl.TypeInput;
import com.epam.preprod.kirnos.task7.create.ReflectionCreator;
import com.epam.preprod.kirnos.task7.create.TypesToolContainer;
import com.epam.preprod.kirnos.task7.properties.Locales;
import com.epam.preprod.kirnos.task7.properties.ResourceManager;
import com.epam.preprod.kirnos.task9.Server;
import com.epam.preprod.kirnos.task9.commnd.Command;
import com.epam.preprod.kirnos.task9.commnd.impl.CountCommand;
import com.epam.preprod.kirnos.task9.commnd.impl.ProductCommand;
import com.epam.preprod.kirnos.task9.thread.http.ThreadFactoryHttp;
import com.epam.preprod.kirnos.task9.thread.tcp.ThreadFactoryTcp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class Context {

    private static final Logger log = LoggerFactory.getLogger(Context.class);

    private static int PORT_TCP = 3000;
    private static int PORT_HTTP = 1000;

    private static Server serverTcp;
    private static Server serverHttp;

    public static void init(List<SmokingTool> smokingTools) throws IOException {
        // Init DAO
        BasketDao basketDao = new MemoBasketDao();
        SmokingToolDao smokingToolDao = new MemoSmokingToolDao(smokingTools);
        OrderDao orderDao = new MemoOrderDao();
        HistoryDao historyDao = new MemoHistoryDao();

        // Init Service
        IBasketService basketService = new BasketService(basketDao, smokingToolDao, historyDao, orderDao);
        IOrderService orderService = new OrderService(orderDao, smokingToolDao);
        ISmokingToolService smokingToolService = new SmokingToolService(smokingToolDao);
        IHistoryService historyService = new HistoryService(historyDao);

        StrategyContext strategyContext = new StrategyContext();

        Input input = getInput();
        CreateContainer.addStrategy("hookah", new HookahStrategy(input));
        CreateContainer.addStrategy("vape", new VapeStrategy(input));
        CreateContainer.addStrategy("pipe", new PipeStrategy(input));

        Locale locale = getLocale();
        ResourceManager.INSTANCE.changeResource(locale);

        TypesToolContainer.addCreator("hookah", Hookah.class);
        TypesToolContainer.addCreator("vape", Vape.class);
        TypesToolContainer.addCreator("pipe", Pipe.class);
        ReflectionCreator reflectionCreator = new ReflectionCreator(input);

        // Init Command
        AddProductInBasketCommand addProductInBasketCommand = new AddProductInBasketCommand(smokingToolService, basketService);
        AllProductsCommand allProductsCommand = new AllProductsCommand(smokingToolService);
        BasketCommand basketCommand = new BasketCommand(basketService);
        BuyAllCommand buyAllCommand = new BuyAllCommand(basketService);
        MainPageCommand mainPageCommand = new MainPageCommand();
        LastFiveInBasketCommand lastFiveInBasketCommand = new LastFiveInBasketCommand(historyService);
        OrdersByPeriodCommand ordersByPeriodCommand = new OrdersByPeriodCommand(orderService);
        OrdersByDateCommand ordersByDateCommand = new OrdersByDateCommand(orderService);
        AddPageCommand addPageCommand = new AddPageCommand();
        AddSmokingToolStrategyCommand addSmokingToolCommand = new AddSmokingToolStrategyCommand(smokingToolService,
                strategyContext);
        AddSmokingToolReflectionCommand addSmokingToolReflectionCommand = new AddSmokingToolReflectionCommand(smokingToolService, reflectionCreator);

        // Init ContainerCommand
        CommandContainer.MAIN_PAGE.add("Basket", basketCommand);
        CommandContainer.MAIN_PAGE.add("All products", allProductsCommand);
        CommandContainer.MAIN_PAGE.add("Orders by period", ordersByPeriodCommand);
        CommandContainer.MAIN_PAGE.add("Order by date", ordersByDateCommand);
        CommandContainer.MAIN_PAGE.add("Add product", addPageCommand);
        CommandContainer.ERROR_PAGE.add("Main page", mainPageCommand);
        CommandContainer.BASKET_PAGE.add("Main page", mainPageCommand);
        CommandContainer.BASKET_PAGE.add("Buy all", buyAllCommand);
        CommandContainer.BASKET_PAGE.add("Show 5 last added", lastFiveInBasketCommand);
        CommandContainer.ALL_PRODUCTS_PAGE.add("Main page", mainPageCommand);
        CommandContainer.ALL_PRODUCTS_PAGE.add("Add in basket", addProductInBasketCommand);
        CommandContainer.INFO_PAGE.add("Main page", mainPageCommand);
        CommandContainer.ALL_PRODUCTS_WITHOUT_COMMANDS_PAGE.add("Main page", mainPageCommand);
        CommandContainer.ORDER_PAGE.add("Main page", mainPageCommand);
        CommandContainer.ALL_PRODUCTS_TYPE_PAGE.add("Main page", mainPageCommand);
        CommandContainer.ALL_PRODUCTS_TYPE_PAGE.add("Add product with strategy", addSmokingToolCommand);
        CommandContainer.ALL_PRODUCTS_TYPE_PAGE.add("Add product with reflection",
                addSmokingToolReflectionCommand);

        Map<String, Command> commands = new HashMap<>();
        commands.put("count", new CountCommand(smokingToolService));
        commands.put("item", new ProductCommand(smokingToolService));
        ThreadFactoryTcp threadFactoryTcp = new ThreadFactoryTcp(commands, PORT_TCP);
        ThreadFactoryHttp threadFactoryHttp = new ThreadFactoryHttp(commands, PORT_HTTP);

        serverTcp = new Server(threadFactoryTcp);
        serverHttp = new Server(threadFactoryHttp);
        serverTcp.start();
        serverHttp.start();
    }

    private static Locale getLocale() throws IOException {
        System.out.println("Input language:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputData;
        Locales locale;
        Locales[] types = Locales.values();
        for (Locales type : types) {
            System.out.println(type.name());
        }
        inputData = reader.readLine();
        try {
            locale = Locales.valueOf(inputData);
        } catch (IllegalArgumentException e) {
            locale = null;
        }
        while (locale == null) {
            System.out.println("Incorrect input locale. Repeat please.");
            inputData = reader.readLine();
            try {
                locale = Locales.valueOf(inputData);
            } catch (IllegalArgumentException e) {
                locale = null;
            }
        }
        return locale.getLocale();
    }

    private static Input getInput() throws IOException {
        System.out.println("Choice type input:");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputData;
        Input inputType;
        String[] types = TypeInput.getKeys();
        for (String type : types) {
            System.out.println(type);
        }
        inputData = reader.readLine();
        inputType = TypeInput.getInput(inputData);
        while (inputType == null) {
            System.out.println("Incorrect input type input. Repeat please.");
            inputData = reader.readLine();
            inputType = TypeInput.getInput(inputData);
        }
        return inputType;
    }

    public static void doneServers(){
        serverTcp.exit();
        serverHttp.exit();
        try {
            serverTcp.join();
            serverHttp.join();
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
    }

}
