package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.util.Messages;

public class MainPageCommand implements Command {

    @Override
    public CommandContainer execute(Request request, Response response) {
        response.setAttribute(Messages.TITLE,"Main Page");
        return CommandContainer.MAIN_PAGE;
    }
}
