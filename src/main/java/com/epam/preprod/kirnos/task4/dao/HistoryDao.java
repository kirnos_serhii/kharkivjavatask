package com.epam.preprod.kirnos.task4.dao;

import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.Map;

public interface HistoryDao {

    public void add(String id, SmokingTool st) throws DBException;

    public Map<String, SmokingTool> getAll() throws DBException;

}
