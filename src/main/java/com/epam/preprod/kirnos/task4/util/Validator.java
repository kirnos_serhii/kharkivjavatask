package com.epam.preprod.kirnos.task4.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class Validator {

    public static final String DATE_REGEX = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|" +
            "^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|" +
            "^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
    public static final String TIME_AND_DATE_REGEX = "^(0[0-9]|1[0-9]|2[0-3])([- /.])(0[0-9]|[1-5][0-9])\\2(0[1-9]|[12][0-9]|3[01])\\2(0[1-9]|1[012])\\2(19|20)\\d\\d$";
    public static final String NUMBER_REGEX = "^(0)|([1-9]\\d*)$";
    public static final String NUMBER_REGEX_WITHOUT_ZERO = "^[1-9]\\d*$";
    public static final String ZERO_ONE = "^[0-1]$";
    public static final String NAME_FILE = "^[\\w][\\w,\\s-]*$";
    public static final String FILE = "^[\\w][\\w,\\s-]*\\.[^.\\\\/:*?\"<>|\\r\\n]+$";
    public static final String EXPANSION = "^[^.\\\\/:*?\"<>|\\r\\n]+$";
    public static final String NAME_ITEM = "^\\w{1}.*\\w{1}$|^\\w{1}$";
    public static final String PLUS_OR_MINUS = "^[+-]$";

    public static boolean validate(String str, String regex){
        Pattern pattern = Pattern.compile(regex, Pattern.UNICODE_CASE);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

}
