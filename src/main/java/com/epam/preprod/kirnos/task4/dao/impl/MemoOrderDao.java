package com.epam.preprod.kirnos.task4.dao.impl;

import com.epam.preprod.kirnos.task4.dao.OrderDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class MemoOrderDao implements OrderDao {

    private TreeMap<Date, Map<String, Integer>> orders;

    public MemoOrderDao() {
        orders = new TreeMap<>();
    }

    @Override
    public Map<Date, Map<String, Integer>> getAllOrders() {
        return orders;
    }

    @Override
    public void addOrder(Date date, Map<String, Integer> products) throws DBException {
        if (date == null || products == null || products.isEmpty()) {
            throw new DBException("Date or products is null or products is empty.");
        }
        orders.put(date, products);
    }

    @Override
    public Map<Date, Map<String, Integer>> getByPeriod(Date firstDate, Date secondDate) throws DBException {
        if (firstDate == null || secondDate == null) {
            throw new DBException("Date is empty.");
        }
        return orders.subMap(firstDate, secondDate);
    }
}
