package com.epam.preprod.kirnos.task4.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class HistoryMap<K, V> extends LinkedHashMap<K, V> {

    private int capacity;

    public HistoryMap(int capacity){
        this.capacity = capacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K,V> eldest) {
        return size() > capacity;
    }

}
