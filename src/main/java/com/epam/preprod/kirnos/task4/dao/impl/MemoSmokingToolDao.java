package com.epam.preprod.kirnos.task4.dao.impl;

import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.ArrayList;
import java.util.List;

public class MemoSmokingToolDao implements SmokingToolDao {

    private List<SmokingTool> smokingTools;

    public MemoSmokingToolDao() {
        smokingTools = new ArrayList<>();
    }

    public MemoSmokingToolDao(List<SmokingTool> inputSmokingTools) {
        smokingTools = inputSmokingTools;
    }

    @Override
    public List<SmokingTool> getAllSmokingTool() {
        return smokingTools;
    }

    @Override
    public SmokingTool getSmokingToolById(String id) throws DBException {
        if(id == null){
            throw new DBException("ID can  not be null.");
        }
        for (SmokingTool element : smokingTools) {
            if (id.equals(element.getId())) {
                return element;
            }
        }
        throw new DBException("Tool with this id do not exist.");
    }

    @Override
    public void addSmokingTool(SmokingTool smokingTool) throws DBException {
        smokingTools.add(smokingTool);
    }
}
