package com.epam.preprod.kirnos.task4.view;

import java.util.List;

public class TableView {

    private String headline = "---Commands---";

    public void show(List<String> commands) {
        System.out.println(headline);
        int i = 0;
        for(String command: commands){
            System.out.println(i++ + " " + command);
        }
    }
}
