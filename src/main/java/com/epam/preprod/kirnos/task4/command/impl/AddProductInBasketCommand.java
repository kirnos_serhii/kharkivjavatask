package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task4.util.Messages;
import com.epam.preprod.kirnos.task4.util.Validator;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AddProductInBasketCommand implements Command {

    private ISmokingToolService smokingToolService;
    private IBasketService basketService;

    public AddProductInBasketCommand(ISmokingToolService smokingToolService, IBasketService basketService) {
        this.smokingToolService = smokingToolService;
        this.basketService = basketService;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException, IOException {
        System.out.println("Input product ID:");
        BufferedReader reader = (BufferedReader) request.getAttribute("reader");
        String id = reader.readLine();
        if (!Validator.validate(id, Validator.NUMBER_REGEX)) {
            throw new ShopException("Incorrect input. ID must be integer.");
        }
        // Check exist this ID
        SmokingTool st = smokingToolService.getSmokingToolById(id);
        if (st == null) {
            throw new ShopException("Incorrect ID.");
        }

        System.out.println("Input count:");
        String countStr = reader.readLine();
        if (!Validator.validate(countStr, Validator.NUMBER_REGEX_WITHOUT_ZERO)) {
            throw new ShopException("Incorrect input. Count must be integer and large 0.");
        }

        // add product in basket
        int count = Integer.parseInt(countStr);
        for (int i = 0; i < count; i++) {
            basketService.addSmokingTool(st);
        }
        Map<SmokingTool, Integer> products = new HashMap<>();
        products.put(st, count);
        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "Was added:");
        return CommandContainer.BASKET_PAGE;
    }
}