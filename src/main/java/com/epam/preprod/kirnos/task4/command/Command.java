package com.epam.preprod.kirnos.task4.command;

import com.epam.preprod.kirnos.task4.exceptions.ShopException;

import java.io.IOException;

/**
 * Main interface for the Command pattern implementation.
 *
 * @author Serhii Kirnos
 *
 */
public interface Command {

    /**
     * Execution method for command.
     *
     * @return Address to go once the command is executed.
     */
    CommandContainer execute(Request request,
                        Response response) throws ShopException, IOException;

}
