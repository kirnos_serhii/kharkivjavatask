package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IHistoryService;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.util.Messages;

import java.util.List;

public class LastFiveInBasketCommand implements Command {

    private IHistoryService historyService;

    public LastFiveInBasketCommand(IHistoryService historyService) {
        this.historyService = historyService;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException {
        List<SmokingTool> products = historyService.getLastFive();
        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "Last 5 products in basket");
        return CommandContainer.ALL_PRODUCTS_WITHOUT_COMMANDS_PAGE;
    }
}
