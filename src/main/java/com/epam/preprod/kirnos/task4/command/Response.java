package com.epam.preprod.kirnos.task4.command;

import java.util.Map;
import java.util.TreeMap;

public class Response {

    private Map<String, Object> params = new TreeMap<>();

    public void setAttribute(String name, Object object) {
        params.put(name, object);
    }

    public Object getAttribute(String name){
        return params.get(name);
    }

    public Map<String, Object> getParams() {
        return params;
    }
}
