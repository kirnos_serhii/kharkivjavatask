package com.epam.preprod.kirnos.task4.view;

import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;

public interface Page {

    public void execute(Response response) throws ShopException;

}


