package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.service.IBasketService;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.util.Messages;

import java.util.Map;

public class BasketCommand implements Command {

    private IBasketService basketService;

    public BasketCommand(IBasketService basketService) {
        this.basketService = basketService;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException {
        Map<SmokingTool, Integer> products = basketService.getAll();
        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "Basket");
        return CommandContainer.BASKET_PAGE;
    }
}