package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IOrderService;
import com.epam.preprod.kirnos.task4.util.Messages;
import com.epam.preprod.kirnos.task4.util.Validator;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

public class OrdersByDateCommand implements Command {

    private IOrderService orderService;

    public OrdersByDateCommand(IOrderService orderService) {
        this.orderService = orderService;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException, IOException {
        BufferedReader reader = (BufferedReader) request.getAttribute("reader");
        System.out.println("Input date in format \"HH/mm/dd/MM/yyyy\":");
        String dateStr = reader.readLine();
        if (!Validator.validate(dateStr, Validator.TIME_AND_DATE_REGEX)) {
            throw new ShopException("Incorrect date input.");
        }

        Date date;
        try {
            date = new SimpleDateFormat("HH/mm/dd/MM/yyyy").parse(dateStr);
            Date currentDate = new Date();
            if(currentDate.before(date)){
                throw new ShopException("Date must be less current.");
            }
        } catch (ParseException ex) {
            throw new ShopException("Incorrect date format.");
        }
        Map<Date, Map<SmokingTool, Integer>> products = orderService.getByDate(date);
        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "Product by date.");
        return CommandContainer.ORDER_PAGE;
    }
}
