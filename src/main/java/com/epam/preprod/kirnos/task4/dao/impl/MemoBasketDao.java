package com.epam.preprod.kirnos.task4.dao.impl;

import com.epam.preprod.kirnos.task4.dao.BasketDao;
import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.LinkedHashMap;
import java.util.Map;

public class MemoBasketDao implements BasketDao {

    private Map<String, Integer> basketData;

    public MemoBasketDao() {
        basketData = new LinkedHashMap<>();
    }

    @Override
    public Map<String, Integer> getAll() {
        return basketData;
    }

    @Override
    public void addSmokingTool(SmokingTool st) throws DBException {
        if(st == null){
            throw new DBException("SmokingTool do not can be null.");
        }
        Integer countProducts = basketData.get(st.getId());
        if(countProducts == null){
            countProducts = 1;
        } else{
            countProducts += 1;
        }
        basketData.put(st.getId(), countProducts);
    }

    @Override
    public void clear() {
        basketData.clear();
    }
}
