package com.epam.preprod.kirnos.task4.util;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class Serializer {

    public static void initParam(String fileName, List<SmokingTool> smokingTools) throws ShopException {
        File file = new File(fileName);
        if(!file.exists()){
            try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName))) {
                out.writeInt(0);
            } catch (IOException e) {
                throw new ShopException("Error with write object in file.");
            }
        }
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
            int count = in.readInt();
            if(count < 0){
                throw new IllegalArgumentException();
            }
            for(int i = 0; i < count; i++){
                smokingTools.add((SmokingTool) in.readObject());
            }
        } catch (FileNotFoundException e) {
            throw new ShopException("File not found exception.");
        } catch (IOException e) {
            throw new ShopException("Error reading collection from file.");
        } catch (ClassNotFoundException e) {
            throw new ShopException("Incorrect data in file.");
        }
    }

    public static void saveObjects(String fileName, List<SmokingTool> smokingTools) throws ShopException {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName))){
            int countObjects = smokingTools.size();
            out.writeInt(countObjects);
            for (SmokingTool smokingTool : smokingTools) {
                out.writeObject(smokingTool);
            }
        } catch (FileNotFoundException e) {
            throw new ShopException("File not found exception.");
        } catch (IOException e) {
            throw new ShopException("Error writing collection in file.");
        }
    }

}
