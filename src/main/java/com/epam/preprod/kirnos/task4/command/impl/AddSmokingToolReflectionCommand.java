package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task4.util.Messages;
import com.epam.preprod.kirnos.task7.create.ReflectionCreator;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

public class AddSmokingToolReflectionCommand implements Command {

    private ISmokingToolService smokingToolService;
    private ReflectionCreator smokingToolReflectionCreator;

    public AddSmokingToolReflectionCommand(ISmokingToolService smokingToolService,
                                           ReflectionCreator smokingToolReflectionCreator) {
        this.smokingToolService = smokingToolService;
        this.smokingToolReflectionCreator = smokingToolReflectionCreator;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException, IOException {
        BufferedReader reader = (BufferedReader) request.getAttribute("reader");
        SmokingTool smokingTool;
        try {
            System.out.println("Input type tool:");
            smokingTool = smokingToolReflectionCreator.create(reader.readLine());
            while(smokingTool == null){
                System.out.println("Incorrect input. Reaped please.");
                smokingTool = smokingToolReflectionCreator.create(reader.readLine());
            }
        } catch (IllegalAccessException | InstantiationException e) {
            throw new ShopException("Error create object.", e);
        }
        smokingToolService.addSmokingTool(smokingTool);
        List<SmokingTool> products = smokingToolService.getAllSmokingTool();
        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "All products");
        return CommandContainer.ALL_PRODUCTS_PAGE;
    }
}
