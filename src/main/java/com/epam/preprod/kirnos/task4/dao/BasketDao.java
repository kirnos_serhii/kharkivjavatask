package com.epam.preprod.kirnos.task4.dao;

import com.epam.preprod.kirnos.task4.exceptions.DBException;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.Map;

public interface BasketDao {

    public Map<String, Integer> getAll() throws DBException;

    public void addSmokingTool(SmokingTool st) throws DBException;

    public void clear() throws DBException;
}
