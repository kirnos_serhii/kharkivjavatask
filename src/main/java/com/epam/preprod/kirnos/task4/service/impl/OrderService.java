package com.epam.preprod.kirnos.task4.service.impl;

import com.epam.preprod.kirnos.task4.dao.OrderDao;
import com.epam.preprod.kirnos.task4.dao.SmokingToolDao;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.IOrderService;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.*;

public class OrderService implements IOrderService {

    private OrderDao orderDao;
    private SmokingToolDao smokingToolDao;

    public OrderService(OrderDao orderDao, SmokingToolDao smokingToolDao) {
        this.orderDao = orderDao;
        this.smokingToolDao = smokingToolDao;
    }

    public Map<Date, Map<SmokingTool, Integer>> getByPeriod(Date firstDate, Date secondDate) throws ShopException {
        Map<Date, Map<SmokingTool, Integer>> resultList = new HashMap<>();
        for(Map.Entry<Date,Map<String, Integer>> entry: orderDao.getByPeriod(firstDate, secondDate).entrySet()){
            Map<SmokingTool, Integer> addList = new HashMap<>();
            for(Map.Entry<String, Integer> element: entry.getValue().entrySet()){
                addList.put(smokingToolDao.getSmokingToolById(element.getKey()), element.getValue());
            }
            resultList.put(entry.getKey(), addList);
        }
        return resultList;
    }

    public void addOrder(Date date, Map<String, Integer> order) throws ShopException {
        orderDao.addOrder(date, order);
    }

    public Map<Date, Map<SmokingTool, Integer>> getByDate(Date date) throws ShopException {
        Map<Date, Map<String, Integer>> orders = orderDao.getAllOrders();
        long min = date.getTime();
        Date resultDate = null;
        Map<String, Integer> resultMap = null;
        Map<Date, Map<SmokingTool, Integer>> result = new HashMap<>();
        for(Map.Entry<Date, Map<String, Integer>> entry: orders.entrySet()){
            long sub = Math.abs(entry.getKey().getTime() - date.getTime());
            if(min > sub){
                min = sub;
                resultDate = entry.getKey();
                resultMap = entry.getValue();
            }
        }
        if(resultDate != null && resultMap != null){
            Map<SmokingTool, Integer> addMap = new HashMap<>();
            for(Map.Entry<String, Integer> element: resultMap.entrySet()){
                addMap.put(smokingToolDao.getSmokingToolById(element.getKey()), element.getValue());
            }
            result.put(resultDate, addMap);
        }
        return result;
    }
}
