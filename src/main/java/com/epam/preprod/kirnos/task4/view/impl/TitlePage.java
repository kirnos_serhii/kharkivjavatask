package com.epam.preprod.kirnos.task4.view.impl;

import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.view.Page;

public class TitlePage implements Page {

    @Override
    public void execute(Response response) {
        System.out.println(response.getAttribute("title"));
    }
}
