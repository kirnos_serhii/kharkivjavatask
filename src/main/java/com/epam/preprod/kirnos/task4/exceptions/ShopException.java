package com.epam.preprod.kirnos.task4.exceptions;

/**
 * An exception that provides information on an console application error.
 *
 * @author D.Kolesnikov
 *
 */
public class ShopException extends Exception {

    public ShopException() {
        super();
    }

    public ShopException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShopException(String message) {
        super(message);
    }

}
