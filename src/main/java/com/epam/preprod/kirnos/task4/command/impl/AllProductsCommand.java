package com.epam.preprod.kirnos.task4.command.impl;

import com.epam.preprod.kirnos.task4.command.Command;
import com.epam.preprod.kirnos.task4.command.CommandContainer;
import com.epam.preprod.kirnos.task4.command.Request;
import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.service.ISmokingToolService;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.util.Messages;

import java.util.List;

public class AllProductsCommand implements Command {

    private ISmokingToolService smokingToolService;

    public AllProductsCommand(ISmokingToolService smokingToolService) {
        this.smokingToolService = smokingToolService;
    }

    @Override
    public CommandContainer execute(Request request, Response response) throws ShopException {
        List<SmokingTool> products = smokingToolService.getAllSmokingTool();

        response.setAttribute(Messages.PRODUCTS, products);
        response.setAttribute(Messages.TITLE, "All products");
        return CommandContainer.ALL_PRODUCTS_PAGE;
    }
}