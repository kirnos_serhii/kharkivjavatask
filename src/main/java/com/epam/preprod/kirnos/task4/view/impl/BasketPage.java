package com.epam.preprod.kirnos.task4.view.impl;

import com.epam.preprod.kirnos.task4.command.Response;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.view.Page;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.Map;

public class BasketPage implements Page {

    @Override
    public void execute(Response response) throws ShopException {
        Map<SmokingTool, Integer> products = (Map<SmokingTool, Integer>) response.getAttribute("products");
        System.out.println(response.getAttribute("title"));
        int i = 1;
        if(products.isEmpty()){
            System.out.println("Basket is empty.");
        }
        for (Map.Entry<SmokingTool, Integer> pair: products.entrySet()) {
            System.out.println("| N  |count| product");
            System.out.println(String.format("| %-2d | %-2d  | ", i++, pair.getValue()) + pair.getKey());
        }
    }
}