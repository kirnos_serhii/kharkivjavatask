
document.addEventListener('DOMContentLoaded', function() {
    document.getElementById("myForm").addEventListener("submit", validateForm, false);
 }, false);

function validateForm() {
    var node = document.createElement("div");
    try {
        let inputLogin = document.getElementById("inputLogin");
        let inputPassword = document.getElementById("inputPassword");
        let inputEmail = document.getElementById("inputEmail");
        let inputTelephoneCode = document.getElementById("inputTelephoneCode");
        let inputTelephone = document.getElementById("inputTelephone");
        let reLogin = /^[a-zA-Z_]+$/;
        let rePassword = /^[a-zA-Z_1-9]{6,}$/;
        let reEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let reTelephone = /^[0-9]{10}$/;
        let reTelephoneCode = /^[A-Z]{2}$/;
        let OK = reLogin.exec(inputLogin.value);
        if (!OK) {
            addErrorMessageJS(node, inputLogin.value + ' isn\'t a correct login!');
            inputLogin.focus();
            return false;
        }

        OK = rePassword.exec(inputPassword.value);
        if (!OK) {
            addErrorMessageJS(node, 'It isn\'t a correct password!');
            inputPassword.focus();
            return false;
        }

        OK = reEmail.exec(inputEmail.value);
        if (!OK) {
            addErrorMessageJS(node, inputEmail.value + ' isn\'t a correct email!');
            inputEmail.focus();
            return false;
        }

        OK = reTelephone.exec(inputTelephone.value);
        if (!OK) {
            addErrorMessageJS(node, reTelephone.value + ' isn\'t a correct terlephone number!');
            inputTelephone.focus();
            return false;
        }

        OK = reTelephoneCode.exec(inputTelephoneCode.value);
        if (!OK) {
            addErrorMessageJS(node, inputTelephoneCode.value + ' isn\'t a correct terlephone number!');
            inputTelephoneCode.focus();
            return false;
        }
        return true;
    } finally {
        let alertForm = document.getElementById("alertForm")
        if (alertForm != null) {
            alertForm.remove();
        }
        if (node.hasChildNodes) {
            node.classList.add("alert");
            node.classList.add("alert-danger");
            node.classList.add("mt-3");
            node.id = "alertForm";
            document.getElementById("myForm").appendChild(node);
        }
    }
}

function addErrorMessageJS(node, text) {
    var textnode = document.createTextNode(text);
    node.appendChild(textnode);
}