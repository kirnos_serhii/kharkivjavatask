
$(document).ready(function () {
    $("#myForm").submit(validateFormJQuery)
});

function validateFormJQuery() {
    let isValid = checkInvalidInput($(this).find("#inputLogin"), /^[a-zA-Z_]+$/,
        $(this).find("#inputLogin").val() + ' isn\'t a correct login!');

    if (isValid != false) {
        isValid = checkInvalidInput($(this).find("#inputPassword"), /^[a-zA-Z_1-9]{6,}$/,
            "It isn\'t a correct password!");
    }

    if (isValid != false) {
        isValid = checkInvalidInput($(this).find("#inputEmail"), /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            $(this).find("#inputEmail").val() + ' isn\'t a correct email!');
    }

    if (isValid != false) {
        isValid = checkInvalidInput($(this).find("#inputTelephoneCode"), /^[A-Z]{2}$/,
            $(this).find("#inputTelephoneCode").val() + ' isn\'t a correct telephone code!');
    }

    if (isValid != false) {
        isValid = checkInvalidInput($(this).find("#inputTelephone"), /^[0-9]{10}$/,
            $(this).find("#inputTelephone").val() + ' isn\'t a correct telephone number!');
    }

    return isValid;
}

function checkInvalidInput($object, regex, errMessage) {
    if (!$object.val().match(regex)) {
        addErrorMessageJqQuery(errMessage);
        $object.focus();
        return false;
    }
}

function addErrorMessageJqQuery(errMessage) {
    if ($("#alertForm").length) {
        $("#alertForm").remove();
    }
    let $newdiv = $("<div>" + errMessage + "</div>");
    $newdiv.attr('class', 'alert alert-danger mt-3');
    $newdiv.attr('id', 'alertForm');
    $("#myForm").append($newdiv);
}