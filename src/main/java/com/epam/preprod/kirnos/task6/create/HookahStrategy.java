package com.epam.preprod.kirnos.task6.create;

import com.epam.preprod.kirnos.task1.entity.Hookah;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task6.util.Message;

public class HookahStrategy extends ClassicSmokingToolStrategy {

    public HookahStrategy(Input typeInput) {
        super(typeInput);
    }

    @Override
    public SmokingTool createSmokingTool() throws ShopException {
        Hookah hookah = new Hookah();
        super.enrich(hookah);
        hookah.setNumbTubes(typeInput.inputInt(Message.INPUT_NUMBER_TUBES_MSG, 0, 4));
        hookah.setMineHookah(typeInput.inputEnum(Message.INPUT_MATERIAL_MINE_MSG, Material.values()));
        hookah.setHeight(typeInput.inputInt(Message.INPUT_HEIGHT_MSG, 0, 200));
        hookah.setClickSystem(typeInput.inputBoolean(Message.INPUT_CLICK_SYSTEM_MSG));
        return hookah;
    }
}
