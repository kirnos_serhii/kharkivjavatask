package com.epam.preprod.kirnos.task6.create;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task4.util.Validator;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task6.util.Message;

public abstract class SmokingToolStrategy {

    Input typeInput;

    SmokingToolStrategy(Input typeInput) {
        this.typeInput = typeInput;
    }

    public abstract SmokingTool createSmokingTool() throws ShopException;

    void enrich(SmokingTool smokingTool) throws ShopException {
        smokingTool.setId(typeInput.inputString(Message.INPUT_ID_MSG,
                input -> Validator.validate(input, Validator.NUMBER_REGEX)));
        smokingTool.setName(typeInput.inputString(Message.INPUT_NAME_MSG,
                input -> Validator.validate(input, Validator.NAME_ITEM)));
        smokingTool.setManufacturer(typeInput.inputString(Message.INPUT_MANUFACTURER_MSG,
                input -> Validator.validate(input, Validator.NAME_ITEM)));
        smokingTool.setPrice(typeInput.inputDouble(Message.INPUT_PRICE_MSG,0, 1000000));
    }

}
