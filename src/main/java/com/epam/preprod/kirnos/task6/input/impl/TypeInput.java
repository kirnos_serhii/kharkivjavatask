package com.epam.preprod.kirnos.task6.input.impl;

import com.epam.preprod.kirnos.task6.input.Input;

import java.util.HashMap;
import java.util.Map;

/**
 * Holder for all input types.<br/>
 *
 * @author Serhii Kirnos
 */
public class TypeInput {

    private static Map<String, Input> types = new HashMap<>();

    static{
        types.put("console", new ConsoleInput());
        types.put("random", new RandomInput());
    }

    /**
     * Returns input type by key or {@code null} if map do not
     * contains value by this key.
     *
     * @param key the key whose associated value is to be returned
     * @return {@code null} if map contains no mapping for the key
     */
    public static Input getInput(String key){
        return types.get(key);
    }

    /**
     * Returns an array view of the keys contained in this map.
     *
     * @return an array view of the keys contained in this map.
     */
    public static String[] getKeys(){
        return types.keySet().toArray(new String[0]);
    }

}
