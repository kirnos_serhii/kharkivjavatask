package com.epam.preprod.kirnos.task6.util;

public class Message {

    public static String INPUT_ID_MSG = "Input Id: ";
    public static String INPUT_NAME_MSG = "Input name: ";
    public static String INPUT_MANUFACTURER_MSG = "Input manufacturer: ";
    public static String INPUT_PRICE_MSG = "Input price: ";
    public static String INPUT_MANAGEMENT_MSG = "Input management type: ";
    public static String INPUT_BATTERY_CAPACITY_MSG = "Input battery capacity: ";
    public static String INPUT_CASE_MATERIAL_MSG = "Input case material: ";
    public static String INPUT_FILTER_MSG = "Input filter: ";
    public static String INPUT_MATERIAL_MSG = "Input material: ";
    public static String INPUT_NUMBER_TUBES_MSG = "Input number tubes: ";
    public static String INPUT_MATERIAL_MINE_MSG = "Input material mine: ";
    public static String INPUT_HEIGHT_MSG = "Input hookah height";
    public static String INPUT_CLICK_SYSTEM_MSG = "Click system is exist: ";
    public static String INPUT_WOOD_TYPE_MSG = "Input wood type: ";
    public static String INPUT_PIPE_DIAMETER_MSG = "Input pipe diameter: ";
    public static String INPUT_TYPE_PIPE_MSG = "Input type pipe: ";
    public static String INPUT_CAPACITY_MSG = "Input capacity: ";
    public static String INPUT_STREAM_TYPE_MSG = "Input stream type: ";
    public static String INPUT_IS_SERVICED_MSG = "Is serviced: ";

}
