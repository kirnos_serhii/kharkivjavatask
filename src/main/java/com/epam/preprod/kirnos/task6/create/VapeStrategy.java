package com.epam.preprod.kirnos.task6.create;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task1.entity.SteamSystem;
import com.epam.preprod.kirnos.task1.entity.Vape;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task6.util.Message;

public class VapeStrategy extends ElectronicSmokingToolStrategy {

    public VapeStrategy(Input typeInput) {
        super(typeInput);
    }

    @Override
    public SmokingTool createSmokingTool() throws ShopException {
        Vape vape = new Vape();
        super.enrich(vape);
        vape.setCapacity(typeInput.inputDouble(Message.INPUT_CAPACITY_MSG,0, 1000));
        vape.setSteamSystem(typeInput.inputEnum(Message.INPUT_STREAM_TYPE_MSG,
                SteamSystem.values()));
        vape.setServiced(typeInput.inputBoolean(Message.INPUT_IS_SERVICED_MSG));
        return vape;
    }


}
