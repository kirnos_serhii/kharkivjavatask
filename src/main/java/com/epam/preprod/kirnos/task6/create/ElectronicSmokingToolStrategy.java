package com.epam.preprod.kirnos.task6.create;

import com.epam.preprod.kirnos.task1.entity.ElectronicSmokingTool;
import com.epam.preprod.kirnos.task1.entity.ManagementType;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task6.util.Message;

public abstract class ElectronicSmokingToolStrategy extends SmokingToolStrategy {

    ElectronicSmokingToolStrategy(Input typeInput) {
        super(typeInput);
    }

    void enrich(ElectronicSmokingTool electronicSmokingTool) throws ShopException {
        super.enrich(electronicSmokingTool);
        electronicSmokingTool.setManagementType(typeInput.inputEnum(Message.INPUT_MANAGEMENT_MSG,
                ManagementType.values()));
        electronicSmokingTool.setBatteryCapacity(typeInput.inputDouble(Message.INPUT_BATTERY_CAPACITY_MSG, 0, 1000));
        electronicSmokingTool.setCaseMaterial(typeInput.inputEnum(Message.INPUT_CASE_MATERIAL_MSG, Material.values()));
    }

}
