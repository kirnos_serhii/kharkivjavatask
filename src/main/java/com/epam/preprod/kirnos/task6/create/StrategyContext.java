package com.epam.preprod.kirnos.task6.create;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;

public class StrategyContext {

    private SmokingToolStrategy smokingToolStrategy;

    public SmokingTool create() throws ShopException {
        return smokingToolStrategy.createSmokingTool();
    }

    public void setSmokingToolStrategy(SmokingToolStrategy smokingToolStrategy) {
        this.smokingToolStrategy = smokingToolStrategy;
    }

}
