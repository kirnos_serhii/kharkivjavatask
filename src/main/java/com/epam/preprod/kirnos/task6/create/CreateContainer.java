package com.epam.preprod.kirnos.task6.create;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Holder for all strategy.<br/>
 *
 * @author Serhii Kirnos
 *
 */
public class CreateContainer {

    private static Map<String, SmokingToolStrategy> commands = new TreeMap<>();

    public static void addStrategy(String msg, SmokingToolStrategy smokingToolStrategy) {
        commands.put(msg, smokingToolStrategy);
    }

    /**
     * Returns an array view of the keys contained in this map.
     *
     * @return an array view of the keys contained in this map.
     */
    public static Set<String> getKeys(){
        return commands.keySet();
    }

    /**
     * Returns strategy object with the given name.
     *
     * @param strategyName
     *            Name of the command.
     * @return Command object or {@code null} if map contains no mapping for the key
     */
    public static SmokingToolStrategy get(String strategyName) {
        if (strategyName == null || !commands.containsKey(strategyName)) {
            return null;
        }
        return commands.get(strategyName);
    }
}
