package com.epam.preprod.kirnos.task6.create;

import com.epam.preprod.kirnos.task1.entity.ClassicSmokingTool;
import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task6.util.Message;

abstract class ClassicSmokingToolStrategy extends SmokingToolStrategy {

    ClassicSmokingToolStrategy(Input typeInput) {
        super(typeInput);
    }

    void enrich(ClassicSmokingTool classicSmokingTool) throws ShopException {
        super.enrich(classicSmokingTool);
        classicSmokingTool.setFilter(typeInput.inputBoolean(Message.INPUT_FILTER_MSG));
        classicSmokingTool.setMaterial(typeInput.inputEnum(Message.INPUT_MATERIAL_MSG, Material.values()));
    }

}
