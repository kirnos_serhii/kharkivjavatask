package com.epam.preprod.kirnos.task6.create;

import com.epam.preprod.kirnos.task1.entity.Pipe;
import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task1.entity.TypePipe;
import com.epam.preprod.kirnos.task1.entity.Wood;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task6.util.Message;

public class PipeStrategy extends ClassicSmokingToolStrategy {

    public PipeStrategy(Input typeInput) {
        super(typeInput);
    }

    @Override
    public SmokingTool createSmokingTool() throws ShopException {
       Pipe pipe = new Pipe();
       super.enrich(pipe);
       pipe.setWood(typeInput.inputEnum(Message.INPUT_WOOD_TYPE_MSG, Wood.values()));
       pipe.setDiameter(typeInput.inputDouble(Message.INPUT_PIPE_DIAMETER_MSG, 0,20));
       pipe.setTypePipe(typeInput.inputEnum(Message.INPUT_TYPE_PIPE_MSG, TypePipe.values()));
       return  pipe;
    }
}
