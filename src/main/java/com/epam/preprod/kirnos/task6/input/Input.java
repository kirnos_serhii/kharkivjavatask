package com.epam.preprod.kirnos.task6.input;

import com.epam.preprod.kirnos.task4.exceptions.ShopException;

import java.util.function.Predicate;

public interface Input {

    int inputInt(String message, int min, int max) throws ShopException;

    double inputDouble(String message, double min, double max) throws ShopException;

    String inputString(String message, Predicate<String> predicate) throws ShopException;

    <T extends Enum> T inputEnum(String message, T[] en) throws ShopException;

    boolean inputBoolean(String message) throws ShopException;

}
