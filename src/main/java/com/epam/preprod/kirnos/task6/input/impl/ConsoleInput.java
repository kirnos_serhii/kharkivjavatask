package com.epam.preprod.kirnos.task6.input.impl;

import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.input.Input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Predicate;

/**
 * Implements filling of values from console.
 *
 * @author Serhii Kirnos
 */
public class ConsoleInput implements Input {

    private int readInt() throws ShopException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            return Integer.parseInt(br.readLine());
        } catch (NumberFormatException ex) {
            System.out.println("Incorrect input! Input please integer number.");
            return readInt();
        } catch (IOException e) {
            throw new ShopException("Input int number error.", e);
        }
    }

    private double readDouble() throws ShopException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            return Double.parseDouble(br.readLine());
        } catch (NumberFormatException ex) {
            System.out.println("Incorrect input! Input please double number.");
            return readDouble();
        } catch (IOException e) {
            throw new ShopException("Input double number error.", e);
        }
    }

    @Override
    public int inputInt(String message, int min, int max) throws ShopException {
        System.out.println(message);
        System.out.println(String.format("From %d to %d", min, max));
        int inputInt = readInt();
        while (inputInt < min || inputInt > max) {
            System.out.println(String.format("Incorrect number. Number must be from %d to %d", min, max));
            inputInt = readInt();
        }
        return inputInt;
    }

    @Override
    public double inputDouble(String message, double min, double max) throws ShopException {
        System.out.println(message);
        System.out.println(String.format("From %f to %f", min, max));
        double inputDouble = readDouble();
        while (inputDouble < min || inputDouble > max) {
            System.out.println(String.format("Incorrect number. Number must be from %f to %f", min, max));
            inputDouble = readInt();
        }
        return inputDouble;
    }

    @Override
    public String inputString(String message, Predicate<String> predicate) throws ShopException {
        System.out.println(message);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            String inputStr = br.readLine();
            while (!predicate.test(inputStr)) {
                System.out.println("Incorrect string. Enter again.");
                inputStr = br.readLine();
            }
            return inputStr;
        } catch (IOException e) {
            throw new ShopException("Input string error.", e);
        }
    }

    @Override
    public <T extends Enum> T inputEnum(String message, T[] en) throws ShopException {
        System.out.println(message);
        for (int i = 0; i < en.length; i++) {
            System.out.println(i + " - " + en[i]);
        }
        int inputInt = readInt();
        while (inputInt < 0 || inputInt >= en.length) {
            System.out.println(String.format("Incorrect value. Value must be from %d to %d", 0, en.length));
            inputInt = readInt();
        }
        return en[inputInt];
    }

    @Override
    public boolean inputBoolean(String message) throws ShopException {
        System.out.println(message);
        System.out.println("Input true or false:");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            String inputStr = br.readLine();
            while (!inputStr.equals("true") && !inputStr.equals("false")) {
                System.out.println("Incorrect value. Enter again.");
                inputStr = br.readLine();
            }
            return Boolean.parseBoolean(inputStr);
        } catch (IOException e) {
            throw new ShopException("Input boolean error.", e);
        }
    }
}
