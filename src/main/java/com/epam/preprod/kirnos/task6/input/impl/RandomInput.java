package com.epam.preprod.kirnos.task6.input.impl;

import com.epam.preprod.kirnos.task6.input.Input;

import java.util.function.Predicate;

/**
 * Implements random filling of values.
 *
 * @author Serhii Kirnos
 */
public class RandomInput implements Input {

    @Override
    public int inputInt(String message, int min, int max) {
        return (int) (Math.random() * (max - min)) + min;
    }

    @Override
    public double inputDouble(String message, double min, double max) {
        return (Math.random() * (max - min)) + min;
    }

    @Override
    public String inputString(String message, Predicate<String> predicate) {
        StringBuilder resultStr = new StringBuilder("Name");
        for (int i = 0; i < 10; i++) {
            String str = String.valueOf((int) (Math.random() * 10));
            resultStr.append(str);
        }
        return resultStr.toString();
    }

    @Override
    public <T extends Enum> T inputEnum(String message, T[] en) {
        int index = (int) (Math.random() * en.length);
        return en[index];
    }

    @Override
    public boolean inputBoolean(String message) {
        return Math.random() > 0.5;
    }
}
