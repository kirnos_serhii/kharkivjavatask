package com.epam.preprod.kirnos.task1.entity;

import com.epam.preprod.kirnos.task7.annotation.NameField;

import java.io.Serializable;

public abstract class SmokingTool implements Serializable {

    @NameField(name = "ID")
    String id;
    @NameField(name = "NAME")
    String name;
    @NameField(name = "PRICE")
    double price;
    @NameField(name = "MANUFACTURER")
    String manufacturer;

    SmokingTool() {
    }

    SmokingTool(String id, String name, double price, String manufacturer) {
        this();
        this.id = id;
        this.name = name;
        this.price = price;
        this.manufacturer = manufacturer;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SmokingTool other = (SmokingTool) obj;
        if (manufacturer == null) {
            if (other.manufacturer != null)
                return false;
        } else if (!manufacturer.equals(other.manufacturer))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return Double.doubleToLongBits(price) == Double.doubleToLongBits(other.price);
    }

}
