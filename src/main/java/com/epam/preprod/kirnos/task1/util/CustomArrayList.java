package com.epam.preprod.kirnos.task1.util;

import java.util.*;
import java.util.function.Predicate;

public class CustomArrayList<E> implements List<E> {

    private Object[] dataArray;
    /**
     * Where must be insert next element.
     */
    private int lastElement;
    private final int initCapacity;
    private int countAdd;

    public CustomArrayList() {
        initCapacity = countAdd = 10;
        dataArray = new Object[initCapacity];
    }

    public CustomArrayList(int initCapacity, int countAdd) {
        this.initCapacity = initCapacity;
        this.countAdd = countAdd;
        dataArray = new Object[initCapacity];
    }

    private void recapacity() {
        if (lastElement == dataArray.length) {
            int newCapacity = dataArray.length + countAdd;
            dataArray = Arrays.copyOf(dataArray, newCapacity);
        }
    }

    @Override
    public boolean add(E e) {
        recapacity();
        dataArray[lastElement++] = e;
        return true;
    }

    @Override
    public void add(int index, E element) {
        indexCheckAdd(index);
        recapacity();
        for (int i = lastElement; i > index; i--) {
            dataArray[i] = dataArray[i - 1];
        }
        dataArray[index] = element;
        lastElement++;

    }

    private void indexCheck(int index) {
        if (index < 0 || index >= lastElement) {
            throw new IndexOutOfBoundsException("Do not correct index. Such index does not exist.");
        }
    }

    private void indexCheckAdd(int index) {
        if (index < 0 || index > lastElement) {
            throw new IndexOutOfBoundsException("Do not correct index. Such index does not exist.");
        }
    }

    @Override
    public boolean addAll(Collection<? extends E> inputcollection) {
        for (E object : inputcollection) {
            add(object);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        int offset = 0;
        for (E o : c) {
            add(index + offset++, o);
        }
        return true;
    }

    @Override
    public void clear() {
        lastElement = 0;
        dataArray = new Object[0];
        recapacity();
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if (indexOf(o) == -1) {
                return false;
            }
        }
        return true;
    }

    @Override
    public E get(int index) {
        indexCheck(index);
        return getData(index);
    }

    private E getData(int index) {
    	return (E) dataArray[index];
    }

    @Override
    public int indexOf(Object o) {
        if (o == null) {
            for (int i = 0; i < lastElement; i++) {
                if (dataArray[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < lastElement; i++) {
                if (dataArray[i].equals(o)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return lastElement == 0 ? true : false;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyFilterIterator();
    }

    public Iterator<E> iterator(Predicate<E> p) {
        return new MyFilterIterator(p);
    }

    @Override
    public int lastIndexOf(Object o) {
        if (o == null) {
            for (int i = lastElement - 1; i >= 0; i--) {
                if (dataArray[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = lastElement - 1; i >= 0; i--) {
                if (dataArray[i].equals(o)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        throw new UnsupportedOperationException("Do not support listIterator by this CustomArrayList collection.");
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        throw new UnsupportedOperationException("Do not support listIterator(int index) by CustomArrayList collection.");
    }

    @Override
    public boolean remove(Object o) {
        if (o == null) {
            for (int i = 0; i < lastElement; i++) {
                if (dataArray[i] == null) {
                    remove(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < lastElement; i++) {
                if (dataArray[i].equals(o)) {
                    remove(i);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public E remove(int index) {
        indexCheck(index);
        E oldValue = getData(index);
        for (int i = index; i < lastElement - 1; i++) {
            dataArray[i] = dataArray[i + 1];
        }
        lastElement--;
        return oldValue;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean ret = false;
        for (Object o : c) {
            if (remove(o)) {
                ret = true;
            }
        }
        return ret;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean result = false;
        for (Object o : c) {
            if (!this.contains(o)) {
                result = true;
                while (this.remove(o)) ;
            }
        }

        return result;
    }

    @Override
    public E set(int index, E element) {
        indexCheck(index);
        E oldValue = getData(index);
        dataArray[index] = element;
        return oldValue;
    }

    @Override
    public int size() {
        return lastElement;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
		throw new UnsupportedOperationException("Do not support subList by CustomArrayList collection.");
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(dataArray, lastElement);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T[] toArray(T[] inputArray) {
        if (inputArray.length < lastElement) {
            return (T[]) Arrays.copyOf(dataArray, lastElement, inputArray.getClass());
        }

        System.arraycopy(dataArray, 0, inputArray, 0, lastElement);
        if (inputArray.length > lastElement) {
            inputArray[lastElement] = null;
        }

        return inputArray;
    }

    private class MyFilterIterator implements Iterator<E> {

        private int cursor;
        private int lastRet = -1;
        private Predicate<E> predicate;

        MyFilterIterator() {
        }

        MyFilterIterator(Predicate<E> p) {
            predicate = p;
            setCursor(0);
        }

        @Override
        public boolean hasNext() {
            return cursor < lastElement;
        }

        @SuppressWarnings("unchecked")
        @Override
        public E next() {
            int i = cursor;
            if (i >= lastElement) {
                throw new NoSuchElementException("Iteration has no more elements.");
            }
            setCursor(cursor + 1);
            return (E) dataArray[lastRet = i];
        }

        @Override
        public void remove() {
            if (lastRet < 0) {
                throw new IllegalStateException("Method has not	yet been called, or the remove() method has "
                        + "already been called after the last call to the next() method");
            }
            CustomArrayList.this.remove(lastRet);
            setCursor(lastRet);
            lastRet = -1;
        }

        private void setCursor(int start) {
            if (predicate == null) {
                cursor = start;
            } else {
                for (int i = start; i < lastElement; i++) {
                    if (predicate.test(CustomArrayList.this.get(i))) {
                        cursor = i;
                        return;
                    }
                }
                cursor = lastElement;
            }
        }

    }

}
