package com.epam.preprod.kirnos.task1.entity;

import com.epam.preprod.kirnos.task7.annotation.NameField;

public abstract class ElectronicSmokingTool extends SmokingTool {

    @NameField(name = "MANAGEMENT_TYPE")
    ManagementType managementType;
    @NameField(name = "BATTERY_CAPACITY")
    double batteryCapacity;
    @NameField(name = "CASE_MATERIAL")
    Material caseMaterial;

    ElectronicSmokingTool() {
    }

    ElectronicSmokingTool(String id, String name, double price, String manufacturer,
                          ManagementType managementType, double batteryCapacity, Material caseMaterial) {
        super(id, name, price, manufacturer);
        this.managementType = managementType;
        this.batteryCapacity = batteryCapacity;
        this.caseMaterial = caseMaterial;
    }

    public ManagementType getManagementType() {
        return managementType;
    }

    public void setManagementType(ManagementType managementType) {
        this.managementType = managementType;
    }

    public double getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(double batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Material getCaseMaterial() {
        return caseMaterial;
    }

    public void setCaseMaterial(Material caseMaterial) {
        this.caseMaterial = caseMaterial;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ElectronicSmokingTool other = (ElectronicSmokingTool) obj;
        if (Double.doubleToLongBits(batteryCapacity) != Double.doubleToLongBits(other.batteryCapacity))
            return false;
        if (caseMaterial != other.caseMaterial)
            return false;
        return managementType == other.managementType;
    }


}
