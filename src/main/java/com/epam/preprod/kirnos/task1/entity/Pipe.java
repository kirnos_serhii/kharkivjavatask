package com.epam.preprod.kirnos.task1.entity;

import com.epam.preprod.kirnos.task7.annotation.NameField;
import com.epam.preprod.kirnos.task7.proxy.entity.IPipe;

public class Pipe extends ClassicSmokingTool implements IPipe {

    @NameField(name = "WOOD")
    private Wood wood;
    @NameField(name = "DIAMETER")
    private double diameter;
    @NameField(name = "TYPE_PIPE")
    private TypePipe typePipe;

    public Pipe() {
    }

    public Pipe(String id, String name, double price, String manufacturer, boolean filter,
                Material material, Wood wood, double diameter, TypePipe typePipe) {
        super(id, name, price, manufacturer, filter, material);
        this.wood = wood;
        this.diameter = diameter;
        this.typePipe = typePipe;
    }

    public TypePipe getTypePipe() {
        return typePipe;
    }

    public Wood getWood() {
        return wood;
    }

    public void setWood(Wood wood) {
        this.wood = wood;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    public TypePipe isTypePipe() {
        return typePipe;
    }

    public void setTypePipe(TypePipe typePipe) {
        this.typePipe = typePipe;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Pipe other = (Pipe) obj;
        if (Double.doubleToLongBits(diameter) != Double.doubleToLongBits(other.diameter))
            return false;
        if (typePipe != other.typePipe)
            return false;
        if (wood != other.wood)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Pipe [name=" + name + ", price=" + price + ", manufacturer=" + manufacturer + ", material="
                + material + ", wood=" + wood + ", diameter=" + diameter + ", typePipe=" + typePipe + ", filter="
                + filter + ", id=" + id + "]";
    }


}



