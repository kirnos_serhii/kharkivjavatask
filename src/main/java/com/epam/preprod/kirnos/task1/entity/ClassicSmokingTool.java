package com.epam.preprod.kirnos.task1.entity;

import com.epam.preprod.kirnos.task7.annotation.NameField;

public abstract class ClassicSmokingTool extends SmokingTool {

	@NameField(name = "FILTER")
	boolean filter;
	@NameField(name = "MATERIAL")
	Material material;
	
	public ClassicSmokingTool(String id, String name, double price, String manufacturer, boolean filter,
			Material material) {
		super(id, name, price, manufacturer);
		this.filter = filter;
		this.material = material;
	}

	public ClassicSmokingTool(){
	};

	public boolean isFilter() {
		return filter;
	}

	public void setFilter(boolean filter) {
		this.filter = filter;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassicSmokingTool other = (ClassicSmokingTool) obj;
		if (filter != other.filter)
			return false;
		if (material != other.material)
			return false;
		return true;
	}
	
	
}
