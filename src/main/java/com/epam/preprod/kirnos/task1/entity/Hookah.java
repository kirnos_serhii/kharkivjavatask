package com.epam.preprod.kirnos.task1.entity;

import com.epam.preprod.kirnos.task7.annotation.NameField;

public class Hookah extends ClassicSmokingTool {

	@NameField(name = "NUMB_TUBES")
	private int numbTubes;
	@NameField(name = "MINE_HOOKAH")
	private Material mineHookah;
	@NameField(name = "HEIGHT")
	private int height;
	@NameField(name = "CLICK_SYSTEM")
	private boolean clickSystem;

	public Hookah() {}

	public Hookah(String id, String name, double price, String manufacturer, boolean filter,
			Material material, int numbTubes, Material mineHookah, int height, boolean clickSystem) {
		super(id, name, price, manufacturer, filter, material);
		this.numbTubes = numbTubes;
		this.mineHookah = mineHookah;
		this.height = height;
		this.clickSystem = clickSystem;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getNumbTubes() {
		return numbTubes;
	}

	public void setNumbTubes(int numbTubes) {
		this.numbTubes = numbTubes;
	}

	public Material getMineHookah() {
		return mineHookah;
	}

	public void setMineHookah(Material mineHookah) {
		this.mineHookah = mineHookah;
	}
	
	public boolean isClickSystem() {
		return clickSystem;
	}

	public void setClickSystem(boolean clickSystem) {
		this.clickSystem = clickSystem;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Hookah other = (Hookah) obj;
		if (clickSystem != other.clickSystem)
			return false;
		if (height != other.height)
			return false;
		if (mineHookah != other.mineHookah)
			return false;
		return numbTubes == other.numbTubes;
	}

	@Override
	public String toString() {
		return "Hookah [name=" + name + ", price=" + price + ", manufacturer=" + manufacturer + ", height=" + height
				+ ", ClickSystem=" + clickSystem + ", mineHookah=" + mineHookah + ", filter=" + filter + ", material="
				+ material + ", id=" + id + ", numbTubes=" + numbTubes + "]";
	}
	
}
