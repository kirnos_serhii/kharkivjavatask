package com.epam.preprod.kirnos.task1.entity;

import com.epam.preprod.kirnos.task7.annotation.NameField;

public class Vape extends ElectronicSmokingTool {

    @NameField(name = "CAPACITY")
    private double capacity;
    @NameField(name = "STEAM_SYSTEM")
    private SteamSystem steamSystem;
    @NameField(name = "IS_SERVICED")
    private boolean isServiced;

    public Vape() {
    }

    public Vape(String id, String name, double price, String manufacturer, ManagementType managementType,
                double batteryCapacity, Material caseMaterial, double capacity, SteamSystem steamSystem,
                boolean isServiced) {
        super(id, name, price, manufacturer, managementType, batteryCapacity, caseMaterial);
        this.capacity = capacity;
        this.steamSystem = steamSystem;
        this.isServiced = isServiced;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public SteamSystem getSteamSystem() {
        return steamSystem;
    }

    public void setSteamSystem(SteamSystem steamSystem) {
        this.steamSystem = steamSystem;
    }

    public boolean isServiced() {
        return isServiced;
    }

    public void setServiced(boolean isServiced) {
        this.isServiced = isServiced;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Vape other = (Vape) obj;
        if (Double.doubleToLongBits(capacity) != Double.doubleToLongBits(other.capacity))
            return false;
        if (isServiced != other.isServiced)
            return false;
        if (steamSystem != other.steamSystem)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Vape [name=" + name + ", price=" + price + ", manufacturer=" + manufacturer
                + ",capacity=" + capacity + ", steamSystem=" + steamSystem + ", isServiced=" + isServiced
                + ", managementType=" + managementType + ", batteryCapacity=" + batteryCapacity
                + ", caseMaterial=" + caseMaterial + ", id=" + id + "]";
    }


}
