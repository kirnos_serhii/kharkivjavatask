package com.epam.preprod.kirnos.task7.create;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TypesToolContainer {
    private static Map<String, Class<? extends SmokingTool>> creators = new TreeMap<>();

    public static void addCreator(String msg, Class<? extends SmokingTool> smokingTool) {
        creators.put(msg, smokingTool);
    }

    public static Set<String> getKeys(){
        return creators.keySet();
    }

    public static Class<? extends SmokingTool> get(String creatorName) {
        if (creatorName == null || !creators.containsKey(creatorName)) {
            return null;
        }
        return creators.get(creatorName);
    }
}
