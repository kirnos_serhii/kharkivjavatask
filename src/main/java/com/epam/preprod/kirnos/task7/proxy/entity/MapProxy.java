package com.epam.preprod.kirnos.task7.proxy.entity;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class MapProxy implements InvocationHandler {

    private Map<String, Object> fields = new HashMap<>();

    public MapProxy(Object obj) throws InvocationTargetException, IllegalAccessException {
        for (Method method : obj.getClass().getMethods()) {
            if (method.getName().startsWith("get")) {
                fields.put(method.getName().substring(3), method.invoke(obj));
            }
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        if (method.getName().startsWith("set")) {
            return fields.put(method.getName().substring(3), args[0]);
        }
        if (method.getName().startsWith("get")) {
            return fields.get(method.getName().substring(3));
        }
        throw new UnsupportedOperationException("Operation" + method.getName() + "do not support for this object.");
    }
}
