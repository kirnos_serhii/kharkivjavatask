package com.epam.preprod.kirnos.task7.proxy.entity;

import com.epam.preprod.kirnos.task1.entity.Material;
import com.epam.preprod.kirnos.task1.entity.TypePipe;
import com.epam.preprod.kirnos.task1.entity.Wood;

public interface IPipe {

    String getManufacturer();

    void setManufacturer(String manufacturer);

    String getId();

    void setId(String id);

    String getName();

    void setName(String name);

    double getPrice();

    void setPrice(double price);

    TypePipe getTypePipe();

    Wood getWood();

    void setWood(Wood wood);

    double getDiameter();

    void setDiameter(double diameter);

    TypePipe isTypePipe();

    void setTypePipe(TypePipe typePipe);

    boolean isFilter();

    void setFilter(boolean filter);

    Material getMaterial();

    void setMaterial(Material material);
}
