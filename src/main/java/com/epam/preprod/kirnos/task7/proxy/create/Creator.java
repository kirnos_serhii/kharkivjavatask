package com.epam.preprod.kirnos.task7.proxy.create;

public interface Creator {

    Object create(Object object) throws Throwable;
}
