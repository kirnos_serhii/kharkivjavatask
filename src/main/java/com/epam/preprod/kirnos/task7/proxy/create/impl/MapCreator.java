package com.epam.preprod.kirnos.task7.proxy.create.impl;

import com.epam.preprod.kirnos.task7.proxy.create.Creator;
import com.epam.preprod.kirnos.task7.proxy.entity.MapProxy;

import java.lang.reflect.Proxy;

public class MapCreator implements Creator {

    @Override
    public Object create(Object object) throws Throwable {
        return Proxy.newProxyInstance(
                object.getClass().getClassLoader(),
                object.getClass().getInterfaces(),
                new MapProxy(object));
    }
}
