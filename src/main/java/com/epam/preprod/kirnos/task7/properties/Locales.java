package com.epam.preprod.kirnos.task7.properties;

import java.util.Locale;

public enum Locales {
    RU {
        {
            locale = new Locale("ru");
        }
    },
    EN {
        {
            locale = new Locale("en");
        }
    };

    protected Locale locale;

    public Locale getLocale(){
        return locale;
    }
}
