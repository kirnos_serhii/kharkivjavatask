package com.epam.preprod.kirnos.task7.create;

import com.epam.preprod.kirnos.task1.entity.SmokingTool;
import com.epam.preprod.kirnos.task4.exceptions.ShopException;
import com.epam.preprod.kirnos.task6.input.Input;
import com.epam.preprod.kirnos.task7.annotation.NameField;
import com.epam.preprod.kirnos.task7.properties.ResourceManager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ReflectionCreator {

    private Input typeInput;

    public ReflectionCreator(Input typeInput) {
        this.typeInput = typeInput;
    }

    public SmokingTool create(String toolKey) throws IllegalAccessException, InstantiationException, ShopException {
        Class<? extends SmokingTool> clazz = TypesToolContainer.get(toolKey);
        if (clazz == null){
            return null;
        }
        SmokingTool instance = clazz.newInstance();

        List<Field> fields = new ArrayList<>();
        addFields(clazz, fields);
        for (Field field : fields) {
            Object value = getValue(field);
            if (value != null) {
                field.set(instance, value);
            }
        }
        return instance;
    }

    private Object getValue(Field field) throws ShopException {
        Input.class.getEnumConstants();
        if (field.getType().equals(Integer.class) || int.class.equals(field.getType())) {
            return typeInput.inputInt(ResourceManager.INSTANCE.getString(field.getAnnotation(NameField.class).name()),
                    0, 1000000);
        } else if (field.getType().equals(Double.class) || double.class.equals(field.getType())) {
            return typeInput.inputDouble(ResourceManager.INSTANCE.getString(field.getAnnotation(NameField.class).name()),
                    0, 1000000);
        } else if (field.getType().equals(Boolean.class) || boolean.class.equals(field.getType())) {
            return typeInput.inputBoolean(ResourceManager.INSTANCE.getString(field.getAnnotation(NameField.class).name()));
        } else if (field.getType().equals(String.class)) {
            return typeInput.inputString(ResourceManager.INSTANCE.getString(field.getAnnotation(NameField.class).name()),
                    str -> true);
        } else if(field.getType().isEnum()){
            return typeInput.inputEnum(ResourceManager.INSTANCE.getString(field.getAnnotation(NameField.class).name()),
                    (Enum[])field.getType().getEnumConstants());
        }
        return null;
    }

    private void addFields(Class<?> clazz, List<Field> fields) {
        if (clazz.getSuperclass() != null) {
            addFields(clazz.getSuperclass(), fields);
        }
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(NameField.class)) {
                field.setAccessible(true);
                fields.add(field);
            }
        }
    }

}
