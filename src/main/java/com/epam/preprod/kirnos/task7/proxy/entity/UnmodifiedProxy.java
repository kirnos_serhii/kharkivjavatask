package com.epam.preprod.kirnos.task7.proxy.entity;

import java.lang.instrument.UnmodifiableClassException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class UnmodifiedProxy implements InvocationHandler {

    private Object object;

    public UnmodifiedProxy(Object obj) {
        object = obj;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().startsWith("set")) {
            throw new UnsupportedOperationException();
        }
        return method.invoke(object, args);
    }
}
