package com.epam.preprod.kirnos.task7.proxy.create.impl;

import com.epam.preprod.kirnos.task7.proxy.create.Creator;
import com.epam.preprod.kirnos.task7.proxy.entity.UnmodifiedProxy;

import java.lang.reflect.Proxy;

public class UnmodifiedCreator implements Creator {

    @Override
    public Object create(Object object) {
        return Proxy.newProxyInstance(
                object.getClass().getClassLoader(),
                object.getClass().getInterfaces(),
                new UnmodifiedProxy(object)
        );
    }
}
