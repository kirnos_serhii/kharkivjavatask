package com.epam.preprod.kirnos.task7.properties;

import java.util.Locale;
import java.util.ResourceBundle;

public enum ResourceManager {
    INSTANCE;
    private ResourceBundle resourceBundle;
    private final String resourceName = "text";

    private ResourceManager() {
        resourceBundle = ResourceBundle.getBundle(resourceName, new Locale("en", "US"));
    }

    public void changeResource(Locale locale) {
        resourceBundle = ResourceBundle.getBundle(resourceName, locale);
    }

    public String getString(String key) {
        return resourceBundle.getString(key);
    }
}
